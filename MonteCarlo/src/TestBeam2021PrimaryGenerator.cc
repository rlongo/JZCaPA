//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file TestBeam2021PrimaryGenerator.cc
/// \brief Implementation of the TestBeam2021PrimaryGenerator1 class
//
//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "TestBeam2021PrimaryGenerator.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4String.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestBeam2021PrimaryGenerator::TestBeam2021PrimaryGenerator()
: G4VPrimaryGenerator(),
  m_WireChamberSize(101)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TestBeam2021PrimaryGenerator::~TestBeam2021PrimaryGenerator()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestBeam2021PrimaryGenerator::GeneratePrimaryVertex(G4Event* event)
{
  //Get the alignment from DetectorConstruction
  if(event->GetEventID() == 0){
    GetWireChamberDist();
  }

  G4double x = m_RandX->shoot()*(m_WireChamberSize-1)-(m_WireChamberSize-1)/2; //Map (0, 1) to (-50, 50), RandNum * 100 - 50
  G4double y = m_RandY->shoot()*(m_WireChamberSize-1)-(m_WireChamberSize-1)/2;
  G4ThreeVector position(x, y, 0);
  G4double time = 0*s;
  G4PrimaryVertex* vertex = new G4PrimaryVertex(position, time);

  G4cout << "Beam scan & type & energy ==> (" << m_alignment.scanNumber << m_alignment.beam_type << ", " << m_alignment.beam_energy << ")" << G4endl;
  G4cout << "Particle Position ==> (" << position.x() << ", " << position.y() << ", " << position.z() << ")" << G4endl;

  G4ParticleDefinition* particleDefinition
           = G4ParticleTable::GetParticleTable()->FindParticle(m_alignment.beam_type.c_str()); // about the definition of the ParticleTable, I changed the Alignment_2021.xml file, "e" to "e-", "p" to "proton"
  G4PrimaryParticle* particle = new G4PrimaryParticle(particleDefinition);
  particle->SetMomentumDirection(G4ThreeVector(0,0,1));
  particle->SetKineticEnergy(m_alignment.beam_energy*GeV); //I'm not sure if I need units here
  vertex->SetPrimary(particle);

  event->AddPrimaryVertex(vertex);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TestBeam2021PrimaryGenerator::GetWireChamberDist(){
  DetectorConstruction* dC
  = (DetectorConstruction*)G4RunManager::GetRunManager()->GetUserDetectorConstruction();
  m_alignment = dC->GetAlignment2021();

  // Define the position of the particle
  G4String _profile = (std::string)std::getenv("JZCaPA") + "/Utils/2021testbeam_scan/scan" + std::to_string(m_alignment.scanNumber) + ".txt"; //Select the corresponding scan file according to run number

  // Attempt to open the file and check if it succeeded
  std::ifstream _scanfile(_profile);
  if ( !_scanfile.is_open() ){
    G4cout << " ==== This Run does not belong to any processed Scan ==== " << G4endl; // You can check ("JZCaPA")/Utils/2021testbeam_scan/README.md
    return;
  }

  G4double probListX[m_WireChamberSize],
           probListY[m_WireChamberSize];
  // Put the X and Y beam profile into two arrays
  for(int j = 0; j < 2; ++j){
    for(int i = 0; i <m_WireChamberSize; ++i){
      if (!j){
        _scanfile >> probListX[i];
      }
      _scanfile >> probListY[i];
    }
  }
  _scanfile.close();

  m_RandX = new G4RandGeneral(probListX,m_WireChamberSize); //Generate the random number, range (0, 1)
  m_RandY = new G4RandGeneral(probListY,m_WireChamberSize);
}
