//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \ingroup mc
/// \file FiberSD.cc
/// \author Chad Lantz
/// \date 18 December 2023

#include "FiberSD.hh"
#include "SteppingAction.hh"

#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4ios.hh"
#include "G4Poisson.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4OpBoundaryProcess.hh"
#include "G4OpProcessSubType.hh"

#include <string>
#include <iostream>
#include <cmath>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FiberSD::FiberSD(G4String sdName, G4int modNum, G4bool optical)
  :G4VSensitiveDetector(sdName),
  m_modNum(modNum),
  OPTICAL(optical),
  REDUCED_TREE(false),
  ZDC(false),
  RPD(false),
  m_cherenkovVec(0) {
  collectionName.insert(sdName);
  HCID = -1;

  if( G4StrUtil::contains(sdName, "R") ) RPD = true;
  if( G4StrUtil::contains(sdName, "Z") ) ZDC = true;

  m_analysisManager = AnalysisManager::getInstance();
  m_analysisManager->RegisterSD(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FiberSD::~FiberSD(){ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FiberSD::BookOutput(){
  
  G4String modType = (ZDC) ? "ZDC" : "RPD";
  G4String nTupleName = modType + std::to_string(m_modNum) + "tree";
  m_nTupleNum = m_analysisManager->CreateNtuple(nTupleName, modType + " data");
  G4cout << "Created " << nTupleName << " with ntuple number " << m_nTupleNum << G4endl;

  if (REDUCED_TREE)
  {
    // This branch is a vector nFibers long that contains the number of cherenkovs
    // produced in each fiber or photons that arrive at the top of each fiber during an event
    m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "nCherenkovs", m_cherenkovVec);

    // This branch acts as a histogram to store the timing of photon arrival or production
    // depending on optical settings
    m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "timeHist", m_timeHist);
  }
  else
  { // Full output

    // vector< double > branches
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "x", m_x);
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "y", m_y);
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "z", m_z);
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "Px", m_px);
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "Py", m_py);
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "Pz", m_pz);
    m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "time", m_time);
    m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "rodNo", m_rodNo);

    if (OPTICAL)
    {
      // This branch is a vector nFibers long that contains the number of cherenkovs
      // produced in each fiber during an event
      m_nCherenkovsColumn = m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "nCherenkovs");
    }
    else
    {
      // Non-optical vector< double > branches
      m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "energy", m_energy);
      m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "velocity", m_velocity);
      m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "beta", m_beta);
      m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "eDep", m_eDep);
      m_analysisManager->CreateNtupleDColumn(m_nTupleNum, "charge", m_charge);

      // Non-optical vector< int > branches
      m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "nCherenkovs", m_cherenkovVec);
      m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "trackID", m_trackID);
      m_analysisManager->CreateNtupleIColumn(m_nTupleNum, "pid", m_pid);
    }
  }
  m_analysisManager->FinishNtuple(m_nTupleNum);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FiberSD::HistInitialize(){
  std::string name = GetName();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FiberSD::Initialize(G4HCofThisEvent*){
  
  if(REDUCED_TREE){
    // Clear it and resize in case this isn't the first event
    m_cherenkovVec.clear();
    m_cherenkovVec.resize( m_nFibers, 0 );
    m_timeHist.clear();
    m_timeHist.resize( 128*m_nSegments, 0 );
  }else{
    m_cherenkovVec.clear();
    m_rodNo.clear();
    m_trackID.clear();
    m_pid.clear();
    
    m_x.clear();
    m_y.clear();
    m_z.clear();
    m_px.clear();
    m_py.clear();
    m_pz.clear();
    m_time.clear();
    m_energy.clear();
    m_beta.clear();
    m_eDep.clear();
    m_velocity.clear();
    m_charge.clear();
  }

  m_nCherenkovs = 0;
  m_nHits = 0;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool FiberSD::ProcessHits(G4Step* aStep,G4TouchableHistory*){

  G4ParticleDefinition *particle = aStep->GetTrack()->GetDefinition();

  //Get the number of Cherenkov photons created in this step
  int createdPhotons = 0;
  const std::vector<const G4Track*>* secVec = aStep->GetSecondaryInCurrentStep();
  for(uint i = 0; i < secVec->size(); i++){
    if( secVec->at(i)->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition()){
      createdPhotons++;
    }//end if photon
  }//end secondary track loop
  m_nCherenkovs += createdPhotons; // Record the total in case OPTICAL is true

  // If OPTICAL is true, use the optical hit processing function
  if(OPTICAL){
    return ProcessHitOptical(aStep, NULL);

  }else{ // Otherwise record all hits except optical photons

    // Skip optical photons
    if (aStep->GetTrack()->GetDefinition() == G4OpticalPhoton::OpticalPhotonDefinition()) return true;

    G4int rodNum = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(0);

    // If we're using the reduced tree, just record the number of cherenkovs and return true
    if(REDUCED_TREE){
      m_cherenkovVec[rodNum] += createdPhotons;
      FillTimeVector( rodNum, aStep->GetTrack()->GetGlobalTime(), createdPhotons );

      m_nHits++;
      return true;
    }

    // If we've gotten here Optical and reduced tree are both off, and we're dealing
    // with a particle that's not an optical photon. Record all the info about it and move on
    G4ThreeVector pos = aStep->GetTrack()->GetPosition();
    G4ThreeVector momentum = aStep->GetPreStepPoint()->GetMomentum();

    m_cherenkovVec.push_back(createdPhotons);
    m_rodNo.push_back(rodNum);
    m_trackID.push_back(aStep->GetTrack()->GetTrackID());
    m_pid.push_back(particle->GetPDGEncoding());

    m_x.push_back(pos.x());
    m_y.push_back(pos.y());
    m_z.push_back(pos.z());
    m_px.push_back(momentum.x());
    m_py.push_back(momentum.y());
    m_pz.push_back(momentum.z());
    m_time.push_back(aStep->GetTrack()->GetGlobalTime());
    m_energy.push_back(aStep->GetPreStepPoint()->GetTotalEnergy());
    m_beta.push_back(aStep->GetPreStepPoint()->GetBeta());
    m_eDep.push_back(aStep->GetTotalEnergyDeposit());
    m_velocity.push_back(aStep->GetPreStepPoint()->GetVelocity());
    m_charge.push_back(aStep->GetPreStepPoint()->GetCharge());

    return true;
  }

  return false; //Something failed if we got here
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4bool FiberSD::ProcessHitOptical(G4Step *aStep, G4TouchableHistory *)
{
  G4int rodNum = aStep->GetPreStepPoint()->GetTouchableHandle()->GetCopyNumber(0);
  G4ThreeVector pos = aStep->GetTrack()->GetPosition();
  G4ThreeVector momentum = aStep->GetPreStepPoint()->GetMomentum();

  /*************************************************
   * Reject everything but optical photons
   **************************************************/
  if (aStep->GetTrack()->GetDefinition() != G4OpticalPhoton::OpticalPhotonDefinition())
  {
    return true;
  }

  /*************************************************
   * Reject downward going photons
   **************************************************/
  if (momentum.y() < 0.0)
  {
    aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    return true;
  }

  /*************************************************
   * Reject photons not at the boundary of two volumes
   **************************************************/
  G4StepPoint *endPoint = aStep->GetPostStepPoint();
  if (endPoint->GetStepStatus() != fGeomBoundary)
    return true;

  /*************************************************
   * Reject photons that are reflected back down
   * from the top of the fiber
   **************************************************/

  // Get the refractive index
  G4MaterialPropertiesTable *MPT = aStep->GetPreStepPoint()->GetMaterial()->GetMaterialPropertiesTable();
  if (!MPT) // Safety check
    return true;
  G4MaterialPropertyVector *RindexMPV = MPT->GetProperty(kRINDEX);
  if (!RindexMPV) // Safety check
    return true;

  G4double Rindex;
  size_t index = 0;
  double photonEnergy = aStep->GetTrack()->GetDynamicParticle()->GetTotalMomentum();
  Rindex = RindexMPV->Value(photonEnergy, index);

  // Determine the photon's angle from the vertical axis
  G4ThreeVector momDir = aStep->GetPreStepPoint()->GetMomentumDirection();
  G4double angleFromY = atan(sqrt(1 - pow(momDir.y(), 2.0)) / momDir.y());

  // kill the photon if angle is greater than TIR critical angle
  // We're assuming the fiber's top face's normal vector is parallel to the y axis
  if (angleFromY > asin(1.0 / Rindex))
  {
    aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    return true;
  }

  /*************************************************
   * Reject photons that are not totally internally
   * reflected inside the fiber
   **************************************************/

  G4bool isTIR = false;
  G4ProcessVector *postStepDoItVector = G4OpticalPhoton::OpticalPhotonDefinition()->GetProcessManager()->GetPostStepProcessVector(typeDoIt);
  G4int n_proc = postStepDoItVector->entries();
  for (G4int i = 0; i < n_proc; ++i)
  {
    G4OpBoundaryProcess *opProc = dynamic_cast<G4OpBoundaryProcess *>((*postStepDoItVector)[i]);
    if (opProc && opProc->GetStatus() == TotalInternalReflection)
    {
      isTIR = true;
      break;
    }
  }

  if (!isTIR)
  {
    aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    return true;
  }

  /*************************************************
   * Simulate absorption by calculating the photon's
   * remaining path length and rolling a random number
   * against its absorption chance
   **************************************************/
  G4MaterialPropertyVector *AbsMPV = MPT->GetProperty(kABSLENGTH);
  G4double Absorption = AbsMPV->Value(aStep->GetTrack()->GetDynamicParticle()->GetTotalMomentum(), index);

  G4double pathLength = (m_topOfVolume - pos.y()) / cos(angleFromY);
  G4double absChance = 1 - exp(-pathLength / Absorption);

  // This check amounts to if(absorbed)
  if (CLHEP::RandFlat::shoot(0.0, 1.0) < absChance)
  {
    aStep->GetTrack()->SetTrackStatus(fStopAndKill);
    return true;
  }

  /*************************************************
   * Record the survivors
   **************************************************/
  if(REDUCED_TREE){
    m_cherenkovVec[rodNum]++;
    FillTimeVector( rodNum, aStep->GetTrack()->GetGlobalTime() );
    m_nHits++;
  }else{
    G4ThreeVector origin = aStep->GetTrack()->GetVertexPosition();
    m_x.push_back(origin.x());
    m_y.push_back(origin.y());
    m_z.push_back(origin.z());
    m_px.push_back(momentum.x());
    m_py.push_back(momentum.y());
    m_pz.push_back(momentum.z());
    m_time.push_back(aStep->GetTrack()->GetGlobalTime());
    m_rodNo.push_back(rodNum);
  }

  /*************************************************
   * Put the survivors out of their misery
   **************************************************/
  aStep->GetTrack()->SetTrackStatus(fStopAndKill);
  return true;
}

  //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FiberSD::EndOfEvent(G4HCofThisEvent *)
{
  if(REDUCED_TREE){
    G4cout << SensitiveDetectorName << ": " << m_nHits << " Photons captured of " << m_nCherenkovs << " generated" << G4endl;
  }else{
    if(OPTICAL){
      m_analysisManager->FillNtupleIColumn(m_nTupleNum, m_nCherenkovsColumn, m_nCherenkovs);
      G4cout << SensitiveDetectorName << ": " << m_x.size() << " Photons captured of " << m_nCherenkovs << " generated" << G4endl;
    }else{
      G4cout << SensitiveDetectorName << ": " << m_x.size() << " hits generated " << m_nCherenkovs << " photons" << G4endl;
    }
  }
  m_analysisManager->AddNtupleRow(m_nTupleNum);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FiberSD::SetReducedTree ( G4int _nFibers, G4int _nSegments ){
  REDUCED_TREE = true;
  m_nFibers = _nFibers;
  m_nSegments = _nSegments;
  m_nFibersPerSegment = _nFibers/_nSegments;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void FiberSD::FillTimeVector( G4int fiberNo, G4double time, G4int weight ){
  // This vector consists of nSegments number of "histograms" each 128 bins long, covering 64ns
  // concatenated in order of segment number (fiber number/m_nFibersPerSegment a.k.a. Channel)

  int bin = (time <= 64*ns) ? time/0.5 : 127;  // Determine the bin for this segments histo
  int offset = fiberNo/m_nFibersPerSegment;    // Do this because there was a rounding issue when just using an int cast
  bin += 128*offset;                           // Add the offset for the current segment (channel)
  m_timeHist[bin] += weight;                // Add the weight (nPhotons).
}
