/// \ingroup mc
/// \file Materials.cc
/// \author Riccardo Longo

#include "Materials.hh"
#include "G4SystemOfUnits.hh"
#include "TFile.h"
#include "TGraph.h"
#include "TSystem.h"

Materials* Materials::materials = NULL;

Materials* Materials::getInstance(void)
{
  if (materials == NULL) {
    materials = new Materials();
  }
  return materials;
}

/* Function that constructs all the materials - to be then retrieved via pointer */
Materials::Materials(void):
  Elow(0.0),
  deltaE(0.0) {

  fMessenger = new G4GenericMessenger(this,"/Materials/", "Set properties for UI defined materials i.e. Silica and Kapton");
  fMessenger->DeclareMethod("SetWavelengthRange", &Materials::SetWavelengthRange, "Set the wavelength range in nm for UI materials to be defined for");
  fMessenger->DeclareMethod("SetEnergyRange", &Materials::SetEnergyRange, "Set the energy range in eV for UI materials to be defined for");
  // fMessenger->DeclareProperty("Kapton_ABSL_name", kaptonAbslGraphName, “Name of the kapton absorption length graph”);

  H  = new G4Element("Hydrogen", "H",  1.,   1.01   *g/mole);
  N  = new G4Element("Nitrogen", "N",  7.,   14.01  *g/mole);
  Ni = new G4Element("Ni",       "Ni", 28.0, 58.6934*g/mole);
  W  = new G4Element("W",        "W",  74.0, 183.84 *g/mole);
  Fe = new G4Element("Fe",       "Fe", 26.0, 55.845 *g/mole);
  O  = new G4Element("Oxygen",   "O",  8.,   16.00  *g/mole);
  Si = new G4Element("Silicon",  "Si", 14.,  28.08  *g/mole);
  C  = new G4Element("Carbon",   "C",  6.,   12.00  *g/mole);

  //Materials definition
  Xenon = new G4Material("XenonGas", 54., 131.29 * g/mole, 5.458 * mg/cm3,
			 kStateGas, 93.15 * kelvin, 1 * atmosphere);


  // Absorber composition:  savannah.cern.ch/task/download.php?file_id=22925
  NiW = new G4Material("Tungsten/Nickel Composite",18.155*g/cm3,3);
  NiW->AddElement(W,  0.948);
  NiW->AddElement(Ni, 0.037);
  NiW->AddElement(Fe, 0.015);

  Steel = new G4Material("Steel", 7.9*gram/cm3,2);
  Steel->AddElement(Fe, 0.98);
  Steel->AddElement(C,  0.02);

  SilicaCore_UI = new G4Material("SilicaCore_UI", 2.200 * g/cm3, 2);
  SilicaCore_UI->AddElement(Si,1);
  SilicaCore_UI->AddElement(O, 2);

  SilicaClad_UI = new G4Material("SilicaClad_UI", 2.200 * g/cm3, 2);
  SilicaClad_UI->AddElement(Si,1);
  SilicaClad_UI->AddElement(O, 2);

  Kapton_UI = new G4Material("Kapton_UI", 1.42 * g/cm3, 4);
  Kapton_UI->AddElement(C,22);
  Kapton_UI->AddElement(H,10);
  Kapton_UI->AddElement(N,2);
  Kapton_UI->AddElement(O,5);


  pQuartz = new G4Material("Quartz", 2.200 * g/cm3, 2);
  pQuartz->AddElement(Si,1);
  pQuartz->AddElement(O, 2);

  fiberClad = new G4Material("FiberCladding", 2.200 * g/cm3, 2);
  fiberClad->AddElement(Si,1);
  fiberClad->AddElement(O,2);

  EM_Quartz = new G4Material("EMQuartz", 2.200 * g/cm3, 2);
  EM_Quartz->AddElement(Si,1);
  EM_Quartz->AddElement(O, 2);

  nist_manager = G4NistManager::Instance();
  nist_manager->SetVerbose(1);

  Air   = nist_manager->FindOrBuildMaterial("G4_AIR");
  Al    = nist_manager->FindOrBuildMaterial("G4_Al");   //G4_Al
  Cu    = nist_manager->FindOrBuildMaterial("G4_Cu");
  Pb    = nist_manager->FindOrBuildMaterial("G4_Pb");
  pureW = nist_manager->FindOrBuildMaterial("G4_W");

  Polyethylene = new G4Material("Polyethylene", 0.975 * g/cm3, 2);
  Polyethylene->AddElement(H,4);
  Polyethylene->AddElement(C,2);

  PMMA =  new G4Material("Polymethylmethacrylate", 1.18 * g/cm3, 3);
  PMMA->AddElement(C,5);
  PMMA->AddElement(H,8);
  PMMA->AddElement(O,2);

  Kapton = new G4Material("Kapton", 1.42 * g/cm3, 4);
  Kapton->AddElement(C,35);
  Kapton->AddElement(H,8);
  Kapton->AddElement(N,2);
  Kapton->AddElement(O,7);

  Grease =  new G4Material("Grease", 1.0 * g/cm3, 3);
  Grease->AddElement(C,1);
  Grease->AddElement(H,1);
  Grease->AddElement(O,1);

}

void Materials::DefineOpticalProperties(void){



  //////////////////////////////////////////////////////
  //      UIUC Materials
  //////////////////////////////////////////////////////

  G4cout << "Defining material optical properties from " << 1240/(Elow + deltaE*nEntries_UI) << "nm to " << 1240/Elow << "nm " << G4endl;

  //TODO: Get the graphs with variable names set by messenger commands
  std::string path = std::getenv("JZCaPA") + std::string("/Utils/opticalMaterials.root");
  TFile *f;
  if( !gSystem->AccessPathName(path.c_str()) ){
    f = new TFile( path.c_str(),"read");
  }
  else{
    f = new TFile("opticalMaterials.root","read");
  }
  if(!f->IsOpen() || f->IsZombie()){
    G4cout << "Error reading opticalMaterials.root" << G4endl;
  }

  TGraph *gSilicaRIND = (TGraph*)f->Get("spectrosil3301_RINDEX");
  TGraph *gSilicaABSL = (TGraph*)f->Get("spectrosil3301_ABSL_cm");
  TGraph *gKaptonRIND = (TGraph*)f->Get("kaptonRINDEX");
  TGraph *gKaptonREFL = (TGraph*)f->Get("kaptonREFLECTIVITY");
  TGraph *gKaptonABSL = (TGraph*)f->Get("kapton_ABSL_cm");

  //Generate all values for the UI materials
  //Use TMath::Min and TMath::Max to keep the values in physical ranges. i.e. absorption length > 0, 0 < reflectivity < 1, refractive index > 1.0
  G4double photonEnergy_UI[50], silica_RIND_UI[50], silica_clad_RIND_UI[50], silica_ABSL_UI[50],
           kapton_RIND_UI[50],  kapton_REFL_UI[50], kapton_ABSL_UI[50],
           NiW_REFL_UI[50], RefractiveIndexAir[50], Al_refl[50];
  G4double energy;
  if(Elow == 0) Elow = 1240.0/650.0;
  if(deltaE == 0) deltaE = 1240.0/(650.0-300.0)/nEntries_UI;
  for(int i = 0; i < nEntries_UI; i++){
    energy = Elow + i*deltaE;
    photonEnergy_UI[i] = energy*eV;
    silica_RIND_UI[i] = TMath::Max(1.0,gSilicaRIND->Eval(energy));
    silica_ABSL_UI[i] = TMath::Max(0.0,gSilicaABSL->Eval(energy)*cm);
    silica_clad_RIND_UI[i] = sqrt( pow(silica_RIND_UI[i],2.0) - pow(0.22,2.0) );
    kapton_RIND_UI[i] = TMath::Max(1.0,gKaptonRIND->Eval(energy));
    kapton_REFL_UI[i] = TMath::Min(1.0,TMath::Max(0.0,gKaptonREFL->Eval(energy)));
    kapton_ABSL_UI[i] = TMath::Max(0.0,gKaptonABSL->Eval(energy)*cm);
    NiW_REFL_UI[i] = 0.62;
    RefractiveIndexAir[i] = 1.0;
    Al_refl[i] = 0.89;
  }

  //Silica core
  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX",photonEnergy_UI,silica_RIND_UI,nEntries_UI);//index of refraction
  MPT_Array.back()->AddProperty("ABSLENGTH",photonEnergy_UI,silica_ABSL_UI,nEntries_UI);//absorption length
  SilicaCore_UI->SetMaterialPropertiesTable(MPT_Array.back());

  //Silica cladding
  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX",photonEnergy_UI,silica_clad_RIND_UI,nEntries_UI);//index of refraction
  MPT_Array.back()->AddProperty("ABSLENGTH",photonEnergy_UI,silica_ABSL_UI,nEntries_UI);//absorption length
  SilicaClad_UI->SetMaterialPropertiesTable(MPT_Array.back());

  //Kapton
  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX",photonEnergy_UI,kapton_RIND_UI,nEntries_UI);
  MPT_Array.back()->AddProperty("ABSLENGTH",photonEnergy_UI,kapton_ABSL_UI,nEntries_UI);
  MPT_Array.back()->AddProperty("REFLECTIVITY",photonEnergy_UI,kapton_REFL_UI,nEntries_UI);
  Kapton_UI->SetMaterialPropertiesTable(MPT_Array.back());

  //Tungsten nickel composite
  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("REFLECTIVITY",photonEnergy_UI,NiW_REFL_UI,nEntries_UI);
  NiW->SetMaterialPropertiesTable(MPT_Array.back());

  //Air
  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX", photonEnergy_UI, RefractiveIndexAir, nEntries_UI);
  Air->SetMaterialPropertiesTable(MPT_Array.back());

  //Aluminum
  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("REFLECTIVITY",photonEnergy_UI,Al_refl,nEntries_UI);
  Al->SetMaterialPropertiesTable(MPT_Array.back());



  //////////////////////////////////////////////////////
  //      Maryland RPD Materials
  //////////////////////////////////////////////////////

  const float grease_RI = 1.46;
  const float clad_RI = 1.49;
  const float core_RI = 1.6;
  //  const float tile_RI = 1.4585;

  G4double quartz_RIND[nEntries_UMD], quartz_ABSL[nEntries_UMD], PhotonEnergy[nEntries_UMD];
  //G4double quartz_RFLT[nEntries_UMD];
  G4double quartz_EFIC[nEntries_UMD];
  for(int i = 0; i < nEntries_UMD; i++){
    PhotonEnergy[i] = 2.00*eV + i*0.03*eV;
    //         quartz_RIND[i] = tile_RI; //Refractive Index - constants
    quartz_RIND[i] = core_RI; //Refractive Index - constants
    quartz_ABSL[i] = 300.00*cm + i*20*cm; //Attenuation length
    //         quartz_RFLT[i] = 0.5;
    //         quartz_EFIC[i] = 0.5;

  }

  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX",PhotonEnergy,quartz_RIND,nEntries_UMD);//index of refraction
  MPT_Array.back()->AddProperty("ABSLENGTH",PhotonEnergy,quartz_ABSL,nEntries_UMD);//absorption length
  // MPT_Array.back()->AddProperty("REFLECTIVITY",PhotonEnergy,quartz_RFLT,nEntries_UMD);//refelectivity
  // MPT_Array.back()->AddProperty("EFFICIENCY",PhotonEnergy,quartz_EFIC,nEntries_UMD);//efficiency
  pQuartz->SetMaterialPropertiesTable(MPT_Array.back());



  //Aluminium optical properties
  G4double AllPhotonEnergies[50] = {2.00*eV, 2.04*eV, 2.07*eV, 2.11*eV, 2.15*eV, 2.18*eV, 2.22*eV, 2.26*eV, 2.29*eV,
				    2.33*eV, 2.37*eV, 2.40*eV, 2.44*eV, 2.48*eV, 2.51*eV, 2.55*eV, 2.59*eV, 2.62*eV,
				    2.66*eV, 2.70*eV, 2.74*eV, 2.77*eV, 2.81*eV, 2.85*eV, 2.88*eV, 2.92*eV, 2.96*eV,
				    2.99*eV, 3.03*eV, 3.07*eV, 3.10*eV, 3.14*eV, 3.18*eV, 3.21*eV, 3.25*eV, 3.29*eV,
				    3.32*eV, 3.36*eV, 3.40*eV, 3.43*eV, 3.47*eV, 5.0*eV,  25.0*eV,100.0*eV, 1000.00*eV,
				    10000.0*eV, 25000.0*eV, 50000.0*eV, 250000.*eV, 1000000.*eV};


  //Maryland Polyethilene optical properties

  G4double RefractiveIndexClad1[nEntries_UMD];
  G4double AbsClad[nEntries_UMD];
  for (int i = 0; i < nEntries_UMD; i++){
    RefractiveIndexClad1[i] = clad_RI;//
    AbsClad[i] = 20.0*m;
  }

  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX",PhotonEnergy,RefractiveIndexClad1,nEntries_UMD);
  MPT_Array.back()->AddProperty("ABSLENGTH",PhotonEnergy,AbsClad,nEntries_UMD);
  Polyethylene->SetMaterialPropertiesTable(MPT_Array.back());

  //PMMA optical properties
  G4double RefractiveIndexWLSfiber[nEntries_UMD];
  for (int i = 0; i < nEntries_UMD; i++) RefractiveIndexWLSfiber[i] = core_RI;
  G4double AbsWLSfiber[50] = { 5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,
			       5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,
			       5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,5.40*m,1.10*m,
			       1.10*m,1.10*m,1.10*m,1.10*m,1.10*m,1.10*m, 1.*mm, 1.*mm, 1.*mm, 1.*mm,
			       1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm, 1.*mm};
  G4double EmissionWLSfiber[50] = {0.05, 0.10, 0.30, 0.50, 0.75, 1.00, 1.50, 1.85, 2.30, 2.75,
				   3.25, 3.80, 4.50, 5.20, 6.00, 7.00, 8.50, 9.50, 11.1, 12.4,
				   12.9, 13.0, 12.8, 12.3, 11.1, 11.0, 12.0, 11.0, 17.0, 16.9,
				   15.0, 9.00, 2.50, 1.00, 0.05, 0.00, 0.00, 0.00, 0.00, 0.00,
				   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00};

  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("WLSABSLENGTH", PhotonEnergy, AbsWLSfiber, nEntries_UMD);
  MPT_Array.back()->AddProperty("WLSCOMPONENT", PhotonEnergy, EmissionWLSfiber, nEntries_UMD);
  MPT_Array.back()->AddProperty("RINDEX", PhotonEnergy, RefractiveIndexWLSfiber, nEntries_UMD);
  MPT_Array.back()->AddConstProperty("WLSTIMECONSTANT", 0.5*ns);
  PMMA->SetMaterialPropertiesTable(MPT_Array.back());

  //Kapton optical properties
  // OPTICAL PROPERTIES OF MATERIALS FOR CONCENTRATOR PHOTOVOLTAIC SYSTEMS
  // https://engineering.case.edu/centers/sdle/sites/engineering.case.edu.centers.sdle/files/optical_properties_of_materials.pdf
  G4double kapton_RIND[50] = {};

  // Optical Characterization of Commonly Used Thermal Control Paints in a Simulated GEO Environment
  // https://amostech.com/TechnicalPapers/2018/Poster/Bengtson.pdf
  G4double kapton_RFLT[50] = {0.44, 0.42, 0.40, 0.37, 0.35, 0.32, 0.29, 0.26, 0.22, 0.19, 0.15, 0.11, 0.09, 0.06, 0.04,
			      0.03, 0.02, 0.02, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
			      0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01,
			      0.01, 0.01, 0.01, 0.01, 0.01};


  // Synthesis and properties of soluble aromatic polyimides from novel 4,5-diazafluorene-containing dianhydride
  // https://pubs.rsc.org/fa/content/articlehtml/2018/ra/c7ra12101f
  G4double kapton_ABSL[50] = {65.39*um, 64.28*um, 62.72*um, 61.96*um, 60.49*um, 59.43*um, 58.31*um, 57.07*um,
			      56.10*um, 54.85*um, 53.47*um, 52.21*um, 51.09*um, 49.68*um, 48.15*um, 46.51*um,
			      45.24*um, 44.06*um, 43.02*um, 42.01*um, 40.87*um, 40.07*um, 39.16*um, 38.45*um,
			      37.64*um, 37.01*um, 36.26*um, 35.71*um, 35.05*um, 34.42*um, 33.84*um, 33.14*um,
			      32.40*um, 31.73*um, 30.64*um, 29.49*um, 28.41*um, 26.86*um, 25.22*um, 23.35*um,
			      21.15*um, 19.18*um, 16.93*um, 14.84*um, 12.67*um, 10.63*um, 9.07*um, 7.72*um,
			      6.60*um, 5.64*um};


  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX", PhotonEnergy, kapton_RIND, nEntries_UMD);
  MPT_Array.back()->AddProperty("REFLECTIVITY",AllPhotonEnergies,kapton_RFLT,nEntries_UMD);
  MPT_Array.back()->AddProperty("ABSLENGTH",AllPhotonEnergies,kapton_ABSL,nEntries_UMD);
  Kapton->SetMaterialPropertiesTable(MPT_Array.back());

  //Grease (silicone) optical properties
  G4double RefractiveIndexGrease[50];
  for (int i = 0; i < nEntries_UMD; i++) RefractiveIndexGrease[i] = grease_RI;

  MPT_Array.push_back(new G4MaterialPropertiesTable());
  MPT_Array.back()->AddProperty("RINDEX",PhotonEnergy,RefractiveIndexGrease,nEntries_UMD);
  MPT_Array.back()->AddProperty("ABSLENGTH",PhotonEnergy,AbsClad,nEntries_UMD);
  Grease->SetMaterialPropertiesTable(MPT_Array.back());


  // set the optical boundary properties

  AlSurface = new G4OpticalSurface("AlSurface",unified, polished, dielectric_metal, 1);

  G4MaterialPropertiesTable* AlSurfaceProperty = new G4MaterialPropertiesTable();
  //AlSurfaceProperty->AddProperty("RINDEX",AllPhotonEnergies,RefractiveIndexGrease,nEntries_UMD);
  AlSurfaceProperty->AddProperty("REFLECTIVITY",AllPhotonEnergies,Al_refl,nEntries_UMD);
  AlSurfaceProperty->AddProperty("EFFICIENCY",AllPhotonEnergies,quartz_EFIC,nEntries_UMD);
  AlSurface->SetMaterialPropertiesTable(AlSurfaceProperty);

  TileSurface = new G4OpticalSurface("TileSurface",unified, polished, dielectric_dielectric,1);

  G4MaterialPropertiesTable* TileSurfaceProperty = new G4MaterialPropertiesTable();
  TileSurfaceProperty->AddProperty("RINDEX",AllPhotonEnergies,quartz_RIND,nEntries_UMD);
  //TileSurfaceProperty->AddProperty("REFLECTIVITY",AllPhotonEnergies,reflectivity,nEntries_UMD);
  TileSurfaceProperty->AddProperty("EFFICIENCY",AllPhotonEnergies,quartz_EFIC,nEntries_UMD);
  TileSurface->SetMaterialPropertiesTable(TileSurfaceProperty);

  photonDetSurface = new G4OpticalSurface("PhotonDetSurface", glisur, ground, dielectric_metal,1);

  G4MaterialPropertiesTable* photonDetSurfaceProperty = new G4MaterialPropertiesTable();

  G4double p_mppc[] = {2.00*eV, 3.47*eV};
  const G4int nbins = sizeof(p_mppc)/sizeof(G4double);
  G4double refl_mppc[] = {1, 1};
  assert(sizeof(refl_mppc) == sizeof(p_mppc));
  G4double effi_mppc[] = {1, 1};
  assert(sizeof(effi_mppc) == sizeof(p_mppc));

  photonDetSurfaceProperty->AddProperty("REFLECTIVITY",p_mppc,refl_mppc,nbins);
  photonDetSurfaceProperty->AddProperty("EFFICIENCY",p_mppc,effi_mppc,nbins);

  photonDetSurface->SetMaterialPropertiesTable(photonDetSurfaceProperty);

  f->Close();
  delete f;
}

Materials::~Materials(void)
{
  delete H;
  delete N;
  delete O;
  delete Si;
  delete C;

  delete fiberClad;
  delete Xenon;
  delete pQuartz;
  delete SilicaCore_UI;
  delete SilicaClad_UI;
  delete Kapton_UI;
  delete EM_Quartz;
  delete Air;
  delete Al;
  delete Cu;
  delete Pb;
  delete pureW;
  delete NiW;
  delete Steel;
  delete Polyethylene;
  delete PMMA;
  delete Kapton;
  delete Grease;

  for (unsigned int i = 0; i < MPT_Array.size(); i++)
    delete MPT_Array.at(i);

  MPT_Array.clear();
}


void Materials::SetWavelengthRange(G4double _low, G4double _high){
  //E_photon = hc/wavelength. hc = 1240 eV*nm
  SetEnergyRange(1240.0/_high,1240.0/_low);
}


void Materials::SetEnergyRange(G4double _low, G4double _high){
  G4cout << "Elow = " << Elow << " deltaE = " << deltaE << G4endl;
  if(Elow > 0.0 || deltaE > 0.0){
    G4cout << "ERROR in Materials::Set(Energy/Wavelength)Range: Wavelength/Energy already set. Igonring this command" << G4endl;
    return;
  }else if(_low < 0.0 || _high < 0.0 || _high < _low){
    G4cout << "ERROR in Materials::Set(Energy/Wavelength)Range: Negative numbers or wrong order." << G4endl;
    return;
  }

  //No errors, set the values
  Elow = _low;
  deltaE = (_high-_low)/nEntries_UI;
  G4cout << "Elow = " << Elow << " deltaE = " << deltaE << G4endl;
}
