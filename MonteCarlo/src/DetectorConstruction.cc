// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Author: Chad Lantz

/** \ingroup mc
    \file DetectorConstruction.cc
    \class DetectorConstruction
    \brief Detector Construction

  DetectorConstruction is responsible for coordinating the placement of detectors in the scene.
  There are two ways in which geometry can be placed. The method is set in geometry.mac via the
  /Detector/ForcePosition command which is false by default, but can be set to true by the user for manual placement.
  - Manual Placement (ForcePosition == true) uses UI commands in geometry.mac to:
    - Create the world volume
    - Create detectors
    - Specify detector geometry and physical material
    - Specify detector placement

  -Auto placement (ForcePosition == false) uses prototype detector geometry and materials (hard coded)
  and placement from the 2018 test beam which is gathered from the Alignment file contained in the Utils folder.
  Alternate alignments can be specified with the /Detector/ConfigFile command in geometry.mac.

  ## Sequence of events for manual placement
  - DetectorConstruction::Construct is called by G4RunManager
  - Geometry stores are cleared if for some reason Construct has been called before
  - geometry.mac is executed, within which
    - The world volume is created via /Detector/SetWorldDimensions (Mandatory)
    - Detectors are added (object created) via the /Detector/ZDC/Add or /Detector/RPD/Add commands
    - Detector parameters are passed to the detector objects via the relevant UI command
  - ManualConstruction is called which loops over all added detectors and calls Construct for each.
  Additionally, the ZDC absorbers are added to a region for physics parameterization
  - DetectorConstruction::ConstructSDandField is called by G4RunManager, within which
    - The ZDC absorber region shower parameterization is declared
    - ConstructSDandField is called for each detector added by the user

  ## Sequence of events for manual placement
  - DetectorConstruction::Construct is called by G4RunManager
  - Geometry stores are cleared if for some reason Construct has been called before
  - geometry.mac is executed, within which
    - ForcePosition is set to true
    - An alignment file is chosen (or left as default)
    - A TestBeam run number is chosen to be simulated
  - ConstructSPSTestBeam is called during which,
    - Parameters specific to the SPS test area are set
    - The world volume is constructed
    - The configuration and alignment files are loaded. Configuration determines
      which detectors are to be used. Alignment determines the position. The information
      from each is then translated from the SPS/Desy table coordinate system to something we can use.
    - Detectors are constructed with parameters corresponding to the detectors tested in 2018
    - The ZDC absorbers are added to a region for physics parameterization
    - A lead target is constructed if specified by the config file
    - A lead block is constructed in front of the detectors if specified by the config file
  - DetectorConstruction::ConstructSDandField is called by G4RunManager, within which
    - The magnetic field from the Goliath magnet is constructed
    - The ZDC absorber region shower parameterization is declared
    - ConstructSDandField is called for each detector added by the user

  # UI commands and their functions
  ## Commands for the world
  - <b>/Detector/ReducedTree</b> true/false
    - Sets global ReducedTree mode. Used for test beam construction

  - <b>/Detector/Optical</b> true/false
    - Sets global optical flag. Used for test beam construction. Detectors will only record optical photons if true

  - <b>/Detector/Overlaps</b> true/false
    - Check geometry for overlaps if true

  - <b>/Detector/PI0</b> true/false
    - I'm not sure, ask Aric

  - <b>/Detector/RunNumber</b> num
    - The TestBeam run to be simulated

  - <b>/Detector/PrintDebugStatement</b> string
    - Prints the string to help debug errors

  - <b>/Detector/ForcePosition</b> true/false
    - Detectors will be constructed according to UI commands if true

  - <b>/Detector/ConfigFile</b> file name
    - Chooses the Alignment file (I should probably change the name of the command)

  - <b>/Detector/SetWorldDimensions</b> x y z unit
    - Sets the dimensions of the world volume AND constructs it. Dimensions are full length, not half

  ## Commands common to both detector types (XXX is either ZDC or RPD)
  - <b>/Detector/XXX/Add</b>
    - Add a new XXX type detector numbering starts at 1

  - <b>/Detector/XXX/Duplicate</b> num
    - Make another detector based on XXX num (eg ZDC 1)

  - <b>/Detector/XXX/SetCurrent</b> num
    - Set the XXX type detector number num as the current detector being referred to
     any subsequent /Detector/XXX/command commands will be applied to XXX num

  - <b>/Detector/XXX/Optical</b> true/false
    - Optical (Cherenkov) are killed upon creation in this detector. Selectable on a per detector basis

  - <b>/Detector/XXX/Position</b> x y z unit
    - Set the position of the detector

  - <b>/Detector/XXX/HousingThickness</b> thickness unit
    - Set the housing (case) thickness of the detector

  - <b>/Detector/XXX/ReducedTree</b> true/false
    - Sets the ouput tree to have fewer branches for large productions on a per detector basis

  - <b>/Detector/XXX/CheckOverlaps</b> false
    - Check the individual detector geometry for overlaps

  ## ZDC commands
  - <b>/Detector/ZDC/AbsorberDimensions</b> x y z unit
    - Set the tungsten absorber dimensions

  - <b>/Detector/ZDC/nAbsorbers</b> num
    - Set the number of absorbers in this ZDC (stacked in Z)

  - <b>/Detector/ZDC/GapThickness</b> gap unit
    - Set the distance between absorbers

  - <b>/Detector/ZDC/SteelAbsorberHeight</b> height unit
    - Set the height of the steel absorbers which are placed above the tungsten absorbers

  - <b>/Detector/ZDC/HousingMaterial</b> aluminum/steel
    - Chose between aluminum and steel housing material

  - <b>/Detector/ZDC/AbsorberMaterial</b> composite/pure
    - Chose between nickel tungsten composite or pure tungsten absorber material

  - <b>/Detector/ZDC/ZDCtype</b> ATLASHAD/TB2021EM
    - Chose between the the panflute prototype design and the CMS design

  ## RPD commands
  - <b>/Detector/RPD/FiberDiameters</b> core cladding buffer unit
    - Set the core, cladding, and buffer diameters of the optical fibers

  - <b>/Detector/RPD/FiberPitchX</b> distance unit
    - Set the center to center distance of fibers in x

  - <b>/Detector/RPD/FiberPitchZ distance</b> unit
    - Set the center to center distance of fibers in z

  - <b>/Detector/RPD/TileSize</b> size unit
    - Set the side length of the RPD tiles. Tiles are always square

  - <b>/Detector/RPD/MinWallThickness</b> thickness unit
    - Set the minimum allowable thickness of the aluminum material between adjacent fibers

  - <b>/Detector/RPD/FiberReadoutDistance</b> distance unit
    - Set the distance between the top of the top tile and the top of the fiber (PMT window)

  - <b>/Detector/RPD/RPDtype</b> panflute/CMS/TB2021PF
    - Chose between the the 2018 or 2021 panflute prototype design and the CMS tile design

*/


#include "DetectorConstruction.hh"
#include "DetectorMessenger.hh"

#include "G4GeometryManager.hh"
#include "G4SolidStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4EqMagElectricField.hh"
#include "PurgMagTabulatedField3D.hh"
#include "G4ChordFinder.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4PVParameterised.hh"
#include "G4PVReplica.hh"
#include "G4UniformMagField.hh"
#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4SimpleHeum.hh"
#include "G4ClassicalRK4.hh"
#include "G4HelixExplicitEuler.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4CashKarpRKF45.hh"
#include "G4RKG3_Stepper.hh"

//fast simulation
#include "GFlashHomoShowerParameterisation.hh"
#include "G4FastSimulationManager.hh"
#include "GFlashShowerModel.hh"
#include "GFlashHitMaker.hh"
#include "GFlashParticleBounds.hh"

#include "Math/GenVector/Quaternion.h"

#include <G4String.hh>
#include <iostream>
#include <stdio.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction(G4bool _GFlash)
  : G4VUserDetectorConstruction(),
  m_solidWorld(NULL),
  m_logicWorld(NULL),
  m_physWorld(NULL),
  m_leadBlockPos(0),
  m_leadBlockSize(new G4ThreeVector(100*mm,150*mm,40*mm)),
  m_alignment(NULL),
  m_TRI(0),
  m_GFlash(_GFlash),
  OPTICAL(false),
  ForceDetectorPosition(false),
  PI0(false)
{
  new DetectorMessenger(this);
  currentRPD = -1;
  currentZDC = -1;
  m_materials = Materials::getInstance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct(){
  if ( m_physWorld ) {
    G4GeometryManager::GetInstance()->OpenGeometry();
    G4PhysicalVolumeStore::GetInstance()->Clean();
    G4LogicalVolumeStore::GetInstance()->Clean();
    G4SolidStore::GetInstance()->Clean();
    G4LogicalSkinSurface::CleanSurfaceTable();
    G4LogicalBorderSurface::CleanSurfaceTable();
  }

  G4UImanager* UImanager = G4UImanager::GetUIpointer();
  UImanager->ExecuteMacroFile("geometry.mac");
  m_materials->DefineOpticalProperties();

  if( ForceDetectorPosition ){
    ManualConstruction();
  }else{
    //Clear any existing geometry
    for(ModTypeZDC *zdc : m_ZDCvec) delete zdc;
    m_ZDCvec.clear();
    currentZDC = -1;
    for(ModTypeRPD *rpd : m_RPDvec) delete rpd;
    m_RPDvec.clear();
    currentRPD = -1;
    if(m_TRI) delete m_TRI;
    m_TRI = 0;

    if(m_year == 2023) Construct2023SPSTestBeam();
    if(m_year == 2022) Construct2022SPSTestBeam();
    if(m_year == 2021) Construct2021SPSTestBeam();
    if(m_year == 2018) Construct2018SPSTestBeam();
  }
  return m_physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::ConstructWorldVolume(G4double x, G4double y, G4double z){
  printf( "Building world with x %5.1f y %5.1f z %5.1f\n", x, y, z );

  m_solidWorld =
    new G4Box("World", 0.5*x, 0.5*y, 0.5*z );

  m_logicWorld =
    new G4LogicalVolume(m_solidWorld,     //its solid
                        m_materials->Air, //its material
                        "World");         //its name

  m_physWorld =
    new G4PVPlacement(0,                  //no rotation
                      G4ThreeVector(),    //at (0,0,0)
                      m_logicWorld,       //its logical volume
                      "World",            //its name
                      0,                  //its mother  volume
                      false,              //no boolean operation
                      0,                  //copy number
                      false);             //overlaps checking


  G4VisAttributes* boxVisAtt_world = new G4VisAttributes(G4Colour(0.5,0.5,0.5));

  m_logicWorld ->SetVisAttributes(boxVisAtt_world);

  return m_physWorld;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::ManualConstruction(){

  std::cout << "\n******************************************" << std::endl
            << "*       PLACING DETECTORS MANUALLY       *" << std::endl
            << "******************************************\n" << std::endl;

  G4ThreeVector* pos;
  char name[32];
  for(ModTypeZDC* zdc : m_ZDCvec){
    pos = zdc->GetPosition();
    printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
    zdc->Construct();

    //Create a region for this module's tungsten absorbers
    sprintf(name,"ZDC%d_absorber", zdc->GetModNum());
    m_Region.push_back( new G4Region( name ) );
    zdc->GetAbsorberLogicalVolume()->SetRegion(m_Region.back());
    m_Region.back()->AddRootLogicalVolume(zdc->GetAbsorberLogicalVolume());
  }

  for(ModTypeRPD* rpd : m_RPDvec){
    pos = rpd->GetPosition();
    printf( "RPD%d center = (%f,%f,%f)\n", rpd->GetModNum(), pos->x(), pos->y(), pos->z() );
    rpd->Construct();
  }
  if(isTrigger){
    pos = m_TRI->GetPosition();
    printf( "Trigger%d center = (%f,%f,%f)\n", 1, pos->x(), pos->y(), pos->z() );
    m_TRI ->Construct();
  }
  if(m_leadBlockPos && m_leadBlockSize){
    ConstructLeadBlock();//Constructs using m_leadBlockPos and m_leadBlockSize by default
  }
  return m_physWorld;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct2021SPSTestBeam(){

  std::cout << "\n******************************************" << std::endl
            << "*    BUILDING 2021 TESTBEAM, RUN " << std::left << setw(6) << m_runNumber << "  *" << std::endl
            << "******************************************\n" << std::endl;

  LoadAlignmentFile2021();
  LoadSurveyFile2021();

  G4double worldSizeX = 2000*mm;
  G4double worldSizeY = 2000*mm;
  G4double worldSizeZ = 32000*mm;

  if(m_logicWorld){
    m_solidWorld->SetXHalfLength(worldSizeX);
    m_solidWorld->SetYHalfLength(worldSizeY);
    m_solidWorld->SetZHalfLength(worldSizeZ);
  }else{
    ConstructWorldVolume( worldSizeX, worldSizeY, worldSizeZ );
  }

  vector< G4String > detectors = {m_alignment2021->Det1, m_alignment2021->Det2,
                                  m_alignment2021->Det3, m_alignment2021->Det4,
                                  m_alignment2021->Det5, m_alignment2021->Det6};

  G4cout << "Run " << m_alignment2021->runNumber << " contains the following detectors:";
  for( uint det = 0; det < detectors.size(); det++){
    if(detectors[det] != "none"){
      G4cout << detectors[det] << ", ";
      if(detectors[det] == "ZDC"){
        detectors[det] = "HAD";
      }
    }
  }
  G4cout << G4endl;

  std::vector< G4String > finishedHads;
  G4ThreeVector* pos;
  for( Survey2021* survey : m_surveyEntries2021 ){
    for( G4String det : detectors){
      if( G4StrUtil::contains(survey->detector, det) ){

        /********** ATLAS run3 HAD module ************/
        if( G4StrUtil::contains(det, "HAD") ){
          //Check if this had has already been constructed
          G4bool finished = false;
          for( G4String done : finishedHads ){
            if(survey->detector == done) finished = true;
          }
          if(finished) break;

          AddZDC( new G4ThreeVector(  m_alignment2021->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2021->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm ) );
          ModTypeZDC* zdc = m_ZDCvec.back();
          zdc->SetDetectorType("ATLASHAD");
          zdc->SetReducedTreeFlag(REDUCED_TREE);
          zdc->SetOpticalFlag(OPTICAL);
          pos = zdc->GetPosition();
          printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
          zdc->Construct();
          finishedHads.push_back(survey->detector);

        /********** 2021 Test Beam EM Prototype ************/
        }else if( G4StrUtil::contains(det, "UEM") ){
          //Determine if the module was 1/3 full, or completely full of absorber
          int run = m_alignment2021->runNumber;
          float zOffset = 0;
          std::string type = "TB2021EM";
          if( (run > 0 && run < 117) || (run >=490 && run < 621 ) || (run >=847 && run < 70181 ) ){ //1/3 module
            type += "THIRD";
            zOffset = 50.1; //Middle of the rear third of absorber/radiator volume
          }
          AddZDC( new G4ThreeVector(  m_alignment2021->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2021->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm + zOffset ) );
          ModTypeZDC* zdc = m_ZDCvec.back();
          zdc->SetDetectorType(type.c_str());
          zdc->SetReducedTreeFlag(REDUCED_TREE);
          zdc->SetOpticalFlag(OPTICAL);
          pos = zdc->GetPosition();
          printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
          zdc->Construct();

        /********** 2021 Test Beam Pan Flute RPD ************/
        }else if( G4StrUtil::contains(det, "PFRPD") ){
          AddRPD( new G4ThreeVector(  m_alignment2021->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2021->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm ) );
          ModTypeRPD* rpd = m_RPDvec.back();
          rpd->SetDetectorType("TB2021PF");
          rpd->SetReducedTreeFlag(REDUCED_TREE);
          rpd->SetOpticalFlag(OPTICAL);
          pos = rpd->GetPosition();
          printf( "RPD%d center = (%f,%f,%f)\n", rpd->GetModNum(), pos->x(), pos->y(), pos->z() );
          rpd->Construct();

        }//end detector type checks
      }//end detector loop
    }//end if G4StrUtil::contains(survey::detector, det)

    /********** 2021 Test Beam Trigger Paddles ************/
    if( G4StrUtil::contains(survey->detector, "TRI") ){
      AddTRI( new G4ThreeVector(  m_alignment2021->x_trig_table*mm - survey->trig_table.x()*mm + survey->pos.x()*mm,
                                  m_alignment2021->y_trig_table*mm - survey->trig_table.y()*mm + survey->pos.y()*mm,
                                  survey->pos.z()*mm ) );
      pos = m_TRI->GetPosition();
      printf( "Trigger center = (%f,%f,%f)\n", pos->x(), pos->y(), pos->z() );
      m_TRI->Construct();
    }
  }//end survey loop
  G4cout << "2021 Test Beam Construction Completed!!!" << G4endl;
  return m_physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct2022SPSTestBeam(){

  std::cout << "\n******************************************" << std::endl
            << "*    BUILDING 2022 TESTBEAM, RUN " << std::left << setw(6) << m_runNumber << "  *" << std::endl
            << "******************************************\n" << std::endl;

  LoadAlignmentFile2022();
  LoadSurveyFile2022();

  G4double worldSizeX = 2000*mm;
  G4double worldSizeY = 2000*mm;
  G4double worldSizeZ = 10000*mm;
  G4double worldOffsetZ = 15500*mm; //There was a second 15m beam pipe that was ~0.5m from the main pipe

  if(m_logicWorld){
    m_solidWorld->SetXHalfLength(worldSizeX);
    m_solidWorld->SetYHalfLength(worldSizeY);
    m_solidWorld->SetZHalfLength(worldSizeZ);
  }else{
    ConstructWorldVolume( worldSizeX, worldSizeY, worldSizeZ );
  }

  G4ThreeVector* pos;
  G4cout << "Phase = " << m_alignment2022->phase << G4endl;

  //We can safely construct the trigger cross in all situations
  for( Survey2022* survey : m_surveyEntries2022 ){
    if(survey->detector == "TRIGGER"){
      //The position of the trigger was surveyed and never moved
      AddTRI( new G4ThreeVector(  survey->pos.x()*mm, survey->pos.y()*mm, survey->pos.z()*mm - worldOffsetZ ) );
      pos = m_TRI->GetPosition();
      printf( "Trigger center = (%f,%f,%f)\n", pos->x(), pos->y(), pos->z() );
      m_TRI->Construct();
      break;
    }
  }

  if( m_alignment2022->phase == 1 ){
    G4cout << "Building phase 1 configuration" << G4endl;
    for( Survey2021* survey : m_surveyEntries2021 ){
      if( survey->detector == m_alignment2022->detector.c_str() ){
        AddZDC( new G4ThreeVector(  m_alignment2022->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                    m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                    survey->pos.z()*mm - worldOffsetZ ), &survey->rotation);
        ModTypeZDC* zdc = m_ZDCvec.back();
        zdc->SetDetectorType("ATLASHAD");
        zdc->SetReducedTreeFlag(REDUCED_TREE);
        zdc->SetOpticalFlag(OPTICAL);
        pos = zdc->GetPosition();
        printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
        zdc->Construct();
      }
    }
  }else if( m_alignment2022->phase == 2 || m_alignment2022->phase == 3){
    G4cout << "Building phase " << m_alignment2022->phase << " configuration" << G4endl;
    G4String det(m_alignment2022->detector);
    G4String arm = (G4StrUtil::contains(det,"1-2")) ? "1-2" : "8-1"; //Set which arm to look for in the survey entries
    G4cout << "Constructing Arm " << arm << G4endl;

    for( Survey2022* survey : m_surveyEntries2022 ){
      G4String surveyDet(survey->detector);
      if( G4StrUtil::contains(surveyDet, arm) ){
        if( G4StrUtil::contains(surveyDet, "HAD") || G4StrUtil::contains(surveyDet, "EM") ){
          AddZDC( new G4ThreeVector(  m_alignment2022->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm - worldOffsetZ ), &survey->rotation);
          ModTypeZDC* zdc = m_ZDCvec.back();
          zdc->SetDetectorType("ATLASHAD");
          zdc->SetReducedTreeFlag(REDUCED_TREE);
          zdc->SetOpticalFlag(OPTICAL);
          pos = zdc->GetPosition();
          printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
          zdc->Construct();
        }else if( G4StrUtil::contains(surveyDet, "RPD") ){
          AddRPD( new G4ThreeVector(  m_alignment2022->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm - worldOffsetZ ), &survey->rotation );
          ModTypeRPD* rpd = m_RPDvec.back();
          rpd->SetDetectorType("TB2021PF");
          rpd->SetReducedTreeFlag(REDUCED_TREE);
          rpd->SetOpticalFlag(OPTICAL);
          pos = rpd->GetPosition();
          printf( "RPD%d center = (%f,%f,%f)\n", rpd->GetModNum(), pos->x(), pos->y(), pos->z() );
          rpd->Construct();
        }else if( G4StrUtil::contains(surveyDet, "LEAD") ){
          ConstructLeadBlock( new G4ThreeVector(150, 250, 50), //size of the block
                              new G4ThreeVector( m_alignment2022->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
                                                 m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                                 survey->pos.z()*mm - worldOffsetZ ) );
          cout << "Placed Lead Block at Z = " << survey->pos.z()*mm - worldOffsetZ << "mm" <<  std::endl;
        }else{ //This would be the EM
          G4cout << "Prototype EM module not implemented" << G4endl;
        }//end detector types
      }//end if G4StrUtil::contains(survey, arm)
    }//end loop over surveys
  }


  G4cout << "2022 Test Beam Construction Completed!!!" << G4endl;
  return m_physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct2023SPSTestBeam(){

  std::cout << "\n******************************************" << std::endl
            << "*    BUILDING 2023 TESTBEAM, RUN " << std::left << setw(6) << m_runNumber << "  *" << std::endl
            << "******************************************\n" << std::endl;

  //The survey and alignment files are the same format as 2022, so use those tools
  LoadAlignmentFile2022( std::getenv("JZCaPA") + std::string("/Utils/Alignment_2023.xml") );
  LoadSurveyFile2022( std::getenv("JZCaPA") + std::string("/Utils/Survey_2023.xml") );

  G4double worldSizeX = 2000*mm;
  G4double worldSizeY = 2000*mm;
  G4double worldSizeZ = 10000*mm;
  G4double worldOffsetZ = -12931.2*mm; //There was a second 15m beam pipe that was ~0.5m from the main pipe 

  if(m_logicWorld){
    m_solidWorld->SetXHalfLength(worldSizeX);
    m_solidWorld->SetYHalfLength(worldSizeY);
    m_solidWorld->SetZHalfLength(worldSizeZ);
  }else{
    ConstructWorldVolume( worldSizeX, worldSizeY, worldSizeZ );
  }

  G4ThreeVector* pos;
  G4cout << "Phase = " << m_alignment2022->phase << G4endl;

  //We can safely construct the trigger cross in all situations
  for( Survey2022* survey : m_surveyEntries2022 ){
    if(survey->detector == "TRIGGER"){
      //The position of the trigger was surveyed and never moved
      AddTRI( new G4ThreeVector(  survey->pos.x()*mm, survey->pos.y()*mm, survey->pos.z()*mm - worldOffsetZ ) );
      pos = m_TRI->GetPosition();
      printf( "Trigger center = (%f,%f,%f)\n", pos->x(), pos->y(), pos->z() );
      m_TRI->Construct();
      break;
    }
  }

  // if( m_alignment2022->phase == 1 ){
  //   G4cout << "Building phase 1 configuration" << G4endl;
  //   for( Survey2021* survey : m_surveyEntries2021 ){
  //     if( survey->detector == m_alignment2022->detector.c_str() ){
  //       AddZDC( new G4ThreeVector(  m_alignment2022->x_det_table*mm - survey->table.x()*mm + survey->pos.x()*mm,
  //                                   m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
  //                                   survey->pos.z()*mm - worldOffsetZ ), &survey->rotation);
  //       ModTypeZDC* zdc = m_ZDCvec.back();
  //       zdc->SetDetectorType("ATLASHAD");
  //       zdc->SetReducedTreeFlag(REDUCED_TREE);
  //       zdc->SetOpticalFlag(OPTICAL);
  //       pos = zdc->GetPosition();
  //       printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
  //       zdc->Construct();
  //     }
  //   }
  // }else if( m_alignment2022->phase == 2 || m_alignment2022->phase == 3){
    G4cout << "Building phase " << m_alignment2022->phase << " configuration" << G4endl;
    G4String det(m_alignment2022->detector);
    G4String arm = (G4StrUtil::contains(det, "1-2")) ? "1-2" : "8-1"; //Set which arm to look for in the survey entries
    G4cout << "Constructing Arm " << arm << G4endl;

    for( Survey2022* survey : m_surveyEntries2022 ){
      G4String surveyDet(survey->detector);
      // if( G4StrUtil::contains(surveyDet, arm) ){
      if( true ){
        //ATLAS ZDC Modules
        if( G4StrUtil::contains(surveyDet, "HAD") || (G4StrUtil::contains(surveyDet, "EM") && !G4StrUtil::contains(surveyDet, "PROTO")) ){
          AddZDC( new G4ThreeVector( -m_alignment2022->x_det_table*mm + survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm - worldOffsetZ ), &survey->rotation);
          ModTypeZDC* zdc = m_ZDCvec.back();
          zdc->SetDetectorType("ATLASHAD");
          zdc->SetDetectorName(survey->detector);
          zdc->SetReducedTreeFlag(REDUCED_TREE);
          zdc->SetOpticalFlag(OPTICAL);
          pos = zdc->GetPosition();
          printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
          zdc->Construct();

        //RPD
        }else if( G4StrUtil::contains(surveyDet, "RPD") ){
          AddRPD( new G4ThreeVector( -m_alignment2022->x_det_table*mm + survey->table.x()*mm + survey->pos.x()*mm,
                                      m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                      survey->pos.z()*mm - worldOffsetZ ), &survey->rotation );
          ModTypeRPD* rpd = m_RPDvec.back();
          rpd->SetDetectorType("TB2021PF");
          rpd->SetDetectorName(survey->detector);
          rpd->SetReducedTreeFlag(REDUCED_TREE);
          rpd->SetOpticalFlag(OPTICAL);
          pos = rpd->GetPosition();
          printf( "RPD%d center = (%f,%f,%f)\n", rpd->GetModNum(), pos->x(), pos->y(), pos->z() );
          rpd->Construct();

        //Lead block
        }else if( G4StrUtil::contains(surveyDet, "LEAD") ){
          ConstructLeadBlock( new G4ThreeVector(150, 250, 50), //size of the block
                              new G4ThreeVector(-m_alignment2022->x_det_table*mm + survey->table.x()*mm + survey->pos.x()*mm,
                                                 m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                                 survey->pos.z()*mm - worldOffsetZ ) );
          cout << "Placed Lead Block at Z = " << survey->pos.z()*mm - worldOffsetZ << "mm" <<  std::endl;

        //Prototype EM
        }else if( G4StrUtil::contains(surveyDet, "PROTO") ){ //This would be the EM
          AddZDC( new G4ThreeVector(-m_alignment2022->x_det_table*mm + survey->table.x()*mm + survey->pos.x()*mm,
                                     m_alignment2022->y_det_table*mm - survey->table.y()*mm + survey->pos.y()*mm,
                                     survey->pos.z()*mm - worldOffsetZ ), &survey->rotation);
          ModTypeZDC* zdc = m_ZDCvec.back();
          zdc->SetDetectorType("TB2021EMTHIRD");
          zdc->SetDetectorName(survey->detector);
          zdc->SetReducedTreeFlag(REDUCED_TREE);
          zdc->SetOpticalFlag(OPTICAL);
          pos = zdc->GetPosition();
          printf( "ZDC%d center = (%f,%f,%f)\n", zdc->GetModNum(), pos->x(), pos->y(), pos->z() );
          zdc->Construct();
        }//end detector types
      }//end if G4StrUtil::contains(survey, arm)
    }//end loop over surveys
  // }


  G4cout << "2023 Test Beam Construction Completed!!!" << G4endl;
  return m_physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct2018SPSTestBeam(){


  std::cout << "\n******************************************" << std::endl
            << "*    BUILDING 2018 TESTBEAM, RUN " << std::left << setw(6) << m_runNumber << "  *" << std::endl
            << "******************************************\n" << std::endl;

  // Create variables to be used in beamtest 2018 simulation
  G4ThreeVector zdc1Pos,zdc2Pos, rpdPos;
  bool ZDC1 = false, ZDC2 = false, RPD = false;
  G4double firstDetZ, detX, detY, detZ,
           tableX_shift=0, tableY_shift=0;
  G4Material* Lead = m_materials->Pb;
  G4double worldSizeX = 180*mm;
  G4double worldSizeY = 1200*mm;
  G4double worldSizeZ = 32000*mm;
  G4double worldZoffset= worldSizeZ/2.0;
  std::string detector[3];

  //################################ World volume construction

  if(m_logicWorld){
    m_solidWorld->SetXHalfLength(worldSizeX);
    m_solidWorld->SetYHalfLength(worldSizeY);
    m_solidWorld->SetZHalfLength(worldSizeZ);
  }else{
    ConstructWorldVolume( worldSizeX, worldSizeY, worldSizeZ );
  }

  //################################ SURVEY/ALIGNMENT_SETUP

  LoadConfigurationFile();
  LoadAlignmentFile();

  //table(-2250,500) -> rpd/beam(0,0)	where 100=0.1cm in table coordinates
  //-320mm is offset to get from zdc mid to active area mid
  tableX_shift = (-2250.0 - (m_alignment->x_table)  )/100*mm ;//2257 more accurate
  tableY_shift = (500.0   - (m_alignment->y_table)  )/100*mm ;//501  more accurate
  Survey *srvy_rpd = GetSurvey("RPD");

  detector[0]=m_alignment->upstream_Det;
  detector[1]=m_alignment->mid_Det;
  detector[2]=m_alignment->downstream_Det;

  for(int i=0; i<3; i++){
    if(detector[i]=="ZDC1") {ZDC1 = true;}
    if(detector[i]=="ZDC2") {ZDC2 = true;}
    if(detector[i]=="RPD" ) {RPD  = true;}
  }

  firstDetZ = worldSizeZ;
  for(Survey* survey : m_surveyEntries){
    detX = (survey->x_pos*1000.0)*mm;
    detY = (survey->y_pos*1000.0)*mm;
    detZ = (survey->z_pos*1000.0 - worldZoffset )*mm;

    if(detZ < firstDetZ) firstDetZ = detZ;

    if( survey->detector == "ZDC1" && ZDC1 ){
      AddZDC( new G4ThreeVector( detX + ( tableX_shift       - srvy_rpd->x_pos )*mm,
                                 detY + ( tableY_shift - 320 - srvy_rpd->y_pos )*mm,
                                 detZ ) );
    } else if( survey->detector == "ZDC2" && ZDC2 ){
      AddZDC( new G4ThreeVector( detX + ( tableX_shift       - srvy_rpd->x_pos )*mm,
                                 detY + ( tableY_shift - 320 - srvy_rpd->y_pos )*mm,
                                 detZ ) );
    }else if( survey->detector == "RPD" && RPD ){
      AddRPD( new G4ThreeVector( tableX_shift, tableY_shift, detZ) );
    }
  }


  G4ThreeVector* pos;
  G4int modNum;
  char name[32];
  for(ModTypeZDC* zdc : m_ZDCvec){
    zdc->SetFiberDiameters    ( new G4ThreeVector(1.5*mm,0.0,0.0) );
    zdc->SetAbsorberDimensions( new G4ThreeVector(90.0*mm, 180.0*mm, 11.0*mm) );
    zdc->SetnAbsorbers        ( 11 );
    zdc->SetHousingThickness  ( 4.5*mm );
    zdc->SetGapThickness      ( 2.5*mm );
    zdc->SetHousingMaterial   ( "aluminum" );
    zdc->SetAbsorberMaterial  ( "pure" );

    pos = zdc->GetPosition();
    modNum = zdc->GetModNum();
    printf( "ZDC%d center = (%f,%f,%f)\n", modNum, pos->x(), pos->y(), pos->z() );
    zdc->Construct();

    //Create a region for this module's tungsten absorbers
    sprintf(name,"ZDC%d_absorber", zdc->GetModNum());
    m_Region.push_back( new G4Region( name ) );
    zdc->GetAbsorberLogicalVolume()->SetRegion(m_Region.back());
    m_Region.back()->AddRootLogicalVolume(zdc->GetAbsorberLogicalVolume());
  }

  for(ModTypeRPD* rpd : m_RPDvec){
    rpd->SetDetectorType  ( "cms" );

    pos = rpd->GetPosition();
    modNum = rpd->GetModNum();
    printf( "RPD%d center = (%f,%f,%f)\n", modNum, pos->x(), pos->y(), pos->z() );
    rpd->Construct();
  }
//################################ Get SURVEY/ALIGNMENT_END

  // Setup lead target
  if( m_alignment->target_In ){
    G4Box* leadTarget = new G4Box("Target",5*cm, 5*cm, 1.3*cm);

    logic_leadTarget
    = new G4LogicalVolume(leadTarget,
                          Lead,
                          "LeadTarget");

    new G4PVPlacement(0,
                      G4ThreeVector(0.0, 0.0, (2600-worldZoffset)*mm),
                      logic_leadTarget,
                      "LeadTarget1",
                      m_logicWorld,
                      false,
                      0
                      );

    // Visualization attributes
    G4VisAttributes* boxVisAtt_lead= new G4VisAttributes(G4Colour(1.0,0.0,1.0,1.0)); //magenta
    logic_leadTarget ->SetVisAttributes(boxVisAtt_lead);

   }
   if( m_alignment->lead_In ){
     // Assign lead block position in mm, place approx 1 ft in front of ZDC1
     // half ZDC Z width = 90mm
     // 1ft ~ 300mm
     G4double leadblockZ = ( firstDetZ - 90 - 300)*mm;

     ConstructLeadBlock(new G4ThreeVector( (0.5*worldSizeX)*mm, (0.5*150)*mm, 10*cm), new G4ThreeVector(0.0, 0.0, (leadblockZ)*mm) );

    cout << "Placed Lead Block at Z = " << leadblockZ*mm << "mm" <<  std::endl;
  }
  return m_physWorld;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField( ){
  // Setup Goliath magnetic field (SPS test beam only)
  if( m_alignment && m_alignment->magnet_On ){
    G4double mag_zOffset = -9.55*m;
    //Field grid in A9.TABLE. File must be accessible from run directory.
    G4MagneticField* PurgMagField= new PurgMagTabulatedField3D((std::getenv("JZCaPA") + std::string("/Utils/PurgMag3D.TABLE")).c_str(), mag_zOffset+(32000*mm/2000.0));
    fField.Put(PurgMagField);

    //This is thread-local
    G4FieldManager* pFieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();

    pFieldMgr->SetDetectorField(fField.Get());
    pFieldMgr->CreateChordFinder(fField.Get());
  }

  // -- fast simulation models:
  // **********************************************
  // * Initializing shower model
  // **********************************************
  if(m_GFlash){
    for(G4Region* region : m_Region){
      G4cout << "Creating shower parameterization model for region: " << region->GetName() << G4endl;
      GFlashShowerModel* FastShowerModel = new GFlashShowerModel( "FastShowerModel", region);
      GFlashHomoShowerParameterisation Parameterisation( m_ZDCvec[0]->GetAbsorberMaterial() );
      FastShowerModel->SetParameterisation(Parameterisation);
      // Energy Cuts to kill particles:
      GFlashParticleBounds ParticleBounds;
      //Define the values below
      // GFlashParticleBounds::SetMinEneToParametrise (G4ParticleDefinition &particleType, G4double enemin);
      // GFlashParticleBounds::SetMaxEneToParametrise (G4ParticleDefinition &particleType, G4double enemax);
      // GFlashParticleBounds::SetEneToKill (G4ParticleDefinition &particleType, G4double enekill);
      FastShowerModel->SetParticleBounds(ParticleBounds);

    }
    G4cout<<"end shower parameterization."<<G4endl;
  }
  // **********************************************

  for(ModTypeZDC* zdc : m_ZDCvec){
    zdc->ConstructSDandField();
  }

  for(ModTypeRPD* rpd : m_RPDvec){
    rpd->ConstructSDandField();
  }

  if(isTrigger){
    m_TRI->ConstructSDandField();
  }
}
//***********************************************************************************
// READ Survey_2018.xml and Alignment_2018.xml
//***********************************************************************************
/**
 * @brief Reads the .xml configuration file and load characteristics for all the alignments, immediately sorted into alignment objects
 * @param _inFile
 */

void DetectorConstruction::LoadConfigurationFile( G4String _inFile  ){

  if(_inFile == ""){
    _inFile = std::getenv("JZCaPA");
    _inFile += "/Utils/Survey_2018.xml";
  }

    m_XMLparser = new XMLSettingsReader();


    if (!m_XMLparser->parseFile(_inFile)) {
      if(!m_XMLparser->parseFile("Survey_2018.xml")){
        std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
        return;
      }
      std::cout << " Found Survey_2018.xml in executable directory " << std::endl;
    }

    std::cout << "Loading .xml Configuration File..." << std::endl;
    std::cout << "Found " << m_XMLparser->getBaseNodeCount("Survey") << " survey entries " << std::endl;

    int first_run, last_run;

    for (int i = 0; i < m_XMLparser->getBaseNodeCount("Survey"); i++) { //this was unsigned int i = 0

        m_XMLparser->getChildValue("Survey",i,"start_run",first_run);
        m_XMLparser->getChildValue("Survey",i,"end_run",last_run);

        //Discard entries for any alignment that does not apply to our run
        if(m_runNumber < first_run || m_runNumber > last_run) continue;
          m_survey = new Survey();

        //If the entry applies, we store it in the vector
        m_XMLparser->getChildValue("Survey",i,"detector",m_survey->detector);
        m_XMLparser->getChildValue("Survey",i,"x_pos",m_survey->x_pos);
        m_XMLparser->getChildValue("Survey",i,"y_pos",m_survey->y_pos);
        m_XMLparser->getChildValue("Survey",i,"z_pos",m_survey->z_pos);
        m_XMLparser->getChildValue("Survey",i,"cos_x",m_survey->cos_x);
        m_XMLparser->getChildValue("Survey",i,"cos_y",m_survey->cos_y);
        m_XMLparser->getChildValue("Survey",i,"cos_z",m_survey->cos_z);

        m_surveyEntries.push_back(m_survey);
    }

    if(m_surveyEntries.size() == 0) std::cout << "WARNING: SURVEY NOT FOUND!!!" << std::endl;

    return;
}


/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
void DetectorConstruction::LoadAlignmentFile2022( G4String _inFile ){
    if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/Alignment_2022.xml");
    m_XMLparser = new XMLSettingsReader();

    if (!m_XMLparser->parseFile(_inFile)) {
      if (!m_XMLparser->parseFile("Alignment_2022.xml")) {
            std::cerr << " Could not parse file : " << _inFile << std::endl;
            return;
      }
    }

    m_alignment2022 = new Alignment2022();
    m_alignment2022->run = m_runNumber;

    std::cout << "Loading .xml Alignment File..." << std::endl;
    std::cout << "Found " << m_XMLparser->getBaseNodeCount("Alignment") << " alignment entries " << std::endl;
    std::cout << "Retrieving the information for run " << m_runNumber << std::endl;

    int run;
    for (int i = 0; i < m_XMLparser->getBaseNodeCount("Alignment"); i++) {
        m_XMLparser->getChildValue("Alignment",i,"run",run);
        if(run != m_runNumber) continue;
        std::cout << "Found Run Entry in Alignment file for run " << m_runNumber << std::endl;
        m_XMLparser->getChildValue("Alignment",i,"Shift_leader",  m_alignment2022->shiftLeader   );
        m_XMLparser->getChildValue("Alignment",i,"run",           m_alignment2022->run           );
        m_XMLparser->getChildValue("Alignment",i,"phase",         m_alignment2022->phase         );
        m_XMLparser->getChildValue("Alignment",i,"good_run",      m_alignment2022->good_run      );
        m_XMLparser->getChildValue("Alignment",i,"x_table",       m_alignment2022->x_det_table   );
        m_XMLparser->getChildValue("Alignment",i,"y_table",       m_alignment2022->y_det_table   );
        m_XMLparser->getChildValue("Alignment",i,"x_trigger",     m_alignment2022->x_trig_table  );
        m_XMLparser->getChildValue("Alignment",i,"y_trigger",     m_alignment2022->y_trig_table  );
        m_XMLparser->getChildValue("Alignment",i,"beam_energy",   m_alignment2022->beam_energy   );
        m_XMLparser->getChildValue("Alignment",i,"beam_type",     m_alignment2022->beam_type     );
        m_XMLparser->getChildValue("Alignment",i,"Detector",      m_alignment2022->detector      );
        m_XMLparser->getChildValue("Alignment",i,"ZDC_HV",        m_alignment2022->ZDC_HV        );
        m_XMLparser->getChildValue("Alignment",i,"ZDC_b1",        m_alignment2022->ZDC_b1        );
        m_XMLparser->getChildValue("Alignment",i,"ZDC_b2",        m_alignment2022->ZDC_b2        );
        m_XMLparser->getChildValue("Alignment",i,"ZDC_b3",        m_alignment2022->ZDC_b3        );
        m_XMLparser->getChildValue("Alignment",i,"RPD_HV1",       m_alignment2022->RPD_HV1       );
        m_XMLparser->getChildValue("Alignment",i,"RPD_HV2",       m_alignment2022->RPD_HV2       );
        m_XMLparser->getChildValue("Alignment",i,"RPD_HV3",       m_alignment2022->RPD_HV3       );
        m_XMLparser->getChildValue("Alignment",i,"RPD_HV4",       m_alignment2022->RPD_HV4       );
        m_XMLparser->getChildValue("Alignment",i,"EM_HV",         m_alignment2022->EM_HV         );
    }

    if(m_alignment2022 == NULL) std::cout << "WARNING: ALIGNMENT NOT FOUND!!!" << std::endl;
    delete m_XMLparser; m_XMLparser = NULL;
    return;

}

/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
void DetectorConstruction::LoadAlignmentFile2021( G4String _inFile ){
    if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/Alignment_2021.xml");
    m_XMLparser = new XMLSettingsReader();

    if (!m_XMLparser->parseFile(_inFile)) {
      if (!m_XMLparser->parseFile("Alignment_2021.xml")) {
            std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
            return;
      }
    }

    m_alignment2021 = new Alignment2021();
    m_alignment2021->runNumber = m_runNumber;

    std::cout << "Loading .xml Alignment File..." << std::endl;
    std::cout << "Found " << m_XMLparser->getBaseNodeCount("Alignment") << " alignment entries " << std::endl;
    std::cout << "Retrieving the information for run " << m_runNumber << std::endl;

    int run;
    for (int i = 0; i < m_XMLparser->getBaseNodeCount("Alignment"); i++) {
        m_XMLparser->getChildValue("Alignment",i,"run",run);
        if(run != m_runNumber) continue;
        std::cout << "Found Run Entry in Alignment file for run " << m_runNumber << std::endl;
        m_XMLparser->getChildValue("Alignment",i,"beam_scan",m_alignment2021->scanNumber);
        m_XMLparser->getChildValue("Alignment",i,"beam_type",m_alignment2021->beam_type);
        m_XMLparser->getChildValue("Alignment",i,"beam_energy",m_alignment2021->beam_energy);
        m_XMLparser->getChildValue("Alignment",i,"x_table",m_alignment2021->x_det_table);
        m_XMLparser->getChildValue("Alignment",i,"y_table",m_alignment2021->y_det_table);
        m_XMLparser->getChildValue("Alignment",i,"x_trigger",m_alignment2021->x_trig_table);
        m_XMLparser->getChildValue("Alignment",i,"y_trigger",m_alignment2021->y_trig_table);
        m_XMLparser->getChildValue("Alignment",i,"Det1",m_alignment2021->Det1);
        m_XMLparser->getChildValue("Alignment",i,"Det2",m_alignment2021->Det2);
        m_XMLparser->getChildValue("Alignment",i,"Det3",m_alignment2021->Det3);
        m_XMLparser->getChildValue("Alignment",i,"Det4",m_alignment2021->Det4);
        m_XMLparser->getChildValue("Alignment",i,"Det5",m_alignment2021->Det5);
        m_XMLparser->getChildValue("Alignment",i,"Det6",m_alignment2021->Det6);
    }

    if(m_alignment2021 == NULL) std::cout << "WARNING: ALIGNMENT NOT FOUND!!!" << std::endl;
    delete m_XMLparser; m_XMLparser = NULL;
    return;

}

/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
void DetectorConstruction::LoadAlignmentFile( G4String _inFile ){
  bool debug = false;

  if( _inFile == ""){
    _inFile = std::getenv("JZCaPA");
    _inFile += "/Utils/Alignment_2018.xml";
  }

    m_XMLparser = new XMLSettingsReader();

    if (!m_XMLparser->parseFile(_inFile)) {
        if(!m_XMLparser->parseFile("Alignment_2018.xml")){
          std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
          return;
        }
        std::cout << " Found Alignment_2018.xml in executable directory " << std::endl;
    }

    m_alignment = new Alignment();

    if(debug){
      std::cout << "Loading .xml Alignment File..." << std::endl;
      std::cout << "Found " << m_XMLparser->getBaseNodeCount("Alignment") << " alignment entries " << std::endl;
      std::cout << "Retrieving the information for run " << m_runNumber << std::endl;
    }

    int run;
    for ( int i = 0; i < m_XMLparser->getBaseNodeCount("Alignment"); i++) {
      m_XMLparser->getChildValue("Alignment",i,"run",run);
      if(run != m_runNumber) continue;
        if(debug){
          std::cout << "Found Run Entry in Alignment file for run " << m_runNumber << std::endl;
        }
        m_XMLparser->getChildValue("Alignment",i,"beam_scan",m_alignment->beam_scan);
        m_XMLparser->getChildValue("Alignment",i,"beam_type",m_alignment->beam_type);
        m_XMLparser->getChildValue("Alignment",i,"beam_energy",m_alignment->beam_energy);
        m_XMLparser->getChildValue("Alignment",i,"x_table",m_alignment->x_table);
        m_XMLparser->getChildValue("Alignment",i,"y_table",m_alignment->y_table);
        m_XMLparser->getChildValue("Alignment",i,"upstream_Det",m_alignment->upstream_Det);
        m_XMLparser->getChildValue("Alignment",i,"mid_Det",m_alignment->mid_Det);
        m_XMLparser->getChildValue("Alignment",i,"downstream_Det",m_alignment->downstream_Det);
        m_XMLparser->getChildValue("Alignment",i,"target_In",m_alignment->target_In);
        m_XMLparser->getChildValue("Alignment",i,"lead_In",m_alignment->lead_In);
        m_XMLparser->getChildValue("Alignment",i,"magnet_On",m_alignment->magnet_On);
    }

    if(m_alignment == NULL) std::cout << "WARNING: ALIGNMENT NOT FOUND!!!" << std::endl;
    return;
}



/** @brief Loads calibrated timing information for DRS4 modules based on run number
 *  @param _size Size of the lead block. If none is given m_leadBlockSize will be used if defined
 *  @param _pos Position of the lead block. If none is given m_leadBlockPos will be used if defined
 *
 *  Adds a lead block to the world
 *
 *
 */
void DetectorConstruction::ConstructLeadBlock( G4ThreeVector* _size, G4ThreeVector* _pos ){

  //Assign to member values if inputs weren't defined
  if(_size == 0 && m_leadBlockSize) _size = m_leadBlockSize;
  if(_pos  == 0 && m_leadBlockPos ) _pos  = m_leadBlockPos;

  //Print an error message in case we somehow got here without position defined
  //Size has a default value set in the constructor
  if(_pos == 0){
    G4cerr << "Attempting to place a lead block without position defined" << G4endl;
  }

  G4Box* leadBlock = new G4Box("LeadBlock", _size->x()/2.0, _size->y()/2.0, _size->z()/2.0);

  logic_leadBlock
  = new G4LogicalVolume(leadBlock,
                        m_materials->Pb,
                        "LeadBlock");

  new G4PVPlacement(0,
                   *_pos,
                   logic_leadBlock,
                   "LeadBlock1",
                   m_logicWorld,
                   false,
                   0
                   );

  // Visualization attributes
  G4VisAttributes* boxVisAtt_leadblk= new G4VisAttributes(G4Colour(1.0,0.0,1.0)); //magenta
  logic_leadBlock ->SetVisAttributes(boxVisAtt_leadblk);
}




/** @brief Loads calibrated timing information for DRS4 modules based on run number
 *  @param _inFile Optional argument for loading a custom file
 *
 *  Uses run number to determine scan number and loads the appropriate DRS4 timing
 *  information from the 2021 Testbeam. After loading, we hand each Channel a
 *  pointer to its timing vector. Must be run after LoadConfigurationFile()
 *
 */
void DetectorConstruction::LoadSurveyFile2021( G4String _inFile ){
  if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/Survey_2021.xml");
  m_XMLparser = new XMLSettingsReader();

  if (!m_XMLparser->parseFile(_inFile)) {
    if (!m_XMLparser->parseFile("Survey_2021.xml")) {
          std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
          return;
    }
  }

  int first_run, last_run;
  double x,y,z,tx,ty,ttx,tty,qw,qi,qj,qk;
  std::string names = "";

  for (int i = 0; i < m_XMLparser->getBaseNodeCount("Survey"); i++) {
      Survey2021 *buffer = new Survey2021();

      m_XMLparser->getChildValue("Survey",i,"start_run",first_run);
      m_XMLparser->getChildValue("Survey",i,"end_run",last_run);

      //Discard entries for any channel that does not apply to our run
      if(m_runNumber < first_run || m_runNumber > last_run) continue;

      //If the entry applies, we store it in the vector
      m_XMLparser->getChildValue("Survey",i,"detector",buffer->detector);
      std::cout << "Parsing " << buffer->detector << " from run " << first_run << " to " << last_run << std::endl;
      m_XMLparser->getChildValue("Survey",i,"x_pos",x);
      m_XMLparser->getChildValue("Survey",i,"y_pos",y);
      m_XMLparser->getChildValue("Survey",i,"z_pos",z);
      buffer->pos.set(x,y,z);
      m_XMLparser->getChildValue("Survey",i,"trigger_x",ttx);
      m_XMLparser->getChildValue("Survey",i,"trigger_y",tty);
      buffer->trig_table.set(ttx,tty,0.0);
      m_XMLparser->getChildValue("Survey",i,"table_x",tx);
      m_XMLparser->getChildValue("Survey",i,"table_y",ty);
      buffer->table.set(tx,ty,0.0);
      m_XMLparser->getChildValue("Survey",i,"w",qw);
      m_XMLparser->getChildValue("Survey",i,"i",qi);
      m_XMLparser->getChildValue("Survey",i,"j",qj);
      m_XMLparser->getChildValue("Survey",i,"k",qk);
      TQuaternion q(qw,qi,qj,qk);
      buffer->rotation = QuaternionToRotationMatrix(q);

      //Check for
      bool isNew(true);
      for( uint k = 0; k < m_surveyEntries2021.size(); k++){
          if(buffer->detector == m_surveyEntries2021.at(k)->detector){
              std::cout << "WARNING!!! Redundancy in your settings file for " << buffer->detector << ". Check it carefully. The second entry found will be skipped..." << std::endl;
              isNew = false;
          }
      }
      if(isNew){
        m_surveyEntries2021.push_back(buffer);
        names+= buffer->detector + "; ";
      }
  }

  std::cout << "Detectors found in survey file: " << names << std::endl << std::endl;

  delete m_XMLparser; m_XMLparser = NULL;
}




/** @brief Loads calibrated timing information for DRS4 modules based on run number
 *  @param _inFile Optional argument for loading a custom file
 *
 *  Uses run number to determine scan number and loads the appropriate DRS4 timing
 *  information from the 2022 Testbeam. After loading, we hand each Channel a
 *  pointer to its timing vector. Must be run after LoadConfigurationFile()
 *
 */
void DetectorConstruction::LoadSurveyFile2022( G4String _inFile ){
  if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/Survey_2022.xml");
  m_XMLparser = new XMLSettingsReader();

  if (!m_XMLparser->parseFile(_inFile)) {
    if (!m_XMLparser->parseFile("Survey_2022.xml")) {
          std::cerr << "Could not parse file : " << _inFile << std::endl;
          return;
    }
  }

  int first_run, last_run;
  double x,y,z,tx,ty,qw,qi,qj,qk;
  std::string names = "";

  for (int i = 0; i < m_XMLparser->getBaseNodeCount("Survey"); i++) {
      Survey2022 *buffer = new Survey2022();

      m_XMLparser->getChildValue("Survey",i,"start_run",first_run);
      m_XMLparser->getChildValue("Survey",i,"end_run",last_run);

      //Discard entries for any channel that does not apply to our run
      if(m_runNumber < first_run || m_runNumber > last_run) continue;

      //If the entry applies, we store it in the vector
      m_XMLparser->getChildValue("Survey",i,"detector",buffer->detector);
      std::cout << "Parsing " << buffer->detector << " from run " << first_run << " to " << last_run << std::endl;
      m_XMLparser->getChildValue("Survey",i,"x_pos",x);
      m_XMLparser->getChildValue("Survey",i,"y_pos",y);
      m_XMLparser->getChildValue("Survey",i,"z_pos",z);
      buffer->pos.set(x,y,z);
      m_XMLparser->getChildValue("Survey",i,"table_x",tx);
      m_XMLparser->getChildValue("Survey",i,"table_y",ty);
      buffer->table.set(tx,ty,0.0);
      m_XMLparser->getChildValue("Survey",i,"w",qw);
      m_XMLparser->getChildValue("Survey",i,"i",qi);
      m_XMLparser->getChildValue("Survey",i,"j",qj);
      m_XMLparser->getChildValue("Survey",i,"k",qk);
      TQuaternion q(qw,qi,qj,qk);
      buffer->rotation = QuaternionToRotationMatrix(q);

      //Check for
      bool isNew(true);
      for( uint k = 0; k < m_surveyEntries2022.size(); k++){
          if(buffer->detector == m_surveyEntries2022.at(k)->detector){
              std::cout << "WARNING!!! Redundancy in your settings file for " << buffer->detector << ". Check it carefully. The second entry found will be skipped..." << std::endl;
              isNew = false;
          }
      }
      if(isNew){
        m_surveyEntries2022.push_back(buffer);
        names+= buffer->detector + "; ";
      }
  }

  std::cout << "Detectors found in survey file: " << names << std::endl << std::endl;

  delete m_XMLparser; m_XMLparser = NULL;
}

/*
 * Copied from the Root TRotation(Tquaternion) constructor
 */
G4RotationMatrix DetectorConstruction::QuaternionToRotationMatrix(TQuaternion Q){
  double two_r2 = 2 * Q.fRealPart * Q.fRealPart;
  double two_x2 = 2 * Q.fVectorPart.X() * Q.fVectorPart.X();
  double two_y2 = 2 * Q.fVectorPart.Y() * Q.fVectorPart.Y();
  double two_z2 = 2 * Q.fVectorPart.Z() * Q.fVectorPart.Z();
  double two_xy = 2 * Q.fVectorPart.X() * Q.fVectorPart.Y();
  double two_xz = 2 * Q.fVectorPart.X() * Q.fVectorPart.Z();
  double two_xr = 2 * Q.fVectorPart.X() * Q.fRealPart;
  double two_yz = 2 * Q.fVectorPart.Y() * Q.fVectorPart.Z();
  double two_yr = 2 * Q.fVectorPart.Y() * Q.fRealPart;
  double two_zr = 2 * Q.fVectorPart.Z() * Q.fRealPart;
  double xx, yy, zz, xy, yx, xz, zx, yz, zy;

  // protect agains zero quaternion
  double mag2 = Q.QMag2();
    if (mag2 > 0) {

    // diago + identity
    xx = two_r2 + two_x2;
    yy = two_r2 + two_y2;
    zz = two_r2 + two_z2;

    //        line 0 column 1 and conjugate
    xy = two_xy - two_zr;
    yx = two_xy + two_zr;

    //        line 0 column 2 and conjugate
    xz = two_xz + two_yr;
    zx = two_xz - two_yr;

    //        line 1 column 2 and conjugate
    yz = two_yz - two_xr;
    zy = two_yz + two_xr;

    // protect agains non-unit quaternion
    if (TMath::Abs(mag2-1) > 1e-10) {
      xx /= mag2;
      yy /= mag2;
      zz /= mag2;
      xy /= mag2;
      yx /= mag2;
      xz /= mag2;
      zx /= mag2;
      yz /= mag2;
      zy /= mag2;
    }

    // diago : remove identity
    xx -= 1;
    yy -= 1;
    zz -= 1;

    return G4RotationMatrix( G4ThreeVector(xx, yx, zx),
                             G4ThreeVector(xy, yy, zy),
                             G4ThreeVector(xz, yz, zz));


  } else {
    //The quaternion is somehow unusable
    G4cout << "Quaternion conversion failed" << G4endl;
    return G4RotationMatrix();
  }
}

/*
 * Retrieve a survey based on name
*/
Survey* DetectorConstruction::GetSurvey(G4String name){
  for(unsigned int i = 0; i < m_surveyEntries.size(); i++){
    if( m_surveyEntries[i]->detector.compare( name.c_str() ) ){ return m_surveyEntries[i]; }
  }
  Survey* empty=NULL;
  return empty;
}

/*
 * Add a ZDC module
*/
void DetectorConstruction::AddZDC(G4ThreeVector* position, G4RotationMatrix* rotation){
  uint newModNum = m_ZDCvec.size()+1;
  m_ZDCvec.push_back(new ModTypeZDC(newModNum, m_logicWorld, position, rotation ));
  currentZDC = newModNum;
  printf("Added ZDC%d\n", newModNum);
}

/*
 * Add an RPD module
*/
void DetectorConstruction::AddRPD(G4ThreeVector *position, G4RotationMatrix *rotation){
  uint newModNum = m_RPDvec.size()+1;
  m_RPDvec.push_back(new ModTypeRPD(newModNum, m_logicWorld, position, rotation ));
  currentRPD = newModNum;
  printf("Added RPD%d\n", newModNum);
}

/*
 * Duplicate a ZDC module
*/
void DetectorConstruction::DuplicateZDC( G4int module ){
  uint newModNum = m_ZDCvec.size()+1;
  if((unsigned)module >= newModNum ){
    printf("\n\n Cannot duplicate. ZDC%d does not exist \n\n",module);
    return;
  }
  m_ZDCvec.push_back( new ModTypeZDC( newModNum, m_ZDCvec.at(module-1) ) );
  currentZDC = newModNum;
  printf("Duplicate ZDC%d built from ZDC%d\n", newModNum, module);
}

/*
 * Duplicate an RPD module
*/
void DetectorConstruction::DuplicateRPD( G4int module ){
  uint newModNum = m_RPDvec.size()+1;
  if((unsigned)module >= newModNum ){
    printf("\n\n Cannot duplicate. RPD%d does not exist \n\n",module);
    return;
  }
  m_RPDvec.push_back( new ModTypeRPD( newModNum, m_RPDvec.at(module-1) ) );
  currentRPD = newModNum;
  printf("Duplicate RPD%d built from RPD%d\n", newModNum, module);
}

//Add a Trigger
void DetectorConstruction::AddTRI(G4ThreeVector* position, G4RotationMatrix* rotation){
  m_TRI = new ModTypeTRI( m_logicWorld, position, rotation );
  printf("Added Trigger%d\n", 1);
  isTrigger = true;
}
