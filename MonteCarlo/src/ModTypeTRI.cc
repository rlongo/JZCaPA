//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \ingroup mc
/// \file ModTypeTRU.cc
/// \author Matthew Hoppesch
/// \brief Trigger detector construction

#include "ModTypeTRI.hh"
#include "FiberSD.hh"

#include "G4GeometryManager.hh"
#include "G4SolidStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4MaterialTable.hh"

#include "G4SDManager.hh"

#include "G4Box.hh"
#include "G4Para.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include <stdio.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ModTypeTRI::ModTypeTRI(G4LogicalVolume *mother, G4ThreeVector *pos, G4RotationMatrix *rot)
  : Mod(0,mother,pos,rot)
{
	m_matPaddle = m_materials->PMMA;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ModTypeTRI::~ModTypeTRI()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ModTypeTRI::Construct()
{
  ConstructDetector();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ModTypeTRI::ConstructDetector()
{

  //----------------------------------------------
  // Paddle Dimensions
  //----------------------------------------------
  float frontPad_dim_z = 20*mm;
  float frontPad_dim_y = 250*mm;
  float frontPad_dim_x = 4*mm;
  float backPad_dim_z = 25*mm;
  float backPad_dim_y = 200*mm;
  float backPad_dim_x = 7*mm;
  float separation_dist_z = 52.885*mm;
  float box_dim = 300*mm;
  //----------------------------------------------
  // Housing
  //----------------------------------------------
  m_ModuleBox      = new G4Box("ModuleCasing", box_dim*mm/2.0,box_dim*mm/2.0, box_dim*mm/2.0);
  m_ModuleLogical  = new G4LogicalVolume(m_ModuleBox     ,m_materials->Air, "Module_Logical");
  G4RotationMatrix* nullRotation = new G4RotationMatrix();
  m_ModulePhysical = new G4PVPlacement(nullRotation,G4ThreeVector( m_pos->x(), m_pos->y(), m_pos->z() ) ,m_ModuleLogical,"Trigger_Body_Physical",m_logicMother,false,1,CHECK_OVERLAPS);
  G4VisAttributes* moduleColor = new G4VisAttributes( );
	moduleColor->SetColor(0.,0.,0.,.0);
  m_ModuleLogical->SetVisAttributes( moduleColor );
  //----------------------------------------------
  // Trigger
  //----------------------------------------------

  m_FrontPaddleH = new G4Box("FrontPaddleH",
									frontPad_dim_x*mm/2.0 ,
									frontPad_dim_y*mm/2.0,
									frontPad_dim_z*mm/2.0);
  m_FrontPaddleHLogical =
		new G4LogicalVolume(m_FrontPaddleH,
												m_matPaddle,
												"m_FrontPaddleHLogical");
  m_FrontPaddleV = new G4Box("FrontPaddleV",
  									frontPad_dim_x*mm/2.0 ,
  									frontPad_dim_y*mm/2.0,
  									frontPad_dim_z*mm/2.0);
  m_FrontPaddleVLogical =
  		new G4LogicalVolume(m_FrontPaddleV,
  												m_matPaddle,
  												"m_FrontPaddleVLogical");
  m_BackPaddleH = new G4Box("BackPaddleH",
									backPad_dim_x*mm/2.0 ,
									backPad_dim_y*mm/2.0,
									backPad_dim_z*mm/2.0);
  m_BackPaddleHLogical =
		new G4LogicalVolume(m_BackPaddleH,
												m_matPaddle,
												"m_BackPaddleHLogical");
  m_BackPaddleV = new G4Box("BackPaddleV",
  									backPad_dim_x*mm/2.0 ,
  									backPad_dim_y*mm/2.0,
  									backPad_dim_z*mm/2.0);
  m_BackPaddleVLogical =
  		new G4LogicalVolume(m_BackPaddleV,
  												m_matPaddle,
  												"m_BackPaddleVLogical");

  G4VisAttributes* PadColor = new G4VisAttributes();
  PadColor->SetColor(0.,1.,1.,1.);
  m_FrontPaddleHLogical->SetVisAttributes(PadColor);
  m_FrontPaddleVLogical->SetVisAttributes(PadColor);
  m_BackPaddleHLogical->SetVisAttributes(PadColor);
  m_BackPaddleVLogical->SetVisAttributes(PadColor);

  //----------------------------------------------
  // Placement
  //----------------------------------------------
  G4RotationMatrix* Rotation = new G4RotationMatrix();
  Rotation->rotateZ(90.*deg);
  m_FrontPaddleHPhysical = new G4PVPlacement(Rotation,
                          G4ThreeVector(0.*mm,0.*mm,separation_dist_z*mm/2.+frontPad_dim_z*1.501*mm),
                          m_FrontPaddleHLogical,
                          "FrontPaddleH_Phys",
                          m_ModuleLogical,
                          false,
                          1,
                          CHECK_OVERLAPS);
  m_FrontPaddleVPhysical = new G4PVPlacement(nullRotation,
                          G4ThreeVector(0.*mm,0.*mm,separation_dist_z*mm/2.+frontPad_dim_z*mm/2.),
                          m_FrontPaddleVLogical,
                          "FrontPaddleV_Phys",
                          m_ModuleLogical,
                          false,
                          1,
                          CHECK_OVERLAPS);
  m_BackPaddleHPhysical = new G4PVPlacement(Rotation,
                          G4ThreeVector(0.*mm,0.*mm,-separation_dist_z*mm/2.-backPad_dim_z*1.501*mm),
                          m_BackPaddleHLogical,
                          "BackPaddleH_Phys",
                          m_ModuleLogical,
                          false,
                          1,
                          CHECK_OVERLAPS);
  m_BackPaddleVPhysical = new G4PVPlacement(nullRotation,
                          G4ThreeVector(0.*mm,0.*mm,-separation_dist_z*mm/2.-backPad_dim_z*mm/2.),
                          m_BackPaddleVLogical,
                          "BackPaddleV_Phys",
                          m_ModuleLogical,
                          false,
                          1,
                          CHECK_OVERLAPS);

std::cout << "Trigger construction finished" <<  std::endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void ModTypeTRI::ConstructSDandField(){

  //G4SDManager* SDman = G4SDManager::GetSDMpointer();




  //m_FiberCoreLogical->SetSensitiveDetector( aFiberSD );

  std::cout << "Trigger SD construction finished" <<  std::endl;

}
