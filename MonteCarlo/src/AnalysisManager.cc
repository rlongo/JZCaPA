//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \ingroup mc
/// \file AnalysisManager.hh
/// \brief Definition of the AnalysisManager class
/// \author Chad Lantz
/// \date 18 December 2023


#include "AnalysisManager.hh"
#include "DetectorConstruction.hh"
#include "SteppingAction.hh"

#include "G4SDManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

AnalysisManager* AnalysisManager::analysisManager = NULL;

AnalysisManager* AnalysisManager::getInstance(void)
{
    if (analysisManager == NULL) {
        analysisManager = new AnalysisManager();
    }
    return analysisManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

AnalysisManager::AnalysisManager()
  : m_eventDataNtupleNo(-1),
    m_eventGenNtupleNo(-1),
    m_FactoryOn(false)
{
  m_analysisManager = G4AnalysisManager::Instance();
  m_analysisManager->SetDefaultFileType("root");
  
  m_lastStepVec = new std::vector< G4ThreeVector >;
  m_gunPos = new G4ThreeVector;

  m_Pi0Vert = new std::vector< G4ThreeVector >;
  m_Pi0Mom = new std::vector< G4ThreeVector >;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

AnalysisManager::~AnalysisManager()
{
  delete m_lastStepVec;
  delete m_gunPos;
  delete m_Pi0Vert;
  delete m_Pi0Mom;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::Book( G4String fileName )
{
  // Create or get analysis manager
  // The choice of analysis technology is done via selection of a namespace
  // in AnalysisManager.hh
  G4cout << "Using " << m_analysisManager->GetType() << " type Analysis Manager" << G4endl;
  m_analysisManager->SetVerboseLevel(1);
  m_analysisManager->SetNtupleMerging(true);

  // Open an output file
  //
  G4bool fileOpen;
  if(fileName == ""){
    fileOpen = m_analysisManager->OpenFile("output");
  } else {
    fileOpen = m_analysisManager->OpenFile(fileName);
  }
  if (! fileOpen) {
    G4cerr << "\n---> AnalysisManager::Book(): cannot open "
           << m_analysisManager->GetFileName() << G4endl;
    return;
  }

  //Get information about the detector configuration from DetectorConstruction
  //TODO: Make the PI0 flag set through G4GenericMessenger here instead of detectorConstruction
  DetectorConstruction* detectorConstruction = (DetectorConstruction*)G4RunManager::GetRunManager()->GetUserDetectorConstruction();
  PI0 = detectorConstruction->GetPI0Flag();

  SteppingAction* steppingAction = (SteppingAction*)G4RunManager::GetRunManager()->GetUserSteppingAction();
  steppingAction->SetLastStepVec( m_lastStepVec, &m_lastStepPidVec );
  steppingAction->SetPi0Mom( m_Pi0Mom );
  steppingAction->SetPi0Vertex( m_Pi0Vert );

  //Create the event data tree (Tree 0)
  MakeEventDataTree();

  for( auto sd : m_SDlist){
    sd->BookOutput();
  }

  //Turn on the data factory I guess
  m_FactoryOn = true;

  G4cout << "\n----> Output file is open in "
         << m_analysisManager->GetFileName() << "."
         << m_analysisManager->GetFileType() << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::Save()
{
  if (! m_FactoryOn) return;

  m_analysisManager->Write();
  m_analysisManager->CloseFile();

  G4cout << "\n----> Histograms and ntuples are saved\n" << G4endl;

  delete m_analysisManager;
  m_FactoryOn = false;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::FillNtuples(){

  // Fill the last step vectors

  //G4cout << "Before lastStep loop" << G4endl;
  for(uint i = 0; i < m_lastStepVec->size(); i++){
    m_lastStepXVec.push_back( m_lastStepVec->at(i).x() );
    m_lastStepYVec.push_back( m_lastStepVec->at(i).y() );
    m_lastStepZVec.push_back( m_lastStepVec->at(i).z() );
  }
  for(uint i = 0; i < m_Pi0Mom->size(); i++){
    m_Pi0MomX.push_back( m_Pi0Mom->at(i).x() );
    m_Pi0MomY.push_back( m_Pi0Mom->at(i).y() );
    m_Pi0MomZ.push_back( m_Pi0Mom->at(i).z() );
  }

  for(uint i = 0; i < m_Pi0Vert->size(); i++){
    m_Pi0VertX.push_back( m_Pi0Vert->at(i).x() );
    m_Pi0VertY.push_back( m_Pi0Vert->at(i).y() );
    m_Pi0VertZ.push_back( m_Pi0Vert->at(i).z() );
  }

  m_analysisManager->FillNtupleIColumn( m_eventDataNtupleNo, 0, m_eventNo );

  m_analysisManager->FillNtupleDColumn( m_eventDataNtupleNo, 1, m_gunPos->x() );
  m_analysisManager->FillNtupleDColumn( m_eventDataNtupleNo, 2, m_gunPos->y() );
  m_analysisManager->FillNtupleDColumn( m_eventDataNtupleNo, 3, m_gunPos->z() );

  // fill ntuples  //
  if(m_eventDataNtupleNo >= 0) 
    m_analysisManager->AddNtupleRow(m_eventDataNtupleNo);
  if (m_eventGenNtupleNo >= 0)
    m_analysisManager->AddNtupleRow(m_eventGenNtupleNo);

  m_lastStepVec->clear();
  m_lastStepXVec.clear();
  m_lastStepYVec.clear();
  m_lastStepZVec.clear();
  m_lastStepPidVec.clear();

  m_Pi0Mom->clear();
  m_Pi0MomX.clear();
  m_Pi0MomY.clear();
  m_Pi0MomZ.clear();

  m_Pi0Vert->clear();
  m_Pi0VertX.clear();
  m_Pi0VertY.clear();
  m_Pi0VertZ.clear();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::MakeEventDataTree( ){
  m_eventDataNtupleNo = m_analysisManager->CreateNtuple( "EventData", "Event Data");

  //Integer
  m_analysisManager->CreateNtupleIColumn( m_eventDataNtupleNo, "EventNo"  );

  //Doubles
  m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "gunPosX"  );
  m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "gunPosY"  );
  m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "gunPosZ"  );

  //std::vector< double >
  m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "lastStepX",   m_lastStepXVec   );
  m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "lastStepY",   m_lastStepYVec   );
  m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "lastStepZ",   m_lastStepZVec   );


  //std::vector< int >
  m_analysisManager->CreateNtupleIColumn( m_eventDataNtupleNo, "lastStepPID", m_lastStepPidVec );

  if(PI0){
      //std::vector< double >
      m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "pi0Px",   m_Pi0MomX );
      m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "pi0Py",   m_Pi0MomY );
      m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "pi0Pz",   m_Pi0MomZ );

      //std::vector< double >
      m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "pi0Vx",   m_Pi0VertX );
      m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "pi0Vy",   m_Pi0VertY );
      m_analysisManager->CreateNtupleDColumn( m_eventDataNtupleNo, "pi0Vz",   m_Pi0VertZ );
  }

  m_analysisManager->FinishNtuple( );

  G4cout << "Created EventData tree with ntuple number " << m_eventDataNtupleNo << G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::MakeEventGenTree( std::vector< std::vector<int>* > &intVec , std::vector< std::vector<double>* > &dblVec, G4int type ){

  m_eventGenNtupleNo = m_analysisManager->GetNofNtuples();

  m_analysisManager->CreateNtuple( "EventGen", "Event Generator Data");

  if(type == 0){ // CRMC generated events
    //Integer
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo, "nPart"  );
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo, "nSpec"  );
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo, "model"  );

    //Doubles
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "impactParameter"  );

    ////std::vector< double >
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "px", *dblVec[0] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "py", *dblVec[1] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "pz", *dblVec[2] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo,  "E", *dblVec[3] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo,  "m", *dblVec[4] );


    //vector< int > branches
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo,      "pdgid", *intVec[0] );
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo, "CRMCstatus", *intVec[1] );
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo, "keptStatus", *intVec[2] );
  }
  else if(type == 1){ // Toy pt Generator
    //Integer
    m_analysisManager->CreateNtupleIColumn( m_eventGenNtupleNo, "nSpectators"  );

    //Doubles
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "ptCollision"  );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "ptBreakup"    );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "rpAngle"      );

    ////std::vector< double >
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "px", *dblVec[0] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "py", *dblVec[1] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo, "pz", *dblVec[2] );
    m_analysisManager->CreateNtupleDColumn( m_eventGenNtupleNo,  "E", *dblVec[3] );

  }

  m_analysisManager->FinishNtuple( );

  G4cout << "Created eventGen tree with ntuple number " << m_eventGenNtupleNo << G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::FillEventGenTree( int nPart, int nSpec, int model, double impactParam ){

  m_analysisManager->FillNtupleIColumn( m_eventGenNtupleNo, 0, nPart );
  m_analysisManager->FillNtupleIColumn( m_eventGenNtupleNo, 1, nSpec );
  m_analysisManager->FillNtupleIColumn( m_eventGenNtupleNo, 2, model );

  //Doubles
  m_analysisManager->FillNtupleDColumn( m_eventGenNtupleNo, 3, impactParam );

  G4cout << "Filling EventGenTree" << G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void AnalysisManager::FillEventGenTree( int nSpectators, double ptCollision, double ptBreakup, double rpAngle ){

  m_analysisManager->FillNtupleIColumn( m_eventGenNtupleNo, 0, nSpectators );

  //Doubles
  m_analysisManager->FillNtupleDColumn( m_eventGenNtupleNo, 1, ptCollision );
  m_analysisManager->FillNtupleDColumn( m_eventGenNtupleNo, 2, ptBreakup   );
  m_analysisManager->FillNtupleDColumn( m_eventGenNtupleNo, 3, rpAngle     );

  G4cout << "Filling EventGenTree" << G4endl;
}

