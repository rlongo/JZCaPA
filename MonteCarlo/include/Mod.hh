//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Author: Chad Lantz

#ifndef Mod_h
#define Mod_h 1


#include "globals.hh"
#include "G4PVPlacement.hh"
#include "Materials.hh"

#include <vector>

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4VSolid;
class G4Material;

/// Detector construction class to define materials and geometry.

class Mod
{
public:
  //Default constructor
  Mod()
    : m_modNum(0),
      m_pos(NULL),
      m_rotation(NULL),
      m_logicMother(NULL)
    {
      m_materials = Materials::getInstance();
    }
  //Standard (most used) constructor
  Mod(const int cn, G4LogicalVolume *mother, G4ThreeVector *pos, G4RotationMatrix *rot)
      : m_modNum(cn),
        m_pos(pos),
        m_rotation(rot),
        m_detType(""),
        m_detName(""),
        OPTICAL(false),
        CHECK_OVERLAPS(false),
        REDUCED_TREE(false),
        m_logicMother(mother)
  {
    m_materials = Materials::getInstance();
  }
  //Copy constructor
  Mod(const int cn, Mod *right)
    : m_modNum(cn)
  {
    m_pos              = new G4ThreeVector(*right->m_pos);
    m_rotation         = (right->m_rotation == NULL) ? NULL : new G4RotationMatrix(*right->m_rotation);
    m_HousingThickness = right->m_HousingThickness;
    m_detType          = right->m_detType;
    m_detName             = "";
    OPTICAL            = right->OPTICAL;
    CHECK_OVERLAPS     = right->CHECK_OVERLAPS;
    REDUCED_TREE       = right->REDUCED_TREE;
    m_matHousing       = right->m_matHousing;
    m_logicMother      = right->m_logicMother;
    m_materials        = right->m_materials;
  }

  virtual ~Mod(){
    if(m_pos) delete m_pos;
    if(m_rotation) delete m_rotation;
  };

  virtual void  Construct() = 0;
  virtual void  ConstructSDandField() = 0;


  inline  void  SetPosition          ( G4ThreeVector* vec ){ delete m_pos;       m_pos       = vec; }
  inline  void  SetHousingThickness  ( G4double       arg ){ m_HousingThickness  = arg; }
  inline  void  SetDetectorType      ( G4String       arg ){ m_detType           = arg; }
  inline  void  SetDetectorName      ( G4String       arg ){ m_detName           = arg; }
  inline  void  SetOpticalFlag       ( G4bool         arg ){ OPTICAL             = arg; }
  inline  void  SetOverlapsFlag      ( G4bool         arg ){ CHECK_OVERLAPS      = arg; }
  inline  void  SetReducedTreeFlag   ( G4bool         arg ){ REDUCED_TREE        = arg; }


  inline  G4ThreeVector*   GetPosition             ( ){ return m_pos;         }
  inline  G4int            GetModNum               ( ){ return m_modNum;      }
  inline  G4String         GetName                 ( ){ return m_detName;     }
  inline  G4bool           GetOpticalFlag          ( ){ return OPTICAL;       }
  inline  G4bool           GetReducedTreeFlag      ( ){ return REDUCED_TREE;  }

protected:
  const G4int       m_modNum;
  G4ThreeVector*    m_pos;
  G4RotationMatrix* m_rotation;
  G4double          m_HousingThickness;
  G4double          m_topOfVolume;
  G4String          m_detType;
  G4String          m_detName;
  G4bool            OPTICAL;
  G4bool            CHECK_OVERLAPS;
  G4bool            REDUCED_TREE;
  Materials*        m_materials;
  G4Material*       m_matHousing;
  G4LogicalVolume*  m_logicMother;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
