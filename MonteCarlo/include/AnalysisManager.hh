// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file AnalysisManager.hh
/// \brief Selection of the analysis technology
/// \author Chad Lantz
/// \date 18 December 2023

#ifndef AnalysisManager_h
#define AnalysisManager_h 1

#include "G4AnalysisManager.hh"

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4ThreeVector.hh"

#include <vector>

#include "FiberSD.hh"
class FiberSD;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class AnalysisManager
{
  public:
    static AnalysisManager* getInstance(void);
   ~AnalysisManager();

    void Book( G4String fileName );
    void Save();

    G4int CreateNtuple       ( G4String name, G4String title){ return m_analysisManager->CreateNtuple(name, title); };
    G4int CreateNtupleIColumn( G4int ntupleId, const G4String &name){ return m_analysisManager->CreateNtupleIColumn(ntupleId, name); };
    G4int CreateNtupleFColumn( G4int ntupleId, const G4String &name){ return m_analysisManager->CreateNtupleFColumn(ntupleId, name); };
    G4int CreateNtupleDColumn( G4int ntupleId, const G4String &name){ return m_analysisManager->CreateNtupleDColumn(ntupleId, name); };
    G4int CreateNtupleSColumn( G4int ntupleId, const G4String &name){ return m_analysisManager->CreateNtupleSColumn(ntupleId, name); };
    G4int CreateNtupleIColumn( G4int ntupleId, const G4String &name, std::vector<int> &vector){ return m_analysisManager->CreateNtupleIColumn(ntupleId, name, vector); };
    G4int CreateNtupleFColumn( G4int ntupleId, const G4String &name, std::vector<float> &vector){ return m_analysisManager->CreateNtupleFColumn(ntupleId, name, vector); };
    G4int CreateNtupleDColumn( G4int ntupleId, const G4String &name, std::vector<double> &vector){ return m_analysisManager->CreateNtupleDColumn(ntupleId, name, vector); };
    void  FinishNtuple       ( G4int ntupleId ){ m_analysisManager->FinishNtuple(ntupleId); };

    void FillNtuples();
    void FillNtupleIColumn( G4int ntupleId, G4int columnId, G4int value ){ m_analysisManager->FillNtupleIColumn(ntupleId, columnId, value); };
    void FillNtupleFColumn( G4int ntupleId, G4int columnId, G4float value) { m_analysisManager->FillNtupleFColumn(ntupleId, columnId, value); };
    void FillNtupleDColumn( G4int ntupleId, G4int columnId, G4double value ){ m_analysisManager->FillNtupleDColumn(ntupleId, columnId, value); };
    void FillNtupleSColumn( G4int ntupleId, G4int columnId, G4String value) { m_analysisManager->FillNtupleSColumn(ntupleId, columnId, value); };
    void FillEventGenTree  ( int nPart, int nSpec, int model, double impactParam );
    void FillEventGenTree  ( int nSpectators, double ptCollision, double ptBreakup, double rpAngle );
    void AddNtupleRow      ( G4int ntupleId ){ m_analysisManager->AddNtupleRow(ntupleId); };

    void MakeEventDataTree ( );
    void MakeEventGenTree  ( std::vector< std::vector<int>* > &intVec , std::vector< std::vector<double>* > &dblVec, G4int type );

    inline G4bool GetOpticalFlag( ){ return OPTICAL; }
    inline G4bool GetPI0Flag    ( ){ return PI0;     }
    inline void SetGunPosition ( G4double x, G4double y, G4double z ){ m_gunPos->set(x,y,z); }
    inline void SetEventNo     ( G4int _eventNo ){ m_eventNo = _eventNo; }

    inline void RegisterSD( FiberSD *sd ){ m_SDlist.push_back(sd); };


  private:
    AnalysisManager();
    static AnalysisManager* analysisManager;
    G4int m_eventNo;
    G4int m_eventDataNtupleNo;
    G4int m_eventGenNtupleNo;
    G4bool m_FactoryOn;
    G4bool OPTICAL;
    G4bool PI0;
    G4ThreeVector* m_gunPos;
    G4AnalysisManager* m_analysisManager;
    DetectorConstruction* m_detectorConstruction;
    std::vector< FiberSD* > m_SDlist;
    std::vector< G4ThreeVector >* m_lastStepVec, *m_Pi0Mom, *m_Pi0Vert;
    std::vector< int >  m_lastStepPidVec;
    std::vector< double > m_lastStepXVec, m_lastStepYVec, m_lastStepZVec;
    std::vector< double > m_Pi0MomX, m_Pi0MomY, m_Pi0MomZ;
    std::vector< double > m_Pi0VertX, m_Pi0VertY, m_Pi0VertZ;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
