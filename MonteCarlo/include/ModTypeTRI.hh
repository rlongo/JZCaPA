// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Author: Matthew Hoppesch

#ifndef ModTypeTRI_h
#define ModTypeTRI_h 1

#include "Mod.hh"

/// Detector construction class to define materials and geometry.

class ModTypeTRI : public Mod
{
public:
  ModTypeTRI(ModTypeTRI*);
  ModTypeTRI(G4LogicalVolume*, G4ThreeVector*, G4RotationMatrix* );
  ~ModTypeTRI();

  void  Construct();
  void  ConstructSDandField();
  void  ConstructDetector();

  inline  G4LogicalVolume* GetFHLogicalVolume( ){ return m_FrontPaddleHLogical;    }
  inline  G4LogicalVolume* GetFVLogicalVolume( ){ return m_FrontPaddleVLogical;    }
  inline  G4LogicalVolume* GetBHLogicalVolume( ){ return m_BackPaddleHLogical;    }
  inline  G4LogicalVolume* GetBVLogicalVolume( ){ return m_BackPaddleVLogical;    }

protected:
  G4Material*       m_matPaddle;

  G4VSolid*          m_ModuleBox;
  G4LogicalVolume*   m_ModuleLogical;
  G4VPhysicalVolume* m_ModulePhysical;


  G4VSolid*          m_FrontPaddleH;
  G4LogicalVolume*   m_FrontPaddleHLogical;
  G4VPhysicalVolume* m_FrontPaddleHPhysical;

  G4VSolid*          m_FrontPaddleV;
  G4LogicalVolume*   m_FrontPaddleVLogical;
  G4VPhysicalVolume* m_FrontPaddleVPhysical;

  G4VSolid*          m_BackPaddleH;
  G4LogicalVolume*   m_BackPaddleHLogical;
  G4VPhysicalVolume* m_BackPaddleHPhysical;

  G4VSolid*          m_BackPaddleV;
  G4LogicalVolume*   m_BackPaddleVLogical;
  G4VPhysicalVolume* m_BackPaddleVPhysical;



  // Vertical quartz strips (these strips are full -- no partial segments)

};
#endif
