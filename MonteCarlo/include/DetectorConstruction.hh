//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: DetectorConstruction.hh 69565 2013-05-08 12:35:31Z gcosmo $
//
/// \file DetectorConstruction.hh
/// \brief Definition of the DetectorConstruction class

#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "ModTypeZDC.hh"
#include "ModTypeRPD.hh"
#include "ModTypeTRI.hh"
#include "XMLSettingsReader.hh"
#include "Materials.hh"

#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"
#include "G4Cache.hh"
#include "G4MagneticField.hh"

#include "TQuaternion.h"

#include <vector>


class G4Box;
class G4Para;
class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;

class G4UniformMagField;
class PurgMagTabulatedField3D;

class G4Region;

class GFlashHomoShowerParameterisation;
class GFlashShowerModel;
class GFlashHitMaker;
class GFlashParticleBounds;


class Survey {

 public :

	/** Type of detector - ZDC or RPD **/
    G4String detector;
    /** x_pos for this survey */
    double x_pos;
	/** y_pos for this survey */
    double y_pos;
	/** z_pos for this survey */
    double z_pos;
	/** cos_x for this survey */
    double cos_x;
	/** cos_y for this survey */
    double cos_y;
	/** cos_z for this survey */
    double cos_z;

};

class Survey2021{

public:
  /** Name of the detector this survey entry is for */
  G4String detector;
  /** Position of the Detector during the survey*/
  G4ThreeVector pos;
  /** Table position during survey */
  G4ThreeVector table;
  /** Trigger table position during survey */
  G4ThreeVector trig_table;
  /** Rotation matrix for this detector*/
  G4RotationMatrix rotation;
};

class Survey2022{

public:
  /** Name of the detector this survey entry is for */
  G4String detector;
  /** Position of the Detector during the survey*/
  G4ThreeVector pos;
  /** Table position during survey */
  G4ThreeVector table;
  /** Rotation matrix for this detector*/
  G4RotationMatrix rotation;
};

class Alignment {

 public:

    /** X position of the Desy Table **/
    double x_table;
    /** Y position of the Desy Table **/
    double y_table;
    /** Energy of the beam **/
    double beam_energy;
    /** Scan name of the beam **/
    std::string beam_scan;
    /** Type of the beam **/
    std::string beam_type;
    /** First detector met by the beam **/
    std::string upstream_Det;
    /** Second detector met by the beam **/
    std::string mid_Det;
    /** Third detector met by the beam **/
    std::string downstream_Det;
    /** GOLIATH magnet status **/
    bool magnet_On;
    /** Target in **/
    bool target_In;
    /** Lead absorber in **/
    bool lead_In;

};


class Alignment2021 {

 public:

    /** Run number being analyzed **/
    int runNumber;
    /** Scan number **/
    int scanNumber;
    /** Beam type - can be p or e in H2 2021 **/
    std::string beam_type;
    /** Beam energy **/
    double beam_energy;
    /** X position of the NIKHEF Table **/
    double x_det_table;
    /** Y position of the NIKHEF Table **/
    double y_det_table;
    /** X position of the Trigger Table **/
    double x_trig_table;
    /** Y position of the Trigger Table **/
    double y_trig_table;
    /** First detector met by the beam **/
    std::string Det1;
    /** Second detector met by the beam **/
    std::string Det2;
    /** Third detector met by the beam **/
    std::string Det3;
    /** Fourth detector met by the beam **/
    std::string Det4;
    /** Fifth detector met by the beam **/
    std::string Det5;
    /** Sixth detector met by the beam **/
    std::string Det6;
};

class Alignment2022 {

 public:

    /** Shift leader at the time of this run **/
    std::string shiftLeader;
    /** Run number being analyzed **/
    int run;
    /** Scan number **/
    int phase;
    /** Flag for good runs **/
    bool good_run;
    /** X position of the NIKHEF Table **/
    double x_det_table;
    /** Y position of the NIKHEF Table **/
    double y_det_table;
    /** X position of the Trigger Table **/
    double x_trig_table;
    /** Y position of the Trigger Table **/
    double y_trig_table;
    /** Beam energy **/
    double beam_energy;
    /** Beam type - can be p or e in H2 2021 **/
    std::string beam_type;
    /** Detector(s) in the beam **/
    std::string detector;
    /** Main HV setting of the ZDCs **/
    double ZDC_HV;
    /** Booster 1 setting of the ZDCs **/
    double ZDC_b1;
    /** Booster 2 setting of the ZDCs **/
    double ZDC_b2;
    /** Booster 3 setting of the ZDCs **/
    double ZDC_b3;
    /** HV setting for RPD channel group 1 **/
    double RPD_HV1;
    /** HV setting for RPD channel group 2 **/
    double RPD_HV2;
    /** HV setting for RPD channel group 3 **/
    double RPD_HV3;
    /** HV setting for RPD channel group 4 **/
    double RPD_HV4;
    /** HV setting for RPD channel group 4 **/
    double EM_HV;
};


/// Detector construction class to define materials and geometry.

class DetectorConstruction : public G4VUserDetectorConstruction
{
public:
  DetectorConstruction(G4bool);
  virtual ~DetectorConstruction();

  virtual G4VPhysicalVolume* Construct();
  virtual G4VPhysicalVolume* ConstructWorldVolume(G4double x, G4double y, G4double z);
  virtual G4VPhysicalVolume* Construct2018SPSTestBeam();
  virtual G4VPhysicalVolume* Construct2021SPSTestBeam();
  virtual G4VPhysicalVolume* Construct2022SPSTestBeam();
  virtual G4VPhysicalVolume* Construct2023SPSTestBeam();
  virtual G4VPhysicalVolume* ManualConstruction();
  virtual void ConstructSDandField();

  virtual void    LoadConfigurationFile ( G4String _inFile = "" );
  virtual void    LoadAlignmentFile     ( G4String _inFile = "" );
  virtual void    LoadAlignmentFile2021 ( G4String _inFile = "" );
  virtual void    LoadSurveyFile2021    ( G4String _inFile = "" );
  virtual void    LoadAlignmentFile2022 ( G4String _inFile = "" );
  virtual void    LoadSurveyFile2022    ( G4String _inFile = "" );
  virtual void    AddZDC                ( G4ThreeVector* position = NULL, G4RotationMatrix* rotation = NULL );
  virtual void    AddRPD                ( G4ThreeVector* position = NULL, G4RotationMatrix* rotation = NULL );
  virtual void    AddTRI                ( G4ThreeVector* position = NULL, G4RotationMatrix* rotation = NULL );
  virtual void    SetConfigFileName     ( G4String _name ){ m_configFileName = _name; }
  virtual void    SetRunNumber          ( G4int _run ){ m_runNumber = _run; }
  virtual void    SetTestYear           ( G4int _year){ m_year = _year; }
  inline  void    SetOverlapsFlag       ( G4bool arg ){CHECK_OVERLAPS = arg;}
  inline  void    SetOpticalFlag        ( G4bool arg ){OPTICAL = arg;}
  inline  void    SetReducedTreeFlag    ( G4bool arg ){REDUCED_TREE = arg;}
  inline  void    ForcePosition         ( G4bool arg ){ForceDetectorPosition = arg;}
  inline  void    SetPI0Flag            ( G4bool arg ){PI0 = arg;}

  virtual G4RotationMatrix QuaternionToRotationMatrix( TQuaternion Q );
  virtual Survey*       GetSurvey         ( G4String name );
  inline  Alignment     GetAlignment      ( ){ return *m_alignment;    }
  inline  Alignment2021 GetAlignment2021  ( ){ return *m_alignment2021;}
  inline  G4bool        GetOverlapsFlag   ( ){ return CHECK_OVERLAPS;  }
  inline  G4bool        GetOpticalFlag    ( ){ return OPTICAL;         }
  inline  G4bool        GetPI0Flag        ( ){ return PI0;             }
  inline  std::vector< ModTypeZDC* >* GetZDCvec ( ){ return &m_ZDCvec; }
  inline  std::vector< ModTypeRPD* >* GetRPDvec ( ){ return &m_RPDvec; }
  inline  ModTypeTRI* GetTRI ( ){ return m_TRI; }

  //Manual World Volume
  inline  void SetWorldDimensions      ( G4ThreeVector* vec ){ ConstructWorldVolume( vec->x(), vec->y(), vec->z() ); }

  //For manual ZDCs
  inline  void SetZDCPosition          ( G4ThreeVector* vec ){ m_ZDCvec.at(currentZDC-1)->SetPosition(vec);           }
  inline  void SetZDCFiberDiameters    ( G4ThreeVector* vec ){ m_ZDCvec.at(currentZDC-1)->SetFiberDiameters(vec);     }
  inline  void SetZDCAbsorberDimensions( G4ThreeVector* vec ){ m_ZDCvec.at(currentZDC-1)->SetAbsorberDimensions(vec); }
  inline  void SetZDCnAbsorbers        ( G4int          arg ){ m_ZDCvec.at(currentZDC-1)->SetnAbsorbers(arg);         }
  inline  void SetZDCHousingThickness  ( G4double       arg ){ m_ZDCvec.at(currentZDC-1)->SetHousingThickness(arg);   }
  inline  void SetZDCGapThickness      ( G4double       arg ){ m_ZDCvec.at(currentZDC-1)->SetGapThickness(arg);       }
  inline  void SetZDCSteelAbsHeight    ( G4double       arg ){ m_ZDCvec.at(currentZDC-1)->SetSteelAbsHeight(arg);     }
  inline  void SetZDCDetectorType      ( G4String       arg ){ m_ZDCvec.at(currentZDC-1)->SetDetectorType(arg);    }
  inline  void SetZDCOpticalFlag       ( G4bool         arg ){ m_ZDCvec.at(currentZDC-1)->SetOpticalFlag(arg);        }
  inline  void SetZDCOverlapsFlag      ( G4bool         arg ){ m_ZDCvec.at(currentZDC-1)->SetOverlapsFlag(arg);       }
  inline  void SetZDCReducedTreeFlag   ( G4bool         arg ){ m_ZDCvec.at(currentZDC-1)->SetReducedTreeFlag(arg);    }
  inline  void SetZDCHousingMaterial   ( G4String       arg ){ m_ZDCvec.at(currentZDC-1)->SetHousingMaterial(arg);    }
  inline  void SetZDCAbsorberMaterial  ( G4String       arg ){ m_ZDCvec.at(currentZDC-1)->SetAbsorberMaterial(arg);   }
  inline  void SetCurrentZDC           ( G4int          arg ){ currentZDC = arg; }
  virtual void DuplicateZDC            ( G4int       module );


  //For manual RPDs
  inline  void SetRPDPosition          ( G4ThreeVector* vec ){ m_RPDvec.at(currentRPD-1)->SetPosition(vec);        }
  inline  void SetRPDFiberDimensions   ( G4ThreeVector* vec ){ m_RPDvec.at(currentRPD-1)->SetFiberDiameters(vec);  }
  inline  void SetRPDHousingThickness  ( G4double       arg ){ m_RPDvec.at(currentRPD-1)->SetHousingThickness(arg);}
  inline  void SetRPDFiberPitchX       ( G4double       arg ){ m_RPDvec.at(currentRPD-1)->SetFiberPitchX(arg);     }
  inline  void SetRPDFiberPitchZ       ( G4double       arg ){ m_RPDvec.at(currentRPD-1)->SetFiberPitchZ(arg);     }
  inline  void SetRPDTileSize          ( G4double       arg ){ m_RPDvec.at(currentRPD-1)->SetTileSize(arg);        }
  inline  void SetRPDMinWallThickness  ( G4double       arg ){ m_RPDvec.at(currentRPD-1)->SetMinWallThickness(arg);}
  inline  void SetRPDReadoutDistance   ( G4double       arg ){ m_RPDvec.at(currentRPD-1)->SetReadoutDistance(arg); }
  inline  void SetRPDDetectorType      ( G4String       arg ){ m_RPDvec.at(currentRPD-1)->SetDetectorType(arg);    }
  inline  void SetRPDOpticalFlag       ( G4bool         arg ){ m_RPDvec.at(currentRPD-1)->SetOpticalFlag(arg);     }
  inline  void SetRPDOverlapsFlag      ( G4bool         arg ){ m_RPDvec.at(currentRPD-1)->SetOverlapsFlag(arg);    }
  inline  void SetRPDReducedTreeFlag   ( G4bool         arg ){ m_RPDvec.at(currentRPD-1)->SetReducedTreeFlag(arg);    }
  inline  void SetCurrentRPD           ( G4int          arg ){ currentRPD = arg; }
  virtual void DuplicateRPD            ( G4int       module );

  //For manual Trigger Paddles
  inline  void SetTRIPosition          ( G4ThreeVector* vec ){ m_TRI->SetPosition(vec); }

  //For manual lead block positioning
  inline  void SetLeadBlockPosition    ( G4ThreeVector* vec ){ m_leadBlockPos  = vec; }
  inline  void SetLeadBlockSize        ( G4ThreeVector* vec ){ m_leadBlockSize = vec; }
  void ConstructLeadBlock( G4ThreeVector* _size = 0, G4ThreeVector* _pos = 0 );

protected:
  G4ThreeVector*          m_WorldDimensions;
  G4String                m_configFileName;
  Materials*              m_materials;

  /* World objects */
  G4Box*                  m_solidWorld;
  G4LogicalVolume*        m_logicWorld;
  G4VPhysicalVolume*      m_physWorld;

  /* Lead target objects */
  G4LogicalVolume*        logic_leadTarget;
  G4LogicalVolume*        logic_leadBlock;
  G4ThreeVector*          m_leadBlockPos;
  G4ThreeVector*          m_leadBlockSize;

  /* Configuration parser objects and storage */
  XMLSettingsReader*      m_XMLparser;
  Alignment* 	            m_alignment;
  Alignment2021*          m_alignment2021;
  Alignment2022*          m_alignment2022;
  Survey*                 m_survey;
  G4int                   m_runNumber;
  G4int                   m_year;
  std::vector < Survey* > m_surveyEntries;
  std::vector < Survey2021* > m_surveyEntries2021;
  std::vector < Survey2022* > m_surveyEntries2022;

  /* Number of each detector */
  std::vector< ModTypeZDC* > m_ZDCvec;
  std::vector< ModTypeRPD* > m_RPDvec;
  ModTypeTRI*  m_TRI;
  G4int currentZDC;
  G4int currentRPD;
  G4bool isTrigger=false;


private:
	G4Cache<G4MagneticField*> fField;  //pointer to the thread-local fields

  /* Run condition flags */
  G4bool m_GFlash;
  G4bool OPTICAL;
  G4bool REDUCED_TREE;
  G4bool ForceDetectorPosition;
  G4bool CHECK_OVERLAPS;
  G4bool PI0;

  //Fast simulation
  std::vector< G4Region* > m_Region;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
