//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Author: Chad Lantz

#ifndef ModTypeZDC_h
#define ModTypeZDC_h 1

#include "Mod.hh"

/// Detector construction class to define materials and geometry.

class ModTypeZDC : public Mod
{
public:
  ModTypeZDC(const int, ModTypeZDC*);
  ModTypeZDC(const int, G4LogicalVolume*, G4ThreeVector*, G4RotationMatrix* );
  ~ModTypeZDC();

  void  Construct();
  void  ConstructSDandField();

  void  ConstructDetector();


  inline  void  SetFiberDiameters    ( G4ThreeVector* vec ){ delete m_fiberDiam; m_fiberDiam = vec; }
  inline  void  SetAbsorberDimensions( G4ThreeVector* vec ){ delete m_absDim;    m_absDim    = vec; }
  inline  void  SetDetectorOffset    ( G4ThreeVector* vec ){ delete m_detOffset; m_detOffset    = vec; }
  inline  void  SetnAbsorbers        ( G4int          arg ){ m_nAbsorbers        = arg; }
  inline  void  SetSteelAbsHeight    ( G4double       arg ){ STEEL_ABSORBER = true; m_SteelAbsHeight = arg; }
  inline  void  SetGapThickness      ( G4double       arg ){ m_GapThickness      = arg; }
  void  SetAbsorberMaterial          ( G4String  material );
  void  SetHousingMaterial           ( G4String  material );

  inline  G4LogicalVolume* GetAbsorberLogicalVolume( ){ return m_WLogical;    }
  inline  G4Material*      GetAbsorberMaterial     ( ){ return m_matAbsorber; }
  inline  G4int            GetnFibers              ( ){ return m_nFibers;     }

protected:
  G4int             m_nAbsorbers;
  G4int             m_nFibers;
  G4ThreeVector*    m_fiberDiam;
  G4ThreeVector*    m_absDim;
  G4ThreeVector*    m_detOffset;
  G4double          m_GapThickness;
  G4double          m_SteelAbsHeight;
  G4bool            STEEL_ABSORBER;
  G4Material*       m_matAbsorber;

protected:

  G4VSolid*          m_ModuleBox;
  G4LogicalVolume*   m_ModuleLogical;
  G4VPhysicalVolume* m_ModulePhysical;

  G4VSolid*          m_AirScoringBox;
  G4LogicalVolume*   m_AirScoringLogical;
  G4VPhysicalVolume* m_AirScoringPhysical;

  G4VSolid*          m_HousingBox;
  G4LogicalVolume*   m_HousingLogical;
  G4VPhysicalVolume* m_HousingPhysical;

  G4VSolid*          m_W;
  G4LogicalVolume*   m_WLogical;
  std::vector < G4VPhysicalVolume* > m_WPhysical;

  G4VSolid*          m_Steel;
  G4LogicalVolume*   m_SteelLogical;
  std::vector < G4VPhysicalVolume* > m_SteelPhysical;

  // Vertical quartz strips (these strips are full -- no partial segments)
  G4VSolid*          m_FiberCoreTube;
  G4LogicalVolume*   m_FiberCoreLogical;
  std::vector< std::vector < G4VPhysicalVolume* > > m_FiberCorePhysical;

  G4VSolid*          m_CladdingTube;
  G4LogicalVolume*   m_CladdingLogical;
  std::vector< std::vector < G4VPhysicalVolume* > > m_FiberCladdingPhysical;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
