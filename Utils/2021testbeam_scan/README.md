# Each processed scan data of 2021 test beam has two column,
# First column is the profile of X, the second is Y


# The relation between Scans and Runs are as follows:
scan1: run12-15
scan2: run54-98
scan3: run117-136
scan4: run139-157
scan5: run161-178
scan6: run180-197
scan7: run203-220
scan8: run268-326
scan9: run337-364
scan10: run490-600
scan11: run601-638
scan12: run639-682
scan13: run689-761
(Scan up to 757, 758-761 additional statistics in the center (5,5))