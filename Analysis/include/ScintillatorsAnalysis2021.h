/** @file ZDCAnalysis2021
 *  @brief Function prototypes for ZDCAnalysis2021
 *
 *  This contains the prototypes and members
 *  for ScintillatorsAnalysis in 2021 test beam
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef SCINTILLATORSANALYSIS2021_H
#define SCINTILLATORSANALYSIS2021_H

#include "Analysis.h"
#include "TH2D.h"
#include "Containers.h"
#include "Scintillators.h"

class ScintillatorsAnalysis2021 : public Analysis{

 public :
  ScintillatorsAnalysis2021( );
  virtual ~ScintillatorsAnalysis2021( );

  virtual void   Initialize     ( );
  virtual void   Initialize     ( std::vector < Detector* > _vDet);
  virtual void   SetupHistograms( );
  virtual void   AnalyzeEvent   ( );
  virtual void   AnalyzeEvent   ( const std::vector< TH1* >& ){};
  virtual void   AnalyzeEvent   ( const std::vector< std::vector< float > >& ){};
  virtual void   AnalyzeEvent   ( const std::vector< Channel* > ){};
  virtual void   SetBranches    ( TTree* _tree );
  virtual void   Finalize       ( );

 //Histograms

 private :
  /** Pointer to the Large Horizontal Scintillator (Trigger) */
  Scintillators *m_scintillator = 0;
  /** Pointer to the LV channel */
  Channel* m_sciLV = 0;
  /** Pointer to the LH channel */
  Channel* m_sciLH = 0;
  /** Pointer to the SV channel */
  Channel* m_sciSV = 0;
  /** Pointer to the SH channel */
  Channel* m_sciSH = 0;
  /** Charge histo */
  TH2D *hPeak;

};

#endif
