/** @file EM
 *  @brief Function prototypes for EM
 *
 *  This contains the prototypes and members
 *  for EM
 *
 *  @author Aric Tate
 *  @bug No known bugs.
 */

#ifndef EM_H
#define EM_H

#include "Containers.h"
#include "Detector.h"

class EM : public Detector{

 public:
  // using Detector::Detector; //Inherit constructors
  EM( );
  EM( std::string _name );
  ~EM( );

  Channel* GetElement(int row, int column);

  virtual void LoadElements(std::vector< Channel* > _elements, int _runNumber);
  void SetnRows(int rows){ m_nRows=rows; m_nElements = m_nRows * m_nColumns; ResizeSortedElements(); };
  void SetnCols(int cols){ m_nColumns=cols; m_nElements = m_nRows * m_nColumns; ResizeSortedElements(); };
  int GetnRows() { return m_nRows ; };
  int GetnCols() { return m_nColumns ; };
  void ResizeSortedElements();

  virtual void PrintMap( );

 private:
  /** Number of rows **/
  int m_nRows = 3;
  /** Number of columns **/
  int m_nColumns = 3;
  /** Total elements **/
  int m_nElements = m_nRows * m_nColumns;
  /** 2D vector of channels sorted in a [row][column] format **/
  std::vector< std::vector< Channel* > > m_SortedElements;

  /** Translation of row elements for 2021 testbeam
      Converts the tile mapping (row,col)
            (1,3) (1,2) (1,1)          (0,0) (0,1) (0,2)
      from  (2,3) (2,2) (2,1)   to     (1,0) (1,1) (1,2)
            (3,3) (3,2) (3,1)          (2,0) (2,1) (2,2)


      As viewed from the beam souce
  **/
  int rowTranslation [5];
  /** Translation of column elements for 2018 testbeam
      Converts the PMT mapping (row,col)
            (1,3) (1,2) (1,3)          (0,0) (0,1) (0,2)
      from  (2,3) (2,2) (2,3)   to     (1,0) (1,1) (1,2)
            (3,3) (3,2) (3,3)          (2,0) (2,1) (2,2)


  As viewed from the beam source**/
  int colTranslation [5];


};

#endif
