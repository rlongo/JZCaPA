/** @file ZDCAnalysis2021
 *  @brief Function prototypes for ZDCAnalysis2021
 *
 *  This contains the prototypes and members
 *  for ZDCAnalysis in 2021 test beam
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef ZDCANALYSIS2021_H
#define ZDCANALYSIS2021_H

#include "Analysis.h"
#include "TH2D.h"
#include "Containers.h"
#include "Detector.h"

class ZDCAnalysis2021 : public Analysis{

 public :
  ZDCAnalysis2021( );
  virtual ~ZDCAnalysis2021( );

  virtual void   Initialize         ( );
  virtual void   Initialize         ( std::vector < Detector* > _vDet);
  virtual void   SetupHistograms    ( );
  virtual void   AnalyzeEvent       ( );
  virtual void   AnalyzeEvent       ( const std::vector< TH1* >& ){};
  virtual void   AnalyzeEvent       ( const std::vector< std::vector< float > >& ){};
  virtual void   AnalyzeEvent       ( const std::vector< Channel* > ){};
  virtual void   SetBranches        ( TTree* _tree );
  virtual void   PlotWF             ( int eventNum );
  virtual void   PlotWFandDerivative( int eventNum );
  virtual void   PlotWFandPWF       ( int eventNum );
  virtual void   Finalize           ( );


///// Energy histos

  /** Ratio between the Charge detected in the first ZDC and the sum of ZDC1-2-3 **/
  TH1D* hChargeRatio1Tot;
  /** Ratio between the Charge detected in the second ZDC and the sum of ZDC1-2-3 **/
  TH1D* hChargeRatio2Tot;
  /** Ratio between the Charge detected in the third ZDC and the sum of ZDC1-2-3 **/
  TH1D* hChargeRatio3Tot;
  /** Sum of ZDC charges **/
  TH1D* hChargeSum;
  /** Ratio between the peak height of ZDC1 over avg peak height **/
  TH1D* hPeakRatio1;
  /** Ratio between the peak height of ZDC2 over avg peak height **/
  TH1D* hPeakRatio2;
  /** Ratio between the peak height of ZDC3 over avg peak height  **/
  TH1D* hPeakRatio3;
  /** Charge correlation */
  TH2D *hCharge;
  /** ZDC1 Charge */
  TH1D *hCharge1;
  /** ZDC2 Charge */
  TH1D *hCharge2;
  /** ZDC3 Charge */
  TH1D *hCharge3;
  /** Peak height correlation */
  TH2D *hPeak;
  /** ZDC1 peak height */
  TH1D *hPeak1;
  /** ZDC2 peak height */
  TH1D *hPeak2;
  /** ZDC3 peak height */
  TH1D *hPeak3;
  /** Differential peak correlation */
  TH2D *hDpeak;
  /** ZDC1 peak height */
  TH1D *hDpeak1;
  /** ZDC2 peak height */
  TH1D *hDpeak2;
  /** ZDC3 peak height */
  TH1D *hDpeak3;
  /** Charge-Peak correlation, ZDC1 */
  TH2D* hChargePeakZDC1;
  /** Charge-Peak correlation, ZDC2 */
  TH2D* hChargePeakZDC2;
  /** Charge-Peak correlation, ZDC3 */
  TH2D* hChargePeakZDC3;

///// Timing histos

  /* ZDC1 peak center */
  TH1D* hArrival1;
  /* ZDC2 peak center */
  TH1D* hArrival2;
  /* ZDC3 peak center */
  TH1D* hArrival3;
  /* Time of flight ZDC3 - ZDC1 */
  TH1D* hToF;


 private :
  /** Pointer to the first ZDC module */
  Detector *m_zdc1 = 0;
  /** Pointer to the second ZDC module */
  Detector *m_zdc2 = 0;
  /** Pointer to the third ZDC module */
  Detector *m_zdc3 = 0;
  /** Pointer to the first ZDC channel */
  Channel* zdc1 = 0;
  /** Pointer to the second ZDC channel */
  Channel* zdc2 = 0;
  /** Pointer to the third ZDC channel */
  Channel* zdc3 = 0;
  /** Vector of ZDC channels */
  std::vector< Channel* > vZDC;

  /** 1d vector of waveform histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hWFvec;
  /** 1d vector of processed waveform histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hPWFvec;
  /** 1d vector of waveform derivative histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hDiffvec;
  /** 2d vector of TMarkers or TLines to be drawn on the pads. 1st index is channel number, 2nd index is mark to be drawn */
  std::vector< std::vector < TObject* >* >* hMarksFront;
  /** 2d vector of TMarkers or TLines to be drawn on the pads. 1st index is channel number, 2nd index is mark to be drawn */
  std::vector< std::vector < TObject* >* >* hMarksBack;

  int m_counter;


};

#endif
