/** @file EMAnalysis
 *  @brief Function prototypes for EMAnalysis
 *
 *  This contains the prototypes and members
 *  for EMAnalysis
 *
 *  @author Aric Tate
 *  @bug No known bugs.
 */

#ifndef EMANALYSIS_H
#define EMANALYSIS_H

#include "Analysis.h"
#include "TH2D.h"
#include "Containers.h"
#include "EM.h"

#include <vector>

class EMAnalysis : public Analysis{

 public :
  EMAnalysis( );
  virtual ~EMAnalysis( );

  virtual void   Initialize     ( ){};
  virtual void   Initialize     ( std::vector < Detector* > _vDet );
  virtual void   SetupHistograms( );
  virtual void   AnalyzeEvent   ( );
  virtual void   AnalyzeEvent   ( const std::vector< TH1* >& ){};
  virtual void   AnalyzeEvent   ( const std::vector< std::vector< float > >& ){};
  virtual void   AnalyzeEvent   ( const std::vector< Channel* > ){};
  virtual void   SetBranches    ( TTree* _tree );
  virtual void   Finalize       ( );


  /** Running average of charges */
  double m_charge[5][5];
  /** Running average of charges */
  double m_peak[5][5];
  /** Sum of all channel charges */
  double ChargeSum;
  /** Sum of all channel peak heights */
  double PeakSum;
  /** Sum of all channel differential peak heights */
  double DiffPeakSum;
  /** Average charge per tile */
  TH2D *hCharge;
  /** Average peak height per tile */
  TH2D *hPeak;
  /** Calculated center of mass */
  TH2D *hCenter;
  /** Charge vs peak height */
  TH2D *hChgVsPk;
  /** Peak height vs differential peak height */
  TH2D *hPkVsDiffPk;
  /** Charge sum histogram */
  TH1D *hChargeSum;
  /** Peak height sum histogram */
  TH1D *hPeakSum;
  /** Differential peak height sum histogram */
  TH1D *hDiffPeakSum;
  /** Array of charge histograms. One per tile */
  std::vector< std::vector < TH1D* > > hChargeArr;
  /** Array of peak height histograms. One per tile */
  std::vector< std::vector < TH1D* > > hPeakArr;
  /** Array of differential peak histograms. One per tile */
  std::vector< std::vector < TH1D* > > hDPeakArr;
  /** Center of tiles in X mm */
  double xPos[5] = {-10.0,0.0,10.0, 0, 0 }; //CHECK FOR 2021!
  /** Gap between tiles in Z mm */
  double zPos[5] = {10.0,20.0,30.0, 0, 0};  //CHECK FOR 2021!
  /** Estimated position of the beam calculated form table position */
  double m_beamPosX, m_beamPosY;
  /** Number of rows and columns in the EM module */
  int m_numRows, m_numCols;

  int m_counter;

 private :
  /** Pointer to the EM */
  EM *m_EM = 0;
  /** Array of pointers to the EM channels to avoid repeated GetElement() calls */
  Channel* em[5][5];


};

#endif
