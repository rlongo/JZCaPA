/** @file Scintillators
 *  @brief Function prototypes for Scintillators
 *
 *  This contains the prototypes and members
 *  for Scintillators handling - both large (trigger) and small (beam position constraint)
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef SCINTILLATORS_H
#define SCINTILLATORS_H

/*
Mapping scheme for JZCaPA
L = large paddles - for trigger ->
S = small paddles - for beam position constraining
H = horizontal paddles , V = vertical paddles
LV = (1,1) LH = (1,2) SV = (2,1) SH = (2,2)
*/
#include "Detector.h"

class Scintillators : public Detector{

 public:
  // using Detector::Detector; //Inherit constructors
  Scintillators( );
  Scintillators( std::string _name );
  Scintillators(std::vector<Channel *> _readOut, int _runNumber, std::string _name );
  virtual ~Scintillators( );

  virtual void LoadElements(std::vector< Channel* > _elements, int _runNumber);
  void PrintMap  ( );
  std::string FindPaddleType( Channel *c );
  Channel* GetScintillator ( std::string _scintillatorID );

 private:

  int m_runNumber;

  int m_mappingRow[2] = {1,2};
  int m_mappingColumn[2] = {1,2};


};

#endif
