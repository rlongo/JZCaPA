/** @file WFAnalysis
 *  @brief Function prototypes for WFAnalysis
 *
 *  This contains the prototypes and members
 *  for WFAnalysis
 *
 *  @author Yakov Kulinich
 *  @bug No known bugs.
 */

#ifndef WFANALYSIS_H
#define WFANALYSIS_H

#include "Analysis.h"
#include "Containers.h"
#include "TVirtualFFT.h"
#include "TF1.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TF1.h"
#include "TLine.h"


class WFAnalysis : public Analysis{

 public :
  WFAnalysis( );
  virtual ~WFAnalysis( );

  //Housekeeping
  virtual void   Initialize      ( );
  virtual void   Initialize      ( std::vector < Detector* > ){};
  virtual void   SetupHistograms ( );
  virtual void   Finalize        ( );

  //Set analysis parameters
  inline  void   SetHitWindow    ( int _start, int _end ){ m_fixed_hit_window.first = _start; m_fixed_hit_window.second = _end; m_FixHitWindow = true; }

  //Analysis functions
  virtual void   AnalyzeEvent    ( ){};
  virtual void   AnalyzeEvent    ( const std::vector< TH1* >& );
  virtual void   AnalyzeEvent    ( const std::vector< std::vector< float > >& );
  virtual void   AnalyzeEvent    ( const std::vector< Channel* > vCh );
  virtual int    GetMaximumBin   ( TH1* h, int _start, int _end );
  virtual double GetCharge       ( Channel* ch, int start, int end );
  virtual void   GetDifferential ( Channel* Ch );
  virtual double GetRMS          ( Channel* Ch );
  virtual void   GetPedestal     ( Channel* ch );
  virtual void   SubtractPedestal( Channel* ch );
  virtual void   FindHitWindow   ( Channel* ch );
  virtual void   ZeroSuppress    ( Channel* ch );
  virtual void   MedianFilter    ( Channel* ch );
  virtual void   LowPassFilter   ( Channel* ch, TH1D* hIn = 0 );
  virtual void   DRS4Cal         ( Channel* ch );


 private :
  /** Flag if the user has selected a fixed hit window */
  bool m_FixHitWindow = false;
  /** Fixed hit window set by user */
  std::pair< int, int > m_fixed_hit_window;

  /** Histogram used to get derivative RMS */
  TH1D *hDiffHisto;
  /** Histogram used to find pedestal */
  TH1D *hPed = 0;
  /** Fit used to get RMS value */
  TF1 *f = 0;
  /** TF1 used to correct for DRS4 non-linearity */
  TF1 *nlFit = 0;
  /** Input resistance of the DRS4 in ohms */
  double Rin = 50.0;
};

#endif
