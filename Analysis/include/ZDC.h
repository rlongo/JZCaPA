/** @file ZDC
 *  @brief Function prototypes for ZDC
 *
 *  This contains the prototypes and members
 *  for ZDC
 *
 *  @author Chad Lantz, Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef ZDC_H
#define ZDC_H

#include "Detector.h"

class ZDC : public Detector{

 public:
  // using Detector::Detector; //Inherit constructors
  ZDC( );
  ZDC( std::string _name );
  ZDC(std::vector<Channel *> _readOut, int _runNumber, int _zdcNumber);
  ZDC(int);
  virtual ~ZDC( );

  virtual void LoadElements(std::vector< Channel* > _elements, int _runNumber);
  virtual void PrintMap  ( );
  void SetNumber ( int _number ) { m_Number = _number; }

 private:
  int m_Number;

  int m_runNumber;
};

#endif
