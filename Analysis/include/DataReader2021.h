/** @file DataReader2021
 *  @brief Function prototypes for DataReader2021
 *
 *  This contains the prototypes and members
 *  for DataReader2021
 *
 *  @author Yakov Kulinich
 *  @bug No known bugs.
 */

#ifndef DataReader2021_H
#define DataReader2021_H

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <vector>

#include "XMLSettingsReader.h"
#include "Containers.h"
#include "Detector.h"
#include "Analysis.h"
#include "Visualizer.h"

#include <TChain.h>
#include <TSystem.h>

class TFile;

class DataReader2021{

 public:
  DataReader2021( );
  DataReader2021( const unsigned int = 0,  const unsigned int = 0 );
  DataReader2021( const unsigned int = 0,  const unsigned int = 0,
              const std::string& = "" );
  DataReader2021( const unsigned int = 0,  const unsigned int = 0,
              const std::string& = "", const unsigned int = 0 );
  virtual ~DataReader2021();

  void AddPreAnalysis            ( Analysis* );
  void AddDetectorAnalysis       ( Analysis* );
  void AddDetector               ( Detector* );

  void ReadListOfFiles( std::string listname );

  void LoadAlignmentFile     ( std::string _inFile = "" );
  void LoadConfigurationFile ( std::string _inFile = "" );
  void LoadTimingFile        ( std::string _inFile = "" );
  void LoadSurveyFile        ( std::string _inFile = "" );
  void SetPMTgainFile        ( std::string _inFile = "" ){ m_gain_file = _inFile; };
  void SetDebugMode          ( ) { m_debug = true; }
  void SetVerbosity          ( int _level ){ m_verbose = _level; }
  void SetOutputDirectory    ( std::string _dir ){ m_outputDir = _dir; }
  void SetOutputTag          ( std::string _tag ) { m_outputTag = _tag; }
  void UpdateConsole         ( Long_t _updateRate);
  int  GetMaxEvents          ( void ) { return m_maxEvents; }

  void SetMaxEvents          ( int nEventMax ) { m_maxEvents = nEventMax; }
  void EnablePlotLabel       (  ) { m_useLabel = true; }

  Detector*         GetDetector( std::string _detName );
  Alignment2021*    GetAlignment(){ return m_alignment2021; }

  void Run();

  void Initialize   ( );
  void ProcessEvents( );
  void Finalize     ( );

 protected:
  //Input file name
  std::string m_fNameIn;
  //Number of channels to be read
  unsigned int m_nCh;
  //Number of samples per channel
  unsigned int m_nSamp;


 private:
  // output file
  TFile* m_fOut;
  // Output TTree
  TTree* m_tOut;
  // Output directory
  std::string m_outputDir = "";
  // Output tag for file/directory naming
  std::string m_outputTag = "";
  // Root file containing TGraphs of PMT gain curves
  std::string m_gain_file = "";

  //Maximum events to be processed in a run
  int m_maxEvents = -1;
  // vector of pre-detector analysis
  std::vector< Analysis* > m_ana;
  // vector of detector analysis
  std::vector< Analysis* > m_det_ana;
  // Vector of time vectors for DRS4 modules
  std::vector< std::vector< float >* > m_time;

  //Input list of files
  std::string m_fListOfFiles;

  //Run number
  unsigned int m_runNumber;

  //Boolean switch to enable the reading of a list of files
  bool m_readListOfFiles;

  //Input file (in case of a single processing)
  TFile* m_fIn;
  //TChain to accomodate many files (in case of a list of files)
  TChain* m_fileChain;
  //Tree being read
  TTree* m_tIn;

  //Vector of detectors placed in the 2021 setup
  std::vector < Detector* > m_detectors;

  //Vectors of

  //Alignment information for the given run
  Alignment2021* m_alignment2021;

  //XML parser
  XMLSettingsReader *m_XMLparser;

  //Current event
  int m_event;
  //Event number of last update
  int m_event_old = 0;

  //Boolean to enable plot labelling
  bool m_useLabel = false;

  //DebugVariable
  bool m_debug = false;
  //Verbosity level
  int m_verbose = 0;


  //Memory info container for UpdateConsole
  MemInfo_t m_memInfo;
  //CPU info container for UpdateConsole
  CpuInfo_t m_cpuInfo;
  //Process info container for UpdateConsole
  ProcInfo_t m_procInfo;
};

#endif
