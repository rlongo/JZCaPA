//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun Sep 19 13:03:46 2021 by ROOT version 6.16/00
// from TTree Trigger1/Event "Trigger1", ID 1
// found on file: run70043.root
//////////////////////////////////////////////////////////

#ifndef LHCf_Trigger1_h
#define LHCf_Trigger1_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>

// Header file for the classes stored in the TTree if any.

class LHCf_Trigger1 {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           TIME_nTIME;
   UInt_t          TIME_TIME[2];   //[nTIME]
   Int_t           SCL0_nSCL0;
   UInt_t          SCL0_SCL0[1];   //[nSCL0]
   Int_t           GPI0_nGPI0;
   UInt_t          GPI0_GPI0[35];   //[nGPI0]
   Int_t           ADC0_nADC0;
   UShort_t        ADC0_ADC0[64];   //[nADC0]
   Int_t           CAD0_nCAD0;
   UInt_t          CAD0_CAD0[5];   //[nCAD0]
   Int_t           ADC1_nADC1;
   UShort_t        ADC1_ADC1[64];   //[nADC1]
   Int_t           CAD1_nCAD1;
   UInt_t          CAD1_CAD1[5];   //[nCAD1]
   Int_t           ADC2_nADC2;
   UShort_t        ADC2_ADC2[64];   //[nADC2]
   Int_t           ADC3_nADC3;
   UShort_t        ADC3_ADC3[1];   //[nADC3]
   Int_t           TDC0_nTDC0;
   UInt_t          TDC0_TDC0[7];   //[nTDC0]
   Int_t           SCIF_nSCIF;
   UShort_t        SCIF_SCIF[512];   //[nSCIF]
   Int_t           FSIL_nFSIL;
   UChar_t         FSIL_FSIL[15400];   //[nFSIL]
   Int_t           ZDCH_nZDCH;
   UInt_t          ZDCH_ZDCH[12300];   //[nZDCH]

   // List of branches
   TBranch        *b_TIME;   //!
   TBranch        *b_SCL0;   //!
   TBranch        *b_GPI0;   //!
   TBranch        *b_ADC0;   //!
   TBranch        *b_CAD0;   //!
   TBranch        *b_ADC1;   //!
   TBranch        *b_CAD1;   //!
   TBranch        *b_ADC2;   //!
   TBranch        *b_ADC3;   //!
   TBranch        *b_TDC0;   //!
   TBranch        *b_SCIF;   //!
   TBranch        *b_FSIL;   //!
   TBranch        *b_ZDCH;   //!

   LHCf_Trigger1(TTree *tree=0);
   virtual ~LHCf_Trigger1();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   virtual Int_t    GetEntries () { return fChain->GetEntries(); }

};

#endif

#ifdef LHCf_Trigger1_cxx
LHCf_Trigger1::LHCf_Trigger1(TTree *tree) : fChain(0)
{
   if (tree == 0) {
     std::cout << "NULL POINTER FOR TREE! CRASHING" << std::endl;
   }
   Init(tree);
}

LHCf_Trigger1::~LHCf_Trigger1()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t LHCf_Trigger1::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t LHCf_Trigger1::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void LHCf_Trigger1::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("TIME", &TIME_nTIME, &b_TIME);
   fChain->SetBranchAddress("SCL0", &SCL0_nSCL0, &b_SCL0);
   fChain->SetBranchAddress("GPI0", &GPI0_nGPI0, &b_GPI0);
   fChain->SetBranchAddress("ADC0", &ADC0_nADC0, &b_ADC0);
   fChain->SetBranchAddress("CAD0", &CAD0_nCAD0, &b_CAD0);
   fChain->SetBranchAddress("ADC1", &ADC1_nADC1, &b_ADC1);
   fChain->SetBranchAddress("CAD1", &CAD1_nCAD1, &b_CAD1);
   fChain->SetBranchAddress("ADC2", &ADC2_nADC2, &b_ADC2);
   fChain->SetBranchAddress("ADC3", &ADC3_nADC3, &b_ADC3);
   fChain->SetBranchAddress("TDC0", &TDC0_nTDC0, &b_TDC0);
   fChain->SetBranchAddress("SCIF", &SCIF_nSCIF, &b_SCIF);
   fChain->SetBranchAddress("FSIL", &FSIL_nFSIL, &b_FSIL);
   fChain->SetBranchAddress("ZDCH", &ZDCH_nZDCH, &b_ZDCH);
   Notify();
}

Bool_t LHCf_Trigger1::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void LHCf_Trigger1::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}

Int_t LHCf_Trigger1::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef LHCf_Trigger1_cxx
