/** @file EventTimer
 *  @brief Reimplementation of TTimer
 *
 *  This contains the prototypes and members
 *  for EventTimer
 *
 *  @author Chad Lantz
 *  @bug No known bugs.
 */

#ifndef EVENTTIMER_H
#define EVENTTIMER_H

#include "TTimer.h"
#include "DataReader.h"
#include "DataReader2021.h"

class EventTimer : public TTimer {
  public:
    EventTimer( );
    EventTimer(Long_t milliSec = 0, DataReader* obj = 0, Bool_t mode = kTRUE);
    EventTimer(Long_t milliSec = 0, DataReader2021* obj = 0, Bool_t mode = kTRUE);
    ~EventTimer();

    virtual Bool_t Notify();

    DataReader* m_object;
    DataReader2021* m_object2021;
    Long_t m_rate;
};

#endif
