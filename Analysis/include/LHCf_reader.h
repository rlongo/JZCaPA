/** @file LHCf_reader
 *  @brief Function prototypes for LHCf_reader
 *
 *  This contains the prototypes and members for the LHCf reader class
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef LHCF_READER_H
#define LHCF_READER_H

#include "TTree.h"
#include "TFile.h"
#include "TKey.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include <vector>
#include <string>

#include "DataReader2021.h"
#include "LHCf_Trigger1.h"
#include "caen_correction.h"


class LHCf_reader : public DataReader2021{

 public:
   //LHCf_reader( );
   LHCf_reader( const std::string& _fileName = "ND",
                const unsigned int _nCh = 24,
                const unsigned int _nSamples = 1024 );
   virtual ~LHCf_reader( );

   void OpenFile( void );
   void OpenFileName( std::string _fileName );
   void ConvertFile( void );
   void GetEventWaveforms( int iEvent );
   void InitializeHistograms( void );

   void SetSkipPedestal ( bool _wantToSkip ) { m_skipPedestal = _wantToSkip; }
   void SetSkipBeam ( bool _wantToSkip ) { m_skipBeam = _wantToSkip; }
   void SetDebugMode ( void ) { m_debug = true; }

   void ShowEvent( int iEvent );
   void ReadEvents( int iStartEvent = -1, int iEndEvent = -1 );

   std::vector < float > GetGroupSample(unsigned int dwrd_0,
                                        unsigned int dwrd_1,
                                        unsigned int dwrd_2);

   void VisualizeChannels ( void );

 private:
   /*File with LHCf format*/
   TFile* m_filePointer;
   /*Event data from ZDC*/
   LHCf_Trigger1* m_readTrigger1;
   /*Histograms with each channel signal*/
   std::vector < TH1D* > m_v_histoChannels;
   std::vector < TH1D* > m_v_histoChannelsCorr;
   /*Channel vector */
   /*TBR once the xml file loading in JZCaPA*/
   std::vector < Channel* > m_v_Channel;

   //std::vector < Channel* > m_v_digitizedChannels;
   std::vector < std::vector < float > > m_v_Channels;
   std::vector < std::vector < float > > m_v_Channels_Corrected;

   bool m_debug = true;

   /* Flag to skip pedestal events. By default = true - can be changed w/ dedicated method*/
   bool m_skipPedestal = true;
   /* Flag to skip beam events. By default = false - can be changed w/ dedicated method*/
   bool m_skipBeam = false;

   unsigned int m_nGroups = 3;
   unsigned int m_nChPerGroup= 8;

};
#endif
