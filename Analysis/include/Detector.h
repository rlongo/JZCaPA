/** @file Detector
 *  @brief Function prototypes for Detector
 *
 *  This contains the prototypes and members
 *  for Detector
 *
 *  @author Chad Lantz
 *  @bug No known bugs.
 */

#ifndef DETECTOR_H
#define DETECTOR_H

#include <vector>
#include <string>

#include "Containers.h"
#include "Visualizer.h"

#include "TTree.h"
#include "Math/GenVector/EulerAngles.h"
#include "Math/GenVector/Quaternion.h"
#include "Math/GenVector/VectorUtil.h"

class Detector{

 public:
  Detector( );
  Detector( std::string _name );
  Detector( std::vector< Channel* > _element ){ m_Element = _element;}
  virtual ~Detector( );

  virtual void     LoadElements(std::vector< Channel* > _elements, int _runNumber){ std::cout << "It's using the mother class LoadElements()" << std::endl;}
  virtual bool     LoadSurvey  (std::vector< Survey* > _surveys);
  virtual Channel* GetElement  (int row, int column);
  virtual Channel* GetElement  (std::string _name);
  virtual std::vector < Channel* > GetChannelsVector () { return m_Element; }
  virtual ROOT::Math::XYZVector  GetPosition ( );
  virtual ROOT::Math::XYZVector  GetBeamPos  ( );
  virtual ROOT::Math::EulerAngles GetAngles   ( ) { return ROOT::Math::EulerAngles(m_Survey->q); }
  virtual Alignment*  GetAlignment( ) { return m_Alignment; }
  virtual Alignment2021* GetAlignment2021( ) { return m_Alignment2021; }
  virtual Survey*     GetSurvey( ){ return m_Survey; }

  virtual void     SetNSamples      ( int _nSamples )  { m_nSamp = _nSamples; }
  virtual void     SetElement       ( Channel* _entry) { m_Element.push_back(_entry); }
  virtual void     SetBranches      ( TTree* _dataTree );
  virtual void     SetAlignment     ( Alignment* _alignment ){ m_Alignment = _alignment; }
  virtual void     SetAlignment2021 ( Alignment2021* _alignment ){ m_Alignment2021 = _alignment; }
  virtual void     SetWFAthresholds ( double _threshold, int _diffSmoothing ){ m_Threshold = _threshold; m_diffSmoothing = _diffSmoothing; }
  virtual void     SetFFTlowPass    ( int _freq ){ m_LPFfreq = _freq; m_doLPF = true; }
  virtual void     SetMedianFilter  ( int _window ){ m_medHW = _window; m_doMedianFilter = true; }
  virtual void     SetVerbosity     ( int _level ){ m_verbose = _level; }
  virtual void     DoDRS4NLcomp     ( ){ m_doDRS4NLcomp = true; }
  virtual void     DeclareHistograms( );
  virtual void     FillHistograms   ( );

  virtual void     PrintMap           ( ) = 0;
  virtual void     PlotWF             ( int eventNum );
  virtual void     PlotWFandDerivative( int eventNum, double _yMin1=0, double _yMax1=0, double _yMin2=0, double _yMax2=0 );
  virtual void     PlotWFandPWF       ( int eventNum, double _yMin1=0, double _yMax1=0, double _yMin2=0, double _yMax2=0 );

          bool     WasFound(){ return (m_Element.size() > 0) ? true : false; }
          void     SetName ( std::string _name ) { m_name = _name; }
   std::string     GetName ( ) { return m_name; }

  private:
  /** Verbosity level */
  int m_verbose = 0;
  /** Name of the detector **/
  std::string m_name;
  /** Vector of channels associated to the dector **/
  std::vector< Channel* > m_Element;
  /** 1d vector of waveform histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hWFvec;
  /** 1d vector of processed waveform histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hPWFvec;
  /** 1d vector of waveform derivative histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hDiffvec;
  /** 2d vector of TMarkers or TLines to be drawn on the pads. 1st index is channel number, 2nd index is mark to be drawn */
  std::vector< std::vector < TObject* >* >* hMarksFront;
  /** 2d vector of TMarkers or TLines to be drawn on the pads. 1st index is channel number, 2nd index is mark to be drawn */
  std::vector< std::vector < TObject* >* >* hMarksBack;
  /** Position of the center of the active area in beam line coordinates **/
  ROOT::Math::XYZVector m_Position;
  /** Hit threshold to be set for all channels in this detector */
  double m_Threshold = 3.5;
  /** Smoothing factor (number of bins to be summed) in calculation of waveform first derivative */
  int m_diffSmoothing = 25;
  /** Frequency cutoff for low pass filtering of waveforms if selected */
  int m_LPFfreq = 50;
  /** Half window size for median filtering */
  int m_medHW = 0;
  /** Flag if low pass filtering is to be performed on this detector's waveforms */
  bool m_doLPF = false;
  /** Perform DRS4 non-linearity compensation on channels of this detector */
  bool m_doDRS4NLcomp = false;
  /** Perform median filtering on channels of this detector */
  bool m_doMedianFilter = false;
  /** Number of samples per channel **/
  int m_nSamp = 1024;
  /** Run number being processed */
  int m_runNumber;
  /** Visualizer for plots **/
  Visualizer* m_viz = 0;
  /** Alignment of the 2018 Testbeam */
  Alignment* m_Alignment = 0;
  /** Alignment of the 2021 Testbeam */
  Alignment2021* m_Alignment2021 = 0;
  /** Survey for this detector */
  Survey* m_Survey = 0;
  };

#endif
