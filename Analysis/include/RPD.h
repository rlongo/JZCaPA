/** @file RPD
 *  @brief Function prototypes for RPD
 *
 *  This contains the prototypes and members
 *  for RPD
 *
 *  @author Chad Lantz, Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef RPD_H
#define RPD_H

#include "Containers.h"
#include "Detector.h"

class RPD : public Detector{

 public:
  //using Detector::Detector; //Inherit constructors
  RPD( );
  RPD( std::string _name );
  RPD( std::vector< Channel* > _readOut, int _runNumber, std::string _name );
  ~RPD( );

  Channel* GetElement(int row, int column);

  virtual void LoadElements(std::vector< Channel* > _elements, int _runNumber);
  void SetnRows(int _rows){m_nRows = _rows; nElements = m_nRows * m_nColumns; ResizeSortedElements(); };
  void SetnCols(int _cols){m_nColumns= _cols; nElements = m_nRows * m_nColumns; ResizeSortedElements(); };
  void ResizeSortedElements();

  virtual void PrintMap( );

 private:

  /** Number of rows **/
  int m_nRows = 4;
  /** Number of columns **/
  int m_nColumns = 4;
  /** Translation of row elements for 2018 testbeam
      Converts the tile mapping (row,col)
            (1,4) (1,3) (1,2) (1,1)          (0,0) (0,1) (0,2) (0,3)
      from  (2,4) (2,3) (2,2) (2,1)   to     (1,0) (1,1) (1,2) (1,3)
            (3,4) (3,3) (3,2) (3,1)          (2,0) (2,1) (2,2) (2,3)
            (4,4) (4,3) (4,2) (4,1)          (3,0) (3,1) (3,2) (3,3)

      As viewed from the beam souce
  **/
  int rowTranslation [4];
  /** Translation of column elements for 2018 testbeam
      Converts the tile mapping (row,col)
            (1,4) (1,3) (1,2) (1,1)          (0,0) (0,1) (0,2) (0,3)
      from  (2,4) (2,3) (2,2) (2,1)   to     (1,0) (1,1) (1,2) (1,3)
            (3,4) (3,3) (3,2) (3,1)          (2,0) (2,1) (2,2) (2,3)
            (4,4) (4,3) (4,2) (4,1)          (3,0) (3,1) (3,2) (3,3)

  As viewed from the beam souce**/
  int colTranslation [4];
  /** Total elements **/
  int nElements = m_nRows * m_nColumns;
  /** 2D vector of channels sorted in a [row][column] format **/
  std::vector< std::vector< Channel* > > m_SortedElements;

};

#endif
