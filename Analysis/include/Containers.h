/** @file Containers.h
 *  @brief Class to define containers: child classes with only public data memebers
 *
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef CONTAINERS_H
#define CONTAINERS_H

#include "Containers.h"
#include "TH1.h"
#include "TF1.h"
#include "TMath.h"
#include "Math/Vector3D.h"
#include "Math/GenVector/Quaternion.h"

#include <iostream>
#include <vector>
#include <utility>

class Channel {

 public :
    /** 2018 mapping
    *   Detector associated to this channel. RPD or ZDC
    *   Mapping (sitting on the impinging beam) [R,C] for ZDC:
    *   ZDC1 -> [0,1]    |
    *   ZDC2 -> [0,2]   \ /
    *   Mapping (sitting on the impinging beam) [R,C] for RPD:
    *   Jura [1,4] [1,3] [1,2] [1,1] Saleve
    *        [2,4] [2,3] [2,2] [2,1]
    *        [3,4] [3,3] [3,2] [3,1]
    *        [4,4] [4,3] [4,2] [4,1]
    *  2021 mapping will come with the dedicated classes
    */
    /*
     *  Hardware channel information
     */
	  /** Type of detector - ZDC or RPD **/
    std::string detector;
    /** Channel name - CXX with XX in [1,20]  */
    std::string name;
    /** High voltage set for this channel - in V */
    double HV;
    /** Delay set for this channel - in ns */
    double delay;
    /** Offset set for this channel */
    double offset;
    /** Mapping in the horizontal direction [R] */
    int mapping_row;
    /** Mapping in the vertical direction [C] */
    int mapping_column;
    /** Was the channel functioning */
    bool is_on;
    /** Operating voltage of the channel*/
    int Vop;
    /** PMT reference code **/
    std::string PMTcode;
    /** Polarity of the channel */
    bool pos_polarity;
    /** Gain of the PMT for this channel */
    float gain;
    /** ADC to mV conversion factor */
    double adc_mV;
    /*
     *  Waveform storage
     */
    /** Raw waveform for a particular event */
    std::vector < float > WF;
    /** First derivative of the waveform for a particular event */
    std::vector < float > vDiff;
    /** Pointer to the WF vector */
    std::vector < float > *pWF = &WF;
    /** Pointer to the DRS4 time vector */
    std::vector < float > *pTimeVec = 0;
    /** Histrogram for visualization and analysis of the waveform */
    TH1D* WF_histo;
    /** Histogram of the processed waveform */
    TH1D* PWF_histo;
    /** Histogram with first derivative **/
    TH1D* FirstDerivative;
    /** Fitting function for the PMT pulse **/
    TF1* FitFunc;
    /*
    *  Waveform processing variables
    */
    /** Flag if the low pass filtering is to be performed on this channel */
    bool doLPF;
    /** Flag if DRS4 non-linearity compensation is to be performed on this channel */
    bool DRS4NLcomp;
    /** Flag if median filtering is to be performed on this channel  */
    bool doMedianFilter;
    /** Hit threshold multiplier for this channel */
    double Threshold;
    /** Smoothing factor (number of bins to be summed) in calculation of waveform first derivative */
    int diffSmoothing;
    /** Low pass filter frequency */
    int LPFfreq;
    /** Median filtering half window size */
    int medFiltHalfWindow;
    /*
     *  Variables produced by WFAnalysis
     */
    /** Flag if the channel was hit */
    bool was_hit;
    /** Number of hits detected in a single event */
    int nHits;
    /** Pair of vectors containing the start and end of each hit window*/
    std::pair< std::vector< int >, std::vector< int > > hit_window;
    /** RMS value of the first derrivative of the waveform **/
    double FirstDerivativeRMS;
    /** Vector of bin number of the peak location*/
    std::vector< int > Peak_center;
    /** Vector of bin number of the derivative peaks */
    std::vector< int > Diff_Peak_center;
    /** Vector of calibrated time of the peak in ns*/
    std::vector< double > Peak_time;
    /** Vector of calibrated time of max slope for each hit in ns*/
    std::vector< double > Diff_Peak_time;
    /** Vector of height of the peaks */
    std::vector< double > Peak_max;
    /** Vector of max value of the derivative in each hit window */
    std::vector< double > Diff_max;
    /** Vector of integral of PWF_histo in each hit window in pC */
    std::vector< double > Charge;
    /** Approximate number of photons detected by this channel */
    std::vector< float > nPhotons;
    /** Pedestal of raw waveform */
    double PedMean;
    /** Pedestal RMS of raw waveform */
    double PedRMS;
    /** Flag if the waveform saturated */
    bool saturated;

};

class Alignment {

 public:

    /** Run number being analyzed **/
    int runNumber;
    /** X position of the Desy Table **/
    double x_table;
    /** Y position of the Desy Table **/
    double y_table;
    /** First detector met by the beam **/
    std::string upstream_Det;
    /** Second detector met by the beam **/
    std::string mid_Det;
    /** Third detector met by the beam **/
    std::string downstream_Det;
    /** GOLIATH magnet status **/
    bool magnet_On;
    /** Target in **/
    bool target_In;
    /** Lead absorber in **/
    bool lead_In;

};

class Alignment2021 {

 public:

    /** Run number being analyzed **/
    int runNumber;
    /** Beam type - can be p or e in H2 2021 **/
    std::string beam_type;
    /** Beam energy **/
    double beam_energy;
    /** X position of the NIKHEF Table **/
    double x_det_table;
    /** Y position of the NIKHEF Table **/
    double y_det_table;
    /** X position of the Trigger Table **/
    double x_trig_table;
    /** Y position of the Trigger Table **/
    double y_trig_table;
    /** First detector met by the beam **/
    std::string Det1;
    /** Second detector met by the beam **/
    std::string Det2;
    /** Third detector met by the beam **/
    std::string Det3;
    /** Fourth detector met by the beam **/
    std::string Det4;
    /** Fifth detector met by the beam **/
    std::string Det5;
    /** Sixth detector met by the beam **/
    std::string Det6;
};

class Survey{

public:
  /** Name of the detector this survey entry is for */
  std::string detector;
  /** Position of the Detector during the survey*/
  ROOT::Math::XYZVector pos;
  /** Table position during survey */
  ROOT::Math::XYZVector table;
  /** Trigger table position during survey */
  ROOT::Math::XYZVector trig_table;
  /** Quaternion for this detector*/
  ROOT::Math::Quaternion q;
};


#endif
