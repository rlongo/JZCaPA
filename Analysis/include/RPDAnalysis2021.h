/** @file RPDAnalysis2021
 *  @brief Function prototypes for RPDAnalysis2021
 *
 *  This contains the prototypes and members
 *  for RPDAnalysis2021. PLease note that only the PF RPD is currently implemented
 *  tile RPD should be implemented by detector responsibles
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#ifndef RPDANALYSIS2021_H
#define RPDANALYSIS2021_H

#include "Analysis.h"
#include "TH2D.h"
#include "Containers.h"
#include "RPD.h"

#include <vector>

class RPDAnalysis2021 : public Analysis{

 public :
  RPDAnalysis2021( );
  virtual ~RPDAnalysis2021( );

  virtual void   Initialize         ( ){};
  virtual void   Initialize         ( std::vector < Detector* > _vDet );
  virtual void   SetupHistograms    ( );
  virtual void   AnalyzeEvent       ( );
  virtual void   AnalyzeEvent       ( const std::vector< TH1* >& ){};
  virtual void   AnalyzeEvent       ( const std::vector< std::vector< float > >& ){};
  virtual void   AnalyzeEvent       ( const std::vector< Channel* > ){};
  virtual void   SetBranches        ( TTree* _tree );
  virtual void   PlotWF             ( int eventNum );
  virtual void   PlotWFandDerivative( int eventNum );
  virtual void   PlotWFandPWF       ( int eventNum );
  virtual void   Finalize           ( );


  /** Running average of charges */
  double m_charge[4][4];
  /** Running average of charges */
  double m_peak[4][4];
  /** Center of mass x */
  double xCoM;
  /** Center of mass y */
  double yCoM;
  /** Sum of all channel charges */
  double ChargeSum;
  /** Sum of all channel peak heights */
  double PeakSum;
  /** Sum of all channel differential peak heights */
  double DiffPeakSum;
  /** Average charge per tile */
  TH2D *hCharge;
  /** Average peak height per tile */
  TH2D *hPeak;
  /** Calculated center of mass */
  TH2D *hCenter;
  /** Charge vs peak height */
  TH2D *hChgVsPk;
  /** Peak height vs differential peak height */
  TH2D *hPkVsDiffPk;
  /** Charge sum histogram */
  TH1D *hChargeSum;
  /** Photon count histogram */
  TH1D *hPhotons;
  /** Peak height sum histogram */
  TH1D *hPeakSum;
  /** Differential peak height sum histogram */
  TH1D *hDiffPeakSum;
  /** Array of charge histograms. One per tile */
  std::vector< std::vector < TH1D* > > hChargeArr;
  /** Array of peak height histograms. One per tile */
  std::vector< std::vector < TH1D* > > hPeakArr;
  /** Array of differential peak histograms. One per tile */
  std::vector< std::vector < TH1D* > > hDPeakArr;
  /** Center of "tiles" in X mm - tile width for PF RPD is 11.4 mm */
  double xPos[4] = {17.1, 5.7, -5.7, -17.1};
  /** Center of "tiles" in Y mm - tile width for PF RPD is 11.4 mm */
  double yPos[4] = {17.1, 5.7, -5.7, -17.1};
  /** Estimated position of the beam calculated form table position */
  double m_beamPosX, m_beamPosY;

  /** 1d vector of waveform histograms used with Visualizer::ManyPadsPlot() for low level analysis */
  std::vector < TH1* > hWFvec;
  /** 1d vector of waveform histograms used with Visualizer::ManyPadsPlot() for low level analysis */
  std::vector < TH1* > hPWFvec;
  /** 1d vector of waveform derivative histograms used with Visualizer for low level analysis */
  std::vector < TH1* > hDiffvec;
  /** 2d vector of TMarkers or TLines to be drawn on the foreground pads. 1st index is channel number, 2nd index is mark to be drawn */
  std::vector< std::vector < TObject* >* >* hMarksFront;
  /** 2d vector of TMarkers or TLines to be drawn on the background pads. 1st index is channel number, 2nd index is mark to be drawn */
  std::vector< std::vector < TObject* >* >* hMarksBack;

  int m_counter;

 private :
  /** Pointer to the RPD */
  RPD *m_RPD = 0;
  /** Array of pointers to the RPD channels to avoid repeated GetElement() calls */
  Channel* rpd[4][4];


};

#endif
