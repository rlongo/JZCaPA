message("${BoldYellow}----------------------------")
message("Preparing Analysis libraries...")
message("----------------------------${ColourReset}")

include_directories(${CMAKE_SOURCE_DIR})
include_directories(${CMAKE_SOURCE_DIR}/Analysis/include)

#In the following the list of source files to be linked
set (ANALYSIS_SRC_DIR "${CMAKE_SOURCE_DIR}/Analysis/src")

#### Get all header files
FILE(GLOB_RECURSE PA_INC
    "${CMAKE_SOURCE_DIR}/Analysis/include/*.hh"
)
FILE(GLOB_RECURSE DEPENDENCIES
    "${CMAKE_SOURCE_DIR}/Analysis/include/*.hh"
    "${CMAKE_SOURCE_DIR}/Analysis/src/*.cpp"
)

add_library(analysis_lib SHARED ${DEPENDENCIES})
TARGET_LINK_LIBRARIES(analysis_lib ${ROOT_LIBRARIES})
TARGET_LINK_LIBRARIES(analysis_lib ${XERCESC_LIBRARY})

####
   
add_executable(AnalysisExample userFunctions/AnalysisExample.cpp)
TARGET_LINK_LIBRARIES(AnalysisExample analysis_lib)

install(TARGETS AnalysisExample RUNTIME DESTINATION bin)

####

add_executable(AnalysisExample2021 userFunctions/AnalysisExample2021.cpp)
TARGET_LINK_LIBRARIES(AnalysisExample2021 analysis_lib)

install(TARGETS AnalysisExample2021 RUNTIME DESTINATION bin)

####

add_executable(myAnalysis userFunctions/myAnalysis.cpp)
TARGET_LINK_LIBRARIES(myAnalysis analysis_lib)

install(TARGETS myAnalysis RUNTIME DESTINATION bin)

####

add_executable(postAnalysis userFunctions/postAnalysis.cpp)
TARGET_LINK_LIBRARIES(postAnalysis analysis_lib)

install(TARGETS postAnalysis RUNTIME DESTINATION bin)

####

#add_executable(postAnalysisZDC userFunctions/postAnalysisZDC.cpp)
#TARGET_LINK_LIBRARIES(postAnalysisZDC analysis_lib)

#install(TARGETS postAnalysisZDC RUNTIME DESTINATION bin)

####

#add_executable(postAnalysisRPD userFunctions/postAnalysisRPD.cpp)
#TARGET_LINK_LIBRARIES(postAnalysisRPD analysis_lib)

#install(TARGETS postAnalysisRPD RUNTIME DESTINATION bin)

####
add_executable(convertLHCf userFunctions/convertLHCf.cpp)
TARGET_LINK_LIBRARIES(convertLHCf analysis_lib)
install(TARGETS convertLHCf RUNTIME DESTINATION bin)

add_executable(RPD_Matrix_Analysis userFunctions/RPD_Matrix_Analysis.cpp)
TARGET_LINK_LIBRARIES(RPD_Matrix_Analysis analysis_lib)

install(TARGETS RPD_Matrix_Analysis RUNTIME DESTINATION bin)
