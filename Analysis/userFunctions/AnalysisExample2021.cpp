/** @file AnalysisExample.cpp
 *  @brief Example of a simple analysis: read and plot all the w
 *
 *
 *  @author Aric Tate
 *  @bug No known bugs.
 */

#include "DataReader.h"
#include "DataReader2021.h"
#include "ZDCAnalysis2021.h"
#include "RPDAnalysis2021.h"
#include "ScintillatorsAnalysis2021.h"
#include "EMAnalysis.h"
#include "WFAnalysis.h"
#include "EventTimer.h"
#include <vector>
#include <dirent.h>

//using namespace std;

namespace {
  void PrintUsage() {
    std::cout << " Usage: AnalysisExample2021 [-d /path/to/input/files/ ] [-f /list/of.root /files/here.root]" << std::endl
              << "                            [-o /output/directory/] [-c /config/file.xml] [-a /alignment/file.xml]" << std::endl
              << "                            [-s /survey/file.xml] [-t /timing/file.txt] [-tag tag] [-n #events]" << std::endl
              << "                            [-g /gain/file.root] [-v (verbose)] [--help]" << std::endl;
  }
}

void PrintHelp(){
  PrintUsage();

  std::cout << std::endl;
  std::cout << "    f    List of files to be processed (must include path)" << std::endl;
  std::cout << "    d    Path to directory with files to be processed" << std::endl;
  std::cout << "    o    Output file directory" << std::endl;
  std::cout << "    n    Maximum number of events to be processed for a given run" << std::endl;
  std::cout << "    c    Configuration file to be used" << std::endl;
  std::cout << "    a    Alignment file to be used" << std::endl;
  std::cout << "    s    Survey file to be used" << std::endl;
  std::cout << "    t    Timing file to be used" << std::endl;
  std::cout << "    g    Path to root file containing TGraphs with PMT gain curves" << std::endl;
  std::cout << "    tag  Tag the output directory/file" << std::endl;
  std::cout << "    v    Verbose. Currently prints event#, CPU/RAM usage if flag is used. Takes no argument" << std::endl;

}

std::vector<std::string> open_directory(const std::string path_dir){
  DIR *directory;
  dirent *pdir;
  std::vector<std::string> files_name;

  directory = opendir(path_dir.c_str());

  while((pdir = readdir(directory))) {
    std::string file_name = pdir->d_name;
    if(file_name.substr(file_name.find_last_of(".") + 1) == "root") {
      files_name.push_back(file_name);
    }
  }
  return files_name;
}

int get_runnumber(const std::string file_name){
  // remove ".root"
  std::string result = file_name.substr(0, file_name.find_last_of("."));
  // find nunNumber
  result = result.substr(result.find_last_not_of("0123456789") + 1);
  return atoi(result.c_str());
}


int main(int argc, char *argv[]){

  // can put arguments here, for now I will just do defaults.

  int nCh    = 64;            // 4 ch for scintillators, 9 ch for Upgraded EM, 16 ch for PF RPD, 3 ch for the HADs
  int nSamp  = 1024;          // Default number of samples?
  int runNum;                 // Will be Extracted from file name
  int maxEvents = 0;
  std::string fNameIn = "";   // Read from directory
  std::string output_dir = "";
  std::string path_to_datafiles = "";
  std::string config_file = "";
  std::string alignment_file = "";
  std::string survey_file = "";
  std::string gain_file = "";
  std::string output_tag = "";
  std::string timing_file = (std::string)std::getenv("JZCaPA") + "/Utils/Timing_data/2021_PreliminaryTiming.txt";
  int verbosity = 0;
  std::vector<std::string> root_files;


  //Parse the arguments
  for(int i=1; i<argc; i+=2 ){
         if(TString(argv[i]) == "-d"){ root_files = open_directory(argv[i+1]); path_to_datafiles = argv[i+1]; }//Read all the files with .root extension in the following directory
    else if(TString(argv[i]) == "-f"){// Read a list of files provided by the user
      for(i+=1; i<argc; i++){
        if(!TString(argv[i]).Contains("-")) root_files.push_back(argv[i]);
        else{
          i-=2;//Return i so the next time around it's on a flag
          break;
        }
      }
    }
    else if(TString(argv[i]) == "-o") output_dir = argv[i+1];
    else if(TString(argv[i]) == "-n") maxEvents = atoi(argv[i+1]);
    else if(TString(argv[i]) == "-c") config_file = argv[i+1];
    else if(TString(argv[i]) == "-a") alignment_file = argv[i+1];
    else if(TString(argv[i]) == "-s") survey_file = argv[i+1];
    else if(TString(argv[i]) == "-g") gain_file = argv[i+1];
    else if(TString(argv[i]) == "-t") timing_file = argv[i+1];
    else if(TString(argv[i]) == "-tag") output_tag = argv[i+1];
    else if(TString(argv[i]) == "-v") verbosity = 1;
    else if(TString(argv[i]) == "--help") PrintHelp();
    else{
      PrintUsage();
      return 1;
    }
  }

  std::cout << std::endl;
  for(int idx=0;idx<root_files.size();idx++){
    //Open a file after finding what's the run number
    std::string nameManip = root_files[idx];
    fNameIn = Form("%s%s", path_to_datafiles.c_str(), root_files[idx].c_str());
    runNum  = get_runnumber( root_files[idx] );

    // DataReader is the main class. It reads data and also
    // has analysis classes in it. User should only have to
    // modify the analysis classes and add output in them.
    // User has to add their analysis to DataReader.
    std::cout << Form("DataReader2021 ==> nCh[%d], nSamp[%d], fNameIn[\"%s\"], runNum[%d]", nCh, nSamp, fNameIn.c_str(), runNum ) << std::endl << std::endl;

    DataReader2021* r = new DataReader2021( nCh, nSamp, fNameIn , runNum );
    r->SetVerbosity(1);
    if(output_tag != "") r->SetOutputTag( output_tag );
    if(output_dir != "") r->SetOutputDirectory( output_dir );


    //ADD DETECTORS AND WF ANALYSIS VARIABLES

    //Trigger paddles
    Scintillators *trig = new Scintillators("TRIGGER");
    trig->SetWFAthresholds( 4, 25 );
    trig->SetMedianFilter( 4 );
    r->AddDetector( trig );

    //ATLAS ZDC
    ZDC *zdc1 = new ZDC(1);
    zdc1->SetWFAthresholds( 5, 20 );
    zdc1->SetMedianFilter( 4 );
    r->AddDetector( zdc1 );

    //ATLAS ZDC
    ZDC *zdc2 = new ZDC(2);
    zdc2->SetWFAthresholds( 5, 20 );
    zdc2->SetMedianFilter( 4 );
    r->AddDetector( zdc2 );

    //ATLAS ZDC
    ZDC *zdc3 = new ZDC(3);
    zdc3->SetWFAthresholds( 5, 20 );
    zdc3->SetMedianFilter( 4 );
    r->AddDetector( zdc3 );

    //Pan Flute RPD
    RPD *rpd = new RPD("PFRPD");
    rpd->SetWFAthresholds( 3.2, 28 );
    rpd->SetMedianFilter( 4 );
    r->AddDetector( rpd );

    //Tile RPD
    // RPD *rpd = new RPD("TRPD");
    // rpd->SetWFAthresholds( 3.2, 28 );
    // rpd->SetMedianFilter( 4 );
    // r->AddDetector( rpd );

    //Upgraded EM
    EM *em = new EM("UEM");
    em->SetWFAthresholds( 4, 25 );
    em->SetMedianFilter( 4 );
    r->AddDetector( em );

    //CMS EM cal
    // EM *em = new EM("CMSEM");
    // em->SetWFAthresholds( 4, 25 );
    // em->SetMedianFilter( 4 );
    // r->AddDetector( em );


    std::cout << std::endl << "============ LoadConfigurationFile ============"  << std::endl << std::endl;
    r->SetPMTgainFile( gain_file );
    r->LoadConfigurationFile( config_file );

    //Add pre analysis
    r->AddPreAnalysis( new WFAnalysis() );

    //ADD DETECTORS FOR ANALYSIS (PLOTTING/OUTPUT)
    if( zdc1->WasFound() && zdc2->WasFound() && zdc3->WasFound() ) r->AddDetectorAnalysis( new ZDCAnalysis2021() );
    if( rpd->WasFound()  ) r->AddDetectorAnalysis( new RPDAnalysis2021() );
    if( em->WasFound()   ) r->AddDetectorAnalysis( new EMAnalysis() );
    if( trig->WasFound() ) r->AddDetectorAnalysis( new ScintillatorsAnalysis2021() );

    std::cout << std::endl << "============ LoadAlignmentFile ============"  << std::endl << std::endl;
    r->LoadAlignmentFile( alignment_file );

    std::cout << std::endl << "============ LoadSurveyFile ============"  << std::endl << std::endl;
    r->LoadSurveyFile( survey_file );

    std::cout << std::endl << "============ LoadTimingFile ============"  << std::endl << std::endl;
    r->LoadTimingFile( timing_file );

    r->EnablePlotLabel();
    if( maxEvents != 0 )r->SetMaxEvents( maxEvents );

    EventTimer timer(1000, r, kFALSE);
    if(verbosity > 0) timer.TurnOn();

    std::cout << "~=~=~=~=~=~ Run Start"  << std::endl;
    r->Run();
    std::cout << std::endl << "~=~=~=~=~=~ Run Stop"  << std::endl;

    if(verbosity > 0) timer.TurnOff();

    delete r;
    }

  return 0;
}
