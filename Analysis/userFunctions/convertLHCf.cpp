/** @file ConvertLHCf.cpp
 *  @brief Short code to convert LHCf data format and check its content
 *  @scope To be defined
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include "LHCf_reader.h"

using namespace std;

int main(int argc, char *argv[]){

  string fNameIn = argv[1];
  cout << "Will analyize file " << fNameIn << endl;

  LHCf_reader* myReader = new LHCf_reader( fNameIn.c_str() );
  myReader->OpenFile();

  //myReader->ReadEvents( 215, 220  );
  myReader->ConvertFile();

  //myReader->VisualizeChannels();
  return 0;
}
