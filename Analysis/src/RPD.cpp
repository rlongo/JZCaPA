/** @ingroup ana
 *  @file RPD.cpp
 *  @brief Implementation of RPD.
 *
 *  Function definitions for RPD are provided.
 *  This is a daughter class of Detector.
 *  Methods specific to RPDs are implemented here.
 *
 *  @author Chad Lantz
 *  @bug No known bugs.
 */

#include "RPD.h"

#include <string>
#include <stdio.h>


/** @brief Default Constructor for RPD.
 */
RPD::RPD( ){

}

/** @brief Name Constructor for RPD.
 */
RPD::RPD( std::string _name ){
  SetName(_name);
}

/** @brief Destructor for RPD.
 */
RPD::~RPD( ){

}


/** @brief Legacy Constructor for the RPD class that takes the whole vector of channels readout and selects and stores only the RPD ones
 *  @param _elements Vector of Channels with all detector configs
 *  @param _runNumber Current run number being analyzed
 *  @param _name Identifier for the name of the model of the prototype - that distinguish PF from Tile
 *  Creates a new RPD type Detector and picks the relevant Channels from the input vector.
 *
*/
RPD::RPD( std::vector < Channel* > _elements, int _runNumber, std::string _name ){

      SetName( _name );
      //Set up the row translation if run number is from 2018 test beam
      //TRPD is now the identifier for tile RPD prototypes
      for(int i = 0; i < 4; i++){
        if(_runNumber >=79 && _runNumber <= 413 && GetName() == "TRPD18" ){//UPDATE FOR 2021 IF NEEDED, ELSE DELETE
          rowTranslation[i] = i+1;
          colTranslation[i] = 4-i;
        }else{
          //Same column-row association for both tile and pan flute at the moment
          //Expert in analysis should comment and eventually modify
          rowTranslation[i] = i+1;//ARIC CHANGED
          colTranslation[i] = i+1;
        }
      }

      ResizeSortedElements();
      //std::cout << std::endl << "PANFLUTE RPD TILE-SIGNAL MAPPING" << std::endl << "---------------------------------" << std::endl;
      for(int row = 0; row < m_nRows; row++){
          for(int column = 0; column < m_nColumns; column++){
              for(int i=0; i < (int)_elements.size(); i++){
                  if(_elements.at(i)->mapping_row == rowTranslation[row] && _elements.at(i)->mapping_column == colTranslation[column]
                      && _elements.at(i)->detector == "PFRPD"){
                      m_SortedElements[row][column] = _elements.at(i);
                      // std::cout <<  Form("Config: X_Col[%d], Y_Row[%d] -> Array: X_Col[%d], Y_Row[%d] -> Channel[%s] -> Det[%s]",
                      //               colTranslation[column], rowTranslation[row],  column, row,
                      //               m_SortedElements[row][column]->name.c_str(),
                      //               m_SortedElements[row][column]->detector.c_str()) << std::endl;
                      SetElement(_elements.at(i));
                  }
              }
          }
      }

      if(GetChannelsVector().size() > 0)
        std::cout << std::endl << GetName() << " object created with " << m_nRows << " rows and " << m_nColumns << " columns" << std::endl;

}
/** @brief Constructor that takes the whole vector of channels readout and selects and stores only the RPD ones
 *  @param _elements Vector of Channels with all detector configs
 *  @param _runNumber Current run number being analyzed
 *
 *  Adds the RPD channels to the channels vector
 *
*/
void RPD::LoadElements( std::vector < Channel* > _elements, int _runNumber){

    //Set up the row translation if run number is from 2018 test beam
    //TRPD is now the identifier for tile RPD prototypes
    for(int i = 0; i < 4; i++){
      if(_runNumber >=79 && _runNumber <= 413 && GetName() == "TRPD18" ){//UPDATE FOR 2021 IF NEEDED, ELSE DELETE
        rowTranslation[i] = i+1;
        colTranslation[i] = 4-i;
      }else{
        //Same column-row association for both tile and pan flute at the moment
        //Expert in analysis should comment and eventually modify
        rowTranslation[i] = i+1;//ARIC CHANGED
        colTranslation[i] = i+1;
      }
    }

    ResizeSortedElements();
    //std::cout << std::endl << "PANFLUTE RPD TILE-SIGNAL MAPPING" << std::endl << "---------------------------------" << std::endl;
    for(int row = 0; row < m_nRows; row++){
        for(int column = 0; column < m_nColumns; column++){
            for(int i=0; i < (int)_elements.size(); i++){
                if(_elements.at(i)->mapping_row == rowTranslation[row] && _elements.at(i)->mapping_column == colTranslation[column]
                    && _elements.at(i)->detector == "PFRPD"){
                    m_SortedElements[row][column] = _elements.at(i);
                    // std::cout <<  Form("Config: X_Col[%d], Y_Row[%d] -> Array: X_Col[%d], Y_Row[%d] -> Channel[%s] -> Det[%s]",
                    //               colTranslation[column], rowTranslation[row],  column, row,
                    //               m_SortedElements[row][column]->name.c_str(),
                    //               m_SortedElements[row][column]->detector.c_str()) << std::endl;
                    SetElement(_elements.at(i));
                }
            }
        }
    }

    if(GetChannelsVector().size() > 0)
      std::cout << std::endl << GetName() << " object created with " << m_nRows << " rows and " << m_nColumns << " columns" << std::endl;

}


/** @brief Get the properties of a detector element
 *  @param row Row of element to be accessed
 *  @param column Column of element to be accessed
 *  @return Pointer to the Channel of the requested element
 *
 * If m_SortedElements is populated, return the element in
 * m_SortedElements[row][column].
 * Otherwise returns a pointer to the Channel stored in m_Element
 * with the requested row and column.
 * If the requested element is not found, return a warning message and a NULL pointer.
 *
 */
Channel* RPD::GetElement(int row, int column){
    if( ((int)m_SortedElements.size()) == m_nRows){
      return m_SortedElements[row][column];
    }else{
        for(int i=0; i < (int)GetChannelsVector().size(); i++){
            if(rowTranslation[row] == GetChannelsVector().at(i)->mapping_row && colTranslation[column] == GetChannelsVector().at(i)->mapping_column){
              return GetChannelsVector().at(i);
            }

        }
     }
  std::cerr << " WARNING: Element (" << row << "," << column << ") not found! " << std::endl;
  return nullptr;

}

/** @brief Prints a map of the RPD to the terminal
 *
 * Prints the map of a 4x4 RPD.
 * Displays a grid of elements with row,column on the top line, DRS4
 * Channel on the second line, and if the element is functioning on the
 * third line.
 *
 */
void RPD::PrintMap(){
  //RPD has 4 rows and 4 columns
  for(int col = 0; col <= 3; col++){
      std::string status;
      for(int row = 0; row <= 3; row++){
          if(GetElement(col,row)->is_on) status = "ON";
              else status = "OFF";
          if(row != 3) std::cout << "| " << col << "," << row << " --> \t " << GetElement(col,row)->name.c_str() << " , " << status;
          else         std::cout << "| " << col << "," << row << " --> \t " << GetElement(col,row)->name.c_str() << " , " << status << " |" << std::endl;
        }//End of the loop over columns
      }//End of the loop over rows
}

/** @brief Resizes m_SortedElements
 *
 * Uses m_nRows and m_nColumns to resize m_SortedElements
 * 2D vectors must have columns sized individually.
 *
 */
void RPD::ResizeSortedElements(){
    m_SortedElements.resize(m_nRows);
    for(int row=0; row < m_nRows; row++){
        m_SortedElements.at(row).resize(m_nColumns);
    }
}
