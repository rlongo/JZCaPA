/** @ingroup ana
 *  @file Detector.cpp
 *  @brief Implementation of Detector.
 *
 *  Function definitions for Detector are provided.
 *  This is the mother class for detectors.
 *  Methods common to all detectors are implemented here.
 *
 *  @author Chad Lantz, Riccardo Longo
 *  @bug No known bugs.
 */

#include "Detector.h"
#include "Containers.h"

#include "TLine.h"
#include "TMarker.h"
#include "TSystem.h"

#include <vector>


/** @brief Default Constructor for Detector.
 */
Detector::Detector( ){

}

/** @brief Name Constructor for Detector.
 */
Detector::Detector( std::string _name ){
  m_name = _name;
}

/** @brief Destructor for Detector.
 */
Detector::~Detector( ){

}


/** @brief Load the survey into this detector
 *  @param Vector of surveys for all detectors
 *
 *  Searches for the relevant survey within the input vector
 *
 */
bool Detector::LoadSurvey( std::vector< Survey* > _surveys ){

  for(int i = 0; i < _surveys.size(); i++){
    if(_surveys[i]->detector == m_name){
      m_Survey = _surveys[i];
      return true;
    }
  }

  return false;
}


/** @brief Get the position of the center detector active area in beamline coordinates
 *
 */
ROOT::Math::XYZVector Detector::GetPosition(){

  //If the position hasn't been set, calculate it
  if(m_Position.x() == 0 && m_Position.y() == 0 && m_Position.z() == 0){
    float x,y;
    if(m_Alignment != 0){
      x = m_Alignment->x_table - m_Survey->table.x() + m_Survey->pos.x();
      y = m_Alignment->y_table - m_Survey->table.y() + m_Survey->pos.y();
    }
    if(m_Alignment2021 != 0){
      x = m_Alignment2021->x_det_table - m_Survey->table.x() + m_Survey->pos.x();
      y = m_Alignment2021->y_det_table - m_Survey->table.y() + m_Survey->pos.y();
    }
    m_Position.SetXYZ(x,y,m_Survey->pos.z());
  }

  return m_Position;
}

/** @brief Get the position of the beam relitave to the center of the active area of the detector
 *
 */
ROOT::Math::XYZVector Detector::GetBeamPos(){
  ROOT::Math::XYZVector pos = GetPosition();
  ROOT::Math::XYZVector beamPos;

  //For now just switch from beam coordinates to detector coordinates.
  //Future behavior should involve data from wire chambers
  beamPos.SetXYZ(-pos.x(),-pos.y(),0.0);
  return beamPos;
}

/** @brief Get the properties of a detector element
 *  @param row Row of element to be accessed
 *  @param column Column of element to be accessed
 *  @return Pointer to the Channel of the requested element
 *
 * Returns a pointer to the Channel stored in m_Elements with the
 * requested row and column.
 * If the requested element is not found, return a warning message and a NULL pointer.
 *
 */
Channel* Detector::GetElement(int row, int column){

    for(int i=0; i < (int)m_Element.size(); i++){
            if(row == m_Element[i]->mapping_row && column == m_Element[i]->mapping_column){
            return m_Element[i];
    }
  }
  std::cerr << " WARNING: Element (" << row << "," << column << ") not found! " << std::endl;
  return nullptr;

}

/** @brief Get the properties of a detector element
 *
 * Returns a pointer to the Channel stored in m_Elements with the
 * requested channel name.
 * If the requested element is not found, return a warning message and a NULL pointer.
 *
 */
Channel* Detector::GetElement(std::string _name){

    for(int i=0; i < (int)m_Element.size(); i++){
            if(!_name.compare(m_Element.at(i)->name)) return m_Element[i];
  }
  std::cerr << " WARNING: Element (" << _name << ") not found! " << std::endl;
  return nullptr;

}

/**
 * @brief Set the branches of the tree to the channels of the detectors (according to their name, read from the mapping)
 * @param _dataTree : processed data tree
 */
void Detector::SetBranches( TTree *_dataTree ){

    for( uint ch = 0; ch < m_Element.size(); ch++ ){
      if( m_Element[ch]->is_on) _dataTree->SetBranchAddress( ("Raw" + m_Element.at(ch)->name).c_str(), &m_Element.at(ch)->pWF );
    }

}

/**
 * @brief Declare histograms to be filled with the raw waveform
 */
void Detector::DeclareHistograms(){
  hMarksFront = new std::vector< std::vector< TObject* >* >;
  hMarksBack = new std::vector< std::vector< TObject* >* >;

    for( uint ch = 0; ch < m_Element.size(); ch++ ){
        m_Element.at(ch)->WF_histo        = new TH1D( m_Element.at(ch)->name.c_str(), (m_Element.at(ch)->name + ", " + m_Element.at(ch)->detector).c_str(), m_nSamp, 0, m_nSamp);
        m_Element.at(ch)->PWF_histo       = new TH1D((m_Element.at(ch)->name + " Processed").c_str(),  (m_Element.at(ch)->name + ", " + m_Element.at(ch)->detector + " Processed").c_str() , m_nSamp, 0, m_nSamp);
        m_Element.at(ch)->FirstDerivative = new TH1D((m_Element.at(ch)->name + " Derivative").c_str(), (m_Element.at(ch)->name + ", " + m_Element.at(ch)->detector + " Derivative").c_str(), m_nSamp, 0, m_nSamp);
        m_Element.at(ch)->FitFunc         = new TF1 ((m_Element.at(ch)->name + "logNormal").c_str()  , "[A]*ROOT::Math::lognormal_pdf(x,[m],[s],[x0])" );
        m_Element.at(ch)->FitFunc->SetParLimits(0,0.1,500000);
        m_Element.at(ch)->FitFunc->SetParLimits(1,1,5);
        m_Element.at(ch)->FitFunc->SetParLimits(2,0.01,1.0);
        m_Element.at(ch)->FitFunc->SetParLimits(3,20,1000);
        m_Element.at(ch)->Threshold = m_Threshold;
        m_Element.at(ch)->diffSmoothing = m_diffSmoothing;
        m_Element.at(ch)->doLPF = m_doLPF;
        m_Element.at(ch)->LPFfreq = m_LPFfreq;
        m_Element.at(ch)->DRS4NLcomp = m_doDRS4NLcomp;
        m_Element.at(ch)->doMedianFilter = m_doMedianFilter;
        m_Element.at(ch)->medFiltHalfWindow = m_medHW;


        hWFvec.push_back(m_Element.at(ch)->WF_histo);
        hPWFvec.push_back(m_Element.at(ch)->PWF_histo);
        hDiffvec.push_back(m_Element.at(ch)->FirstDerivative);
        hMarksFront->push_back( new std::vector< TObject* > );
        hMarksBack->push_back( new std::vector< TObject* > );
    }

}

/**
 * @brief Fill histograms with the current raw waveform
 */
void Detector::FillHistograms(){
    for( uint ch = 0; ch < m_Element.size(); ch++ ){
        if (!m_Element.at(ch)->is_on) continue;
        m_Element.at(ch)->WF_histo->Reset();
        m_Element.at(ch)->PWF_histo->Reset();
        m_Element.at(ch)->FirstDerivative->Reset();
        m_Element.at(ch)->vDiff.clear();
        m_Element.at(ch)->FirstDerivativeRMS = 0;
        m_Element.at(ch)->FitFunc->SetParameters(25000,0.5,2,500);
        // Loop over samples in each channel
        for( uint samp = 0; samp < m_nSamp; samp++ ){
          m_Element.at(ch)->WF_histo->SetBinContent( samp + 1, m_Element.at(ch)->WF[ samp ] );
          } // End loop over samples in each channel
        }
}

/** @brief Generic implementation of PlotWF. Draw the waveforms for all Channels for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void Detector::PlotWF( int eventNum ){
  if(eventNum == 0){//If this is the first event create an output directory
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  for(int ch = 0; ch < m_Element.size(); ch++){
    //Delete and clear old markers
    for(int i = 0; i < hMarksFront->at(ch)->size(); i++){
      if( !hMarksFront->at(ch)->at(i)->InheritsFrom("TF1") ){
        delete hMarksFront->at(ch)->at(i);
      }
    }
    hMarksFront->at(ch)->clear();

    //Draw the pedestal no matter what
    hMarksFront->at(ch)->push_back( new TLine( 0, m_Element[ch]->PedMean, m_nSamp, m_Element[ch]->PedMean) );
    ((TLine*)hMarksFront->at(ch)->back())->SetLineColor(kGreen);

    //Draw hit window lines, peak marker, and set the range of the fit function to the full hit window
    if(m_Element[ch]->is_on){
      m_Element[ch]->WF_histo->ResetStats();
      if(m_Element[ch]->was_hit){
        for(int hit = 0; hit < m_Element[ch]->nHits; hit++){
          //Draw vertical red lines around the hit window
          hMarksFront->at(ch)->push_back( new TLine( m_Element[ch]->hit_window.first[hit], m_Element[ch]->WF_histo->GetMinimum(), m_Element[ch]->hit_window.first[hit], m_Element[ch]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksFront->at(ch)->back())->SetLineColor(kRed);
          hMarksFront->at(ch)->push_back( new TLine( m_Element[ch]->hit_window.second[hit], m_Element[ch]->WF_histo->GetMinimum(), m_Element[ch]->hit_window.second[hit], m_Element[ch]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksFront->at(ch)->back())->SetLineColor(kRed);
          hMarksFront->at(ch)->push_back( new TMarker( m_Element[ch]->Peak_center[hit], m_Element[ch]->Peak_max[hit] + m_Element[ch]->PedMean, 5) );
          ((TMarker*)hMarksFront->at(ch)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksFront->at(ch)->back())->SetMarkerSize(3);
        }//end hit loop
      }//end if channel was hit
    }//end if channel is on
  }//end channels loop

  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(m_Alignment2021->runNumber, m_Alignment2021 );
  }
  int pads = ceil(sqrt(m_Element.size()));
  m_viz->SimplePadsPlot( hWFvec, pads, pads, "Time [bins]", "Amplitude [ADC]", //Background histos, foreground histos, nCol, nRow, Detector label
                         Form("%swf%d.png", m_name.c_str(), eventNum), m_name.c_str(), "", // Output file name, chule name, plot type
                         Form("%s/results/plots/", std::getenv("JZCaPA") ), // Output directry
                         true, hMarksFront ); // Autoscale, Plot markers
}



void Detector::PlotWFandDerivative( int eventNum, double _yMin1, double _yMax1, double _yMin2, double _yMax2 ){
  if(eventNum == 0){//If this is the first event create an output directory
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  for(int ch = 0; ch < m_Element.size(); ch++){
    //Delete and clear old markers
    for(int i = 0; i < hMarksBack->at(ch)->size(); i++){
      delete hMarksBack->at(ch)->at(i);
    }
    hMarksBack->at(ch)->clear();
    for(int i = 0; i < hMarksFront->at(ch)->size(); i++){
      if( !hMarksFront->at(ch)->at(i)->InheritsFrom("TF1") ){ // Don't delete the fit functions
        delete hMarksFront->at(ch)->at(i);
      }
    }
    hMarksFront->at(ch)->clear();


    //Draw the pedestal no matter what
    hMarksBack->at(ch)->push_back( new TLine( 0, m_Element[ch]->PedMean, m_nSamp, m_Element[ch]->PedMean) );
    ((TLine*)hMarksBack->at(ch)->back())->SetLineColor(kGreen);

    //Plot the threshold lines on the derivative no matter what
    hMarksFront->at(ch)->push_back( new TLine( 0, m_Element[ch]->FirstDerivativeRMS*m_Element[ch]->Threshold, m_nSamp, m_Element[ch]->FirstDerivativeRMS*m_Element[ch]->Threshold) );
    ((TLine*)hMarksFront->at(ch)->back())->SetLineColor(kMagenta);
    hMarksFront->at(ch)->push_back( new TLine( 0, -1*m_Element[ch]->FirstDerivativeRMS*m_Element[ch]->Threshold, m_nSamp, -1*m_Element[ch]->FirstDerivativeRMS*m_Element[ch]->Threshold) );
    ((TLine*)hMarksFront->at(ch)->back())->SetLineColor(kMagenta);

    //Draw hit window lines, peak marker, and set the range of the fit function to the full hit window
    if(m_Element[ch]->is_on){
      m_Element[ch]->WF_histo->GetYaxis()->SetRange(0,0);
      m_Element[ch]->WF_histo->ResetStats();

      if(m_Element[ch]->was_hit){
        for(int hit = 0; hit < m_Element[ch]->nHits; hit++){
          //Draw vertical red lines around the hit window
          hMarksBack->at(ch)->push_back( new TLine( m_Element[ch]->hit_window.first[hit], -2950, m_Element[ch]->hit_window.first[hit], -2600) );
          ((TLine*)hMarksBack->at(ch)->back())->SetLineColor(kRed);
          hMarksBack->at(ch)->push_back( new TLine( m_Element[ch]->hit_window.second[hit], -2950, m_Element[ch]->hit_window.second[hit], -2600) );
          ((TLine*)hMarksBack->at(ch)->back())->SetLineColor(kRed);

          //Draw an X on the peak
          hMarksBack->at(ch)->push_back( new TMarker( m_Element[ch]->Peak_center[hit], m_Element[ch]->Peak_max[hit] + m_Element[ch]->PedMean, 5) );
          ((TMarker*)hMarksBack->at(ch)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksBack->at(ch)->back())->SetMarkerSize(3);
        }//end hit loop
      }//end if channel was hit
    }//end if channel is on
  }//end channel loop
  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(m_Alignment2021->runNumber, m_Alignment2021 );
  }
  int pads = ceil(sqrt(m_Element.size()));
  m_viz->ManyPadsPlot( hWFvec, hDiffvec, pads, pads, m_name.c_str(), //Background histos, foreground histos, nCol, nRow, Detector label
                       Form("%s/results/plots/%swfDiff_ev%d", std::getenv("JZCaPA"), m_name.c_str(), eventNum ), // Output file name
                       "Time [bins]", "Amplitude [ADC]", "overlay", //X-axis label, Y-axis label, plot type
                       hMarksBack, hMarksFront, //Background plot markers, Foreground plot markers
                       _yMin1, _yMax1, _yMin2, _yMax2); //Y axis ranges


}



void Detector::PlotWFandPWF( int eventNum, double _yMin1, double _yMax1, double _yMin2, double _yMax2 ){
  if(eventNum == 0){//If this is the first event create an output directory
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  for(int ch = 0; ch < m_Element.size(); ch++){
    //Delete and clear old markers
    for(int i = 0; i < hMarksBack->at(ch)->size(); i++){
      delete hMarksBack->at(ch)->at(i);
    }
    hMarksBack->at(ch)->clear();
    for(int i = 0; i < hMarksFront->at(ch)->size(); i++){
      if( !hMarksFront->at(ch)->at(i)->InheritsFrom("TF1") ){
        delete hMarksFront->at(ch)->at(i);
      }
    }
    hMarksFront->at(ch)->clear();


    //Draw the pedestal no matter what
    hMarksBack->at(ch)->push_back( new TLine( 0, m_Element[ch]->PedMean, m_nSamp, m_Element[ch]->PedMean) );
    ((TLine*)hMarksBack->at(ch)->back())->SetLineColor(kGreen);

    //Draw hit window lines, peak marker, and set the range of the fit function to the full hit window
    if(m_Element[ch]->is_on){
      m_Element[ch]->WF_histo->ResetStats();
      if(m_Element[ch]->was_hit){
        for(int hit = 0; hit < m_Element[ch]->nHits; hit++){
          //Draw vertical red lines around the hit window
          hMarksBack->at(ch)->push_back( new TLine( m_Element[ch]->hit_window.first[hit], m_Element[ch]->WF_histo->GetMinimum(), m_Element[ch]->hit_window.first[hit], m_Element[ch]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksBack->at(ch)->back())->SetLineColor(kRed);
          hMarksBack->at(ch)->push_back( new TLine( m_Element[ch]->hit_window.second[hit],m_Element[ch]->WF_histo->GetMinimum(), m_Element[ch]->hit_window.second[hit], m_Element[ch]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksBack->at(ch)->back())->SetLineColor(kRed);
          hMarksBack->at(ch)->push_back( new TMarker( m_Element[ch]->Peak_center[hit], m_Element[ch]->Peak_max[hit] + m_Element[ch]->PedMean, 5) );
          ((TMarker*)hMarksBack->at(ch)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksBack->at(ch)->back())->SetMarkerSize(3);
          // hMarksFront->at(ch)->push_back( m_Element[ch]->FitFunc );
          // ((TF1*)hMarksFront->at(ch)->back())->SetRange(m_Element[ch]->hit_window.first, m_Element[ch]->hit_window.second);
          // ((TF1*)hMarksFront->at(ch)->back())->SetLineColor(kCyan);
        }//end hit loop
      }//end if channel was hit
    }//end if channel is on
  }//end channel loop
  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(m_Alignment2021->runNumber, m_Alignment2021 );
  }
  int pads = ceil(sqrt(m_Element.size()));
  m_viz->ManyPadsPlot( hWFvec, hPWFvec, pads, pads, m_name.c_str(), //Background histos, foreground histos, nCol, nRow, Detector label
                       Form("%s/results/plots/%swfPwfPlot_ev%d",std::getenv("JZCaPA"), m_name.c_str(), eventNum ),  // Output file name
                       "Time [bins]", "Amplitude [ADC]", "overlay", //X-axis label, Y-axis label, plot type
                       hMarksBack, hMarksFront, //Background plot markers, Foreground plot markers
                       _yMin1, _yMax1, _yMin2, _yMax2); //Y axis ranges
}
