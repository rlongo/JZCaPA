/** @ingroup ana
 *  @file ZDC.cpp
 *  @brief Implementation of ZDC.
 *
 *  Function definitions for ZDC are provided.
 *  This is a daughter class of Detector.
 *  Methods specific to ZDCs are implemented here.
 *
 *  @author Chad Lantz
 *  @bug No known bugs.
 */

#include "ZDC.h"


/** @brief Default Constructor for ZDC.
 */
ZDC::ZDC( ){

}

/** @brief Name Constructor for ZDC.
 */
ZDC::ZDC( std::string _name ){
  SetName(_name);
}

/** @brief Legacy constructor for the ZDC class. Will be removed soon
 *  @param _readOut Vector of Channels with all detector configs
 *  @param _runNumber Current run number being analyzed
 *  @param _zdcNumber Number of the ZDC module
 *
 *  Adds the ZDC channel to the channels vector
 *
 */
ZDC::ZDC( std::vector < Channel* > _readOut, int _runNumber, int _zdcNumber){

    m_Number = _zdcNumber;
    m_runNumber = _runNumber;

    SetName ( Form ("HAD%d", m_Number) );

    for(int i = 0; i < (int)_readOut.size(); i++){
        if((_readOut.at(i)->detector.find_first_of("H") != std::string::npos) && (_readOut.at(i)->mapping_column == m_Number)){
            SetElement(_readOut.at(i));
        }
    }
    if(GetChannelsVector().size() > 1) std::cout << "WARNING : more than one entry for one ZDC module. Check the config.xml" << std::endl;
    std::cout << "ZDC object created with " << GetChannelsVector().size() << " channel entries " << std::endl;
}


/** @brief Number Constructor for ZDC.
 */
ZDC::ZDC(int _zdcNumber){
  m_Number = _zdcNumber;
  SetName ( Form ("HAD%d", m_Number) );
}

/** @brief Destructor for ZDC.
 */
ZDC::~ZDC( ){

}


/** @brief Implementation of LoadElements for the ZDC class
 *  @param _readOut Vector of Channels with all detector configs
 *  @param _runNumber Current run number being analyzed
 *
 *  Adds the ZDC channel to the channels vector
 *
 */
void ZDC::LoadElements( std::vector < Channel* > _elements, int _runNumber){

    m_runNumber = _runNumber;

    for(int i = 0; i < (int)_elements.size(); i++){
        if((_elements.at(i)->detector.find_first_of("H") != std::string::npos) && (_elements.at(i)->mapping_column == m_Number)){
            SetElement(_elements.at(i));
        }
    }
    if(GetChannelsVector().size() > 1)
      std::cout << "WARNING : more than one entry for one ZDC module. Check the config.xml" << std::endl;

    if(GetChannelsVector().size() > 0)
      std::cout << "ZDC object created with " << GetChannelsVector().size() << " channel entries " << std::endl;
}


/** @brief Prints a map of the ZDC to the terminal
 *
 * Prints a "map" of the ZDC.
 * Displays one element with ZDC number on the top line, DRS4
 * Channel on the second line, and if the element is functioning on the
 * third line.
 *
 */
void ZDC::PrintMap(){
        Channel *c = GetElement(0,m_Number);

        std::cout<<"|   "     << m_Number  <<"  |"<<std::endl;
	std::cout<<"|   "     << c->name <<"  |"<<std::endl;
	std::cout<<"|       |"<< std::endl;
	std::cout<<"|_______|"<< std::endl;

}
