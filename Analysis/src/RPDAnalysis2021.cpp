/** @ingroup ana
 *  @file RPDAnalysis2021.cpp
 *  @brief Implementation of RPDAnalysis2021.
 *
 *
 *  Function definitions for RPDAnalysis2021 are provided.
 *  This class is the main class for the PF RPD analysis.
 *
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
*/


#include "RPDAnalysis2021.h"
#include "RPD.h"
#include "Containers.h"
#include "TMarker.h"
#include "TLine.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TSystem.h"


/** @brief Default Constructor for RPDAnalysis2021.
 */
RPDAnalysis2021::RPDAnalysis2021( ){

}

/** @brief Destructor for RPDAnalysis2021.
 */
RPDAnalysis2021::~RPDAnalysis2021( ){

}

/** @brief Initialization method for RPDAnalysis2021
 *
 *  Takes a vector of detectors, picks out the RPD
 *  and channels, then assigns them to member pointers
 *
 */
void RPDAnalysis2021::Initialize( std::vector < Detector* > _vDet ){
  m_counter = 0;

    for( auto& det : _vDet ){
        if(det->GetChannelsVector().size() == 0){
          std::cout << "=======================   GetChannelsVector.size() == 0 !!!   =======================" <<std::endl;
          continue;
        }
        for( auto& chan : det->GetChannelsVector() ){
            if(chan->detector == "PFRPD" ){
                m_RPD = (RPD*) det;
                m_alignment2021 = m_RPD->GetAlignment2021();
                for(int row = 0; row < 4; row++){
                    for(int col = 0; col < 4; col++){
                        rpd[row][col] = m_RPD->GetElement(row,col);
                    }
                }
            }
        }
    }

    hMarksFront = new std::vector< std::vector< TObject* >* >;
    hMarksBack = new std::vector< std::vector< TObject* >* >;
    for(int row = 0; row < 4; row++){
      for(int col = 0; col < 4; col++){
        hWFvec.push_back( rpd[row][col]->WF_histo );
        hPWFvec.push_back( rpd[row][col]->PWF_histo );
        hDiffvec.push_back( rpd[row][col]->FirstDerivative );
        hMarksBack->push_back( new std::vector< TObject* > );
        hMarksFront->push_back( new std::vector< TObject* > );
      }
    }

    beamX = m_RPD->GetBeamPos().x();
    beamY = m_RPD->GetBeamPos().y();

}

/** @brief Historgam Setup method for RPDAnalysis2021
 *
 *  Should instantiate any histograms you wish to output here.
 *
 *  @return none
 */
void RPDAnalysis2021::SetupHistograms( ){

   hCharge     = new TH2D("RPD Charge", "average charge per tile", 4, 0, 4, 4, 0, 4);
   hPeak       = new TH2D("RPD Peak", "average peak height per tile", 4, 1, 5, 4, 1, 5);
   //hCenter     = new TH2D("Shower Center", "Calculated center of mass", 200, -50, 50, 200, -50, 50);
   hChgVsPk    = new TH2D("Charge vs Peak height","Q vs Peak; Q; Peak", 300, 0, 300000, 300, 0, 1100);
   hPkVsDiffPk = new TH2D("Peak height vs Differential peak height","Peak vs Diff Peak", 200, 0, 5000, 200, 0, 300);
   hChargeSum  = new TH1D("RPD Integrated Signal","RPD Integrated Signal", 200, 0, 160000);
   hPhotons    = new TH1D("RPD nPhotons", "Total number of photons", 100, 0, 50);
   hPeakSum    = new TH1D("RPD Peak Height Sum","RPD Peak Height Sum", 200, 0, 3000);
   hDiffPeakSum= new TH1D("RPD Differential Peak Height Sum","RPD Diff Peak Sum", 200, 0, 55000);

   hChargeArr.resize(4);
   hPeakArr.resize(4);
   hDPeakArr.resize(4);
   for(int row = 0; row < 4; row++){
     hChargeArr[row].resize(4);
     hPeakArr[row].resize(4);
     hDPeakArr[row].resize(4);
       for(int col = 0; col < 4; col++){
            hChargeArr[row][col] = new TH1D(Form("rpd%d_%d_Charge",row,col),Form("rpd%d_%d_Charge",row,col), 200 , 0, 15000);
            hChargeArr[row][col]->SetAxisRange(0,1500,"Y");

            hPeakArr[row][col]   = new TH1D(Form("rpd%d_%d_Peak",row,col),Form("rpd%d_%d_Peak",row,col), 200, 0, 300);
            hPeakArr[row][col]->SetAxisRange(0,1500,"Y");

            hDPeakArr[row][col]  = new TH1D(Form("rpd%d_%d_Diff_peak",row,col),Form("rpd%d_%d_Diff_peak",row,col), 200, 0, 5000);
            hDPeakArr[row][col]->SetAxisRange(0,1500,"Y");
       }
   }
}

/** @brief Branch setup method for ZDCAnalysis
 *
 *  Adds branches with data created by the analysis
 *
 */
void RPDAnalysis2021::SetBranches( TTree* _tree ){
    m_AnalysisTree = _tree;

    m_AnalysisTree->Branch( "rpd_BeamX", &beamX, "rpd_BeamX/D");
    m_AnalysisTree->Branch( "rpd_BeamY", &beamY, "rpd_BeamY/D");

    for(int row = 0; row < 4; row++){
        for(int col = 0; col < 4; col++){
            m_AnalysisTree->Branch( Form("rpd%d_%d_Charge", row, col),           "std::vector<double>", &rpd[row][col]->Charge            );
            m_AnalysisTree->Branch( Form("rpd%d_%d_nPhotons", row, col),         "std::vector<float>",  &rpd[row][col]->nPhotons          );
            m_AnalysisTree->Branch( Form("rpd%d_%d_Peak_max", row, col),         "std::vector<double>", &rpd[row][col]->Peak_max          );
            m_AnalysisTree->Branch( Form("rpd%d_%d_Diff_max", row, col),         "std::vector<double>", &rpd[row][col]->Diff_max          );
            m_AnalysisTree->Branch( Form("rpd%d_%d_Peak_center", row, col),      "std::vector<int>",    &rpd[row][col]->Peak_center       );
            m_AnalysisTree->Branch( Form("rpd%d_%d_Diff_Peak_center", row, col), "std::vector<int>",    &rpd[row][col]->Diff_Peak_center  );
        }
    }

    //m_AnalysisTree->Branch("rpd_xCoM", &xCoM, "xCoM/D" );
    //m_AnalysisTree->Branch("rpd_yCoM", &yCoM, "yCoM/D" );
    m_AnalysisTree->Branch("rpd_Charge_sum", &ChargeSum, "ChargeSum/D" );
    m_AnalysisTree->Branch("rpd_Peak_sum",   &PeakSum,   "PeakSum/D" );
    m_AnalysisTree->Branch("rpd_Diff_Peak_sum",   &DiffPeakSum,   "DiffPeakSum/D" );

}

/** @brief Analyze Events method for RPDAnalysis2021
 *
 *
 */
void RPDAnalysis2021::AnalyzeEvent( ){
    ChargeSum = 0;
    PeakSum = 0;
    DiffPeakSum = 0;
    float nPhotons = 0.0;

    //This loop takes the running average of charge and peak height per tile
    for(int row = 0; row < 4; row++){
        for(int col = 0; col < 4; col++){
            if( rpd[row][col]->is_on ){
              for(int hit = 0; hit < rpd[row][col]->nHits; hit++){
                  ChargeSum   += rpd[row][col]->Charge[hit];
                  PeakSum     += rpd[row][col]->Peak_max[hit];
                  DiffPeakSum += rpd[row][col]->Diff_max[hit];

                  if(rpd[row][col]->gain != 0.0){
                    nPhotons += rpd[row][col]->nPhotons[hit];
                  }
                  hChargeArr[row][col]->Fill(rpd[row][col]->Charge[hit]);
                  hPeakArr[row][col]->Fill(rpd[row][col]->Peak_max[hit]);
                  hDPeakArr[row][col]->Fill(rpd[row][col]->Diff_max[hit]);
               }//end hit loop
           }//end if channel is on
       }//end column loop
   }//end row loop
   hPhotons->Fill(nPhotons);

    //This loop finds the center of mass per event (should probably be it's own function)

    double totalCharge = 0;
    double weightedRow = 0;
    double weightedCol = 0;
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          if( rpd[i][j]->is_on && rpd[i][j]->was_hit ){
              for(int hit = 0; hit < rpd[i][j]->nHits; hit++){
                    totalCharge += rpd[i][j]->Charge[hit];
                    weightedRow += rpd[i][j]->Charge[hit] * yPos[i];
                    weightedCol += rpd[i][j]->Charge[hit] * xPos[j];
                }//end hit loop
            }//end if channel is on
        }//end column loop
    }//end row loop

    //To draw low level plots of the waveforms primarily for validation of the WFAnalysis
    // PlotWF( m_counter );
    // PlotWFandPWF( m_counter );
    // PlotWFandDerivative( m_counter );

    m_counter++;

    hChargeSum->Fill(ChargeSum);
    hPeakSum->Fill(PeakSum);
    hDiffPeakSum->Fill(DiffPeakSum);

}

/** @brief Draw the waveforms for all RPD channels for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void RPDAnalysis2021::PlotWF(int eventNum){
  if(m_counter == 0){
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  int tile = 0;
  for(int row = 0; row < 4; row++){
    for(int col = 0; col < 4; col++){
      for(int i = 0; i < hMarksFront->at(tile)->size(); i++){
        if( !hMarksFront->at(tile)->at(i)->InheritsFrom("TF1") ){
          delete hMarksFront->at(tile)->at(i);
        }
      }
      hMarksFront->at(tile)->clear();

      //Draw pedestal no matter what
      hMarksFront->at(tile)->push_back( new TLine( 0, rpd[row][col]->PedMean, 1024, rpd[row][col]->PedMean) );
      ((TLine*)hMarksFront->at(tile)->back())->SetLineColor(kGreen);


      if(rpd[row][col]->was_hit){
        for(int hit = 0; hit < rpd[row][col]->nHits; hit++){
          //Hit window start
          hMarksFront->at(tile)->push_back( new TLine( rpd[row][col]->hit_window.first[hit], rpd[row][col]->WF_histo->GetMinimum(), rpd[row][col]->hit_window.first[hit], rpd[row][col]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksFront->at(tile)->back())->SetLineColor(kRed);
          //Hit window end
          hMarksFront->at(tile)->push_back( new TLine( rpd[row][col]->hit_window.second[hit], rpd[row][col]->WF_histo->GetMinimum(), rpd[row][col]->hit_window.second[hit], rpd[row][col]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksFront->at(tile)->back())->SetLineColor(kRed);
          //Place an X on the peak
          hMarksFront->at(tile)->push_back( new TMarker( rpd[row][col]->Peak_center[hit], rpd[row][col]->Peak_max[hit] + rpd[row][col]->PedMean, 5) );
          ((TMarker*)hMarksFront->at(tile)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksFront->at(tile)->back())->SetMarkerSize(3);
        }//end hit loop
      }//end if was hit
      tile++;
    }//end column loop
  }//end row loop

  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(502, m_alignment2021 );
  }
  m_viz->SimplePadsPlot( hWFvec, 4, 4, "Time [bins]", "Amplitude [ADC]", //Background histos, foreground histos, nCol, nRow, Detector label
                         Form("RPDwf%d.png", m_counter), "PFRPD", "", // Output file name, Module name, plot type
                         Form("%s/results/plots/", std::getenv("JZCaPA") ), // Output directry
                         true, hMarksFront ); // Autoscale, Plot markers

}

/** @brief Draw the waveforms and processed waveforms for all RPD channels for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void RPDAnalysis2021::PlotWFandPWF(int eventNum){
  if(m_counter == 0){
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  int tile = 0;
  for(int row = 0; row < 4; row++){
    for(int col = 0; col < 4; col++){

      //Delete and clear old markers
      for(int i = 0; i < hMarksBack->at(tile)->size(); i++){
        delete hMarksBack->at(tile)->at(i);
      }
      hMarksBack->at(tile)->clear();
      for(int i = 0; i < hMarksFront->at(tile)->size(); i++){
        if( !hMarksFront->at(tile)->at(i)->InheritsFrom("TF1") ){
          delete hMarksFront->at(tile)->at(i);
        }
      }
      hMarksFront->at(tile)->clear();

      //Draw the pedestal no matter what
      hMarksBack->at(tile)->push_back( new TLine( 0, rpd[row][col]->PedMean, 1024, rpd[row][col]->PedMean) );
      ((TLine*)hMarksBack->at(tile)->back())->SetLineColor(kGreen);

      if(rpd[row][col]->was_hit){
        for(int hit = 0; hit < rpd[row][col]->nHits; hit++){
          //Hit window start
          hMarksBack->at(tile)->push_back( new TLine( rpd[row][col]->hit_window.first[hit], rpd[row][col]->WF_histo->GetMinimum(), rpd[row][col]->hit_window.first[hit], rpd[row][col]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksBack->at(tile)->back())->SetLineColor(kRed);
          //Hit window end
          hMarksBack->at(tile)->push_back( new TLine( rpd[row][col]->hit_window.second[hit], rpd[row][col]->WF_histo->GetMinimum(), rpd[row][col]->hit_window.second[hit], rpd[row][col]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksBack->at(tile)->back())->SetLineColor(kRed);
          //Place an X on the peak
          hMarksBack->at(tile)->push_back( new TMarker( rpd[row][col]->Peak_center[hit], rpd[row][col]->Peak_max[hit] + rpd[row][col]->PedMean, 5) );
          ((TMarker*)hMarksBack->at(tile)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksBack->at(tile)->back())->SetMarkerSize(3);
          //Plot the fit function on the processed waveform
          // hMarksFront->at(tile)->push_back( rpd[row][col]->FitFunc );
          // ((TF1*)hMarksFront->at(tile)->back())->SetRange(rpd[row][col]->hit_window.first[hit], rpd[row][col]->hit_window.second[hit]);
          // ((TF1*)hMarksFront->at(tile)->back())->SetLineColor(kCyan);
        }//end hit loop
      }//end if channel was hit
      tile++;
    }//end column loop
  }//end row loop

  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(502, m_alignment2021 );
  }

  m_viz->ManyPadsPlot( hWFvec, hPWFvec, 4, 4, "PF RPD", //Background histos, foreground histos, nCol, nRow, Detector label
                       Form("%s/results/plots/RPDwfPwfPlot_ev%d", std::getenv("JZCaPA"), m_counter ), // Output file name
                       "Time [bins]", "Amplitude [ADC]", "overlay", //X-axis label, Y-axis label, plot type
                       hMarksBack, hMarksFront, //Background plot markers, Foreground plot markers
                       -3300, -2550, -10, 700); //Background yMin, Background yMax, Foreground yMin, Foreground yMax

  }


/** @brief Draw the waveforms and waveform derivatives for all RPD channels for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void RPDAnalysis2021::PlotWFandDerivative(int eventNum){
  if(m_counter == 0){
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  int tile = 0;
  for(int row = 0; row < 4; row++){
    for(int col = 0; col < 4; col++){

      //Delete and clear old markers
      for(int i = 0; i < hMarksBack->at(tile)->size(); i++){
        delete hMarksBack->at(tile)->at(i);
      }
      hMarksBack->at(tile)->clear();
      for(int i = 0; i < hMarksFront->at(tile)->size(); i++){
        if( !hMarksFront->at(tile)->at(i)->InheritsFrom("TF1") ){
          delete hMarksFront->at(tile)->at(i);
        }
      }
      hMarksFront->at(tile)->clear();

      //Draw the pedestal no matter what
      hMarksBack->at(tile)->push_back( new TLine( 0, rpd[row][col]->PedMean, 1024, rpd[row][col]->PedMean) );
      ((TLine*)hMarksBack->at(tile)->back())->SetLineColor(kGreen);

      //Plot the threshold lines on the derivative no matter what
      hMarksFront->at(tile)->push_back( new TLine( 0, rpd[row][col]->FirstDerivativeRMS*rpd[row][col]->Threshold, 1024, rpd[row][col]->FirstDerivativeRMS*rpd[row][col]->Threshold) );
      ((TLine*)hMarksFront->at(tile)->back())->SetLineColor(kMagenta);
      hMarksFront->at(tile)->push_back( new TLine( 0, -1*rpd[row][col]->FirstDerivativeRMS*rpd[row][col]->Threshold, 1024, -1*rpd[row][col]->FirstDerivativeRMS*rpd[row][col]->Threshold) );
      ((TLine*)hMarksFront->at(tile)->back())->SetLineColor(kMagenta);

      if(rpd[row][col]->was_hit){
        for(int hit = 0; hit < rpd[row][col]->nHits; hit++){
          //Hit window start
          hMarksBack->at(tile)->push_back( new TLine( rpd[row][col]->hit_window.first[hit], rpd[row][col]->WF_histo->GetMinimum(), rpd[row][col]->hit_window.first[hit], rpd[row][col]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksBack->at(tile)->back())->SetLineColor(kRed);
          //Hit window end
          hMarksBack->at(tile)->push_back( new TLine( rpd[row][col]->hit_window.second[hit], rpd[row][col]->WF_histo->GetMinimum(), rpd[row][col]->hit_window.second[hit], rpd[row][col]->WF_histo->GetMaximum()) );
          ((TLine*)hMarksBack->at(tile)->back())->SetLineColor(kRed);
          //Place an X on the peak
          hMarksBack->at(tile)->push_back( new TMarker( rpd[row][col]->Peak_center[hit], rpd[row][col]->Peak_max[hit] + rpd[row][col]->PedMean, 5) );
          ((TMarker*)hMarksBack->at(tile)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksBack->at(tile)->back())->SetMarkerSize(3);
        }//end hit loop
      }//end if channel was hit
      tile++;
    }//end column loop
  }//end row loop

  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(502, m_alignment2021 );
  }

  m_viz->ManyPadsPlot( hWFvec, hDiffvec, 4, 4, "PF RPD", //Background histos, foreground histos, nCol, nRow, Detector label
                       Form("%s/results/plots/RPDwfDiffPlot_ev%d", std::getenv("JZCaPA"), m_counter ), // Output file name
                       "Time [bins]", "Amplitude [ADC]", "overlay", //X-axis label, Y-axis label, plot type
                       hMarksBack, hMarksFront, //Background plot markers, Foreground plot markers
                       -3300, -2550, -400, 900); //Background yMin, Background yMax, Foreground yMin, Foreground yMax

}

/** @brief Finalize method for RPDAnalysis2021
 *
 *
 */
void RPDAnalysis2021::Finalize( ){
    if(m_viz == NULL) m_viz = new Visualizer( "ATLAS" );

    TCanvas cCharge("charge","charge",800,600);
    TCanvas cPeak("peak","peak",800,600);
    TCanvas cDpeak("dpeak","dpeak",800,600);
    cCharge.Divide(4,4);
    cPeak.Divide(4,4);
    cDpeak.Divide(4,4);
    int pad;

    for(int row = 0; row < 4; row++){
        for(int col = 0; col < 4; col++){
            pad = row*4 + col + 1;

            cCharge.cd(pad);
            hChargeArr[row][col]->Draw();

            cPeak.cd(pad);
            hPeakArr[row][col]->Draw();

            cDpeak.cd(pad);
            hDPeakArr[row][col]->Draw();

            hCharge->SetBinContent(col+1, 4-row, hChargeArr[row][col]->GetMean() );// col,row then [row][col] CHECK ARIC
            hPeak->SetBinContent(  col+1, 4-row, hPeakArr[row][col]->GetMean()  );
        }
    }




    TMarker *marker = new TMarker(m_beamPosX,m_beamPosY,5);
    marker->SetMarkerColor(kRed);
    marker->SetMarkerSize(4);


    std::string out_dir =  m_viz->SetOutputDirectory();
    cCharge.Print(  (out_dir + "RPD_Charge_per_tile.png").c_str() );
    cPeak.Print(    (out_dir + "RPD_Peak_per_tile.png").c_str() );
    cDpeak.Print(   (out_dir + "RPD_Diff_peak_per_tile.png").c_str() );

    m_viz->DrawPlot(hChgVsPk,"RPD Q_{total}","RPD Peak_{sum}","RPD_TotalCharge.png","");
    m_viz->DrawPlot(hPkVsDiffPk,"RPD Peak_{sum}","#frac{#partial V}{#partial t}_{max}","RPD_TotalCharge.png","");
    m_viz->DrawPlot(hChargeSum,"RPD Q_{total}","Counts","RPD_TotalCharge.png","");
    m_viz->DrawPlot(hPhotons,"RPD Photon Count","Counts","RPD_nPhotons.png","");
    m_viz->DrawPlot(hPeakSum,"RPD Peak_{sum}","Counts","RPD_PeakSum.png","");
    m_viz->DrawPlot(hDiffPeakSum,"#frac{#partial V}{#partial t}_{max}","Counts","RPD_DiffSum.png","");

    m_viz->DrawPlot(hCharge,"Col","Row","RPD_Charge.png","COLZ text",marker);
    m_viz->DrawPlot(hPeak,"Col","Row","RPD_Peak.png","COLZ text",marker);
  //  m_viz->DrawPlot(hCenter,"x (mm)","y (mm)","RPD_CoM.png","COLZ",marker);




}
