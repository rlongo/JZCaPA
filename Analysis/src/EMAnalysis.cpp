/** @ingroup ana
 *  @file EMAnalysis.cpp
 *  @brief Implementation of EMAnalysis.
 *
 *
 *  Function definitions for EMAnalysis are provided.
 *  This class is the main class for the EM analysis.
 *  The analysis is done on both EMs simultaneously
 *
 *
 *  @author Aric Tate
 *  @bug No known bugs.
*/


#include "EMAnalysis.h"
#include "EM.h"
#include "Containers.h"
#include "TMarker.h"
#include "TH2D.h"
#include "TCanvas.h"


/** @brief Default Constructor for EMAnalysis.
 */
EMAnalysis::EMAnalysis( ){

}

/** @brief Destructor for EMAnalysis.
 */
EMAnalysis::~EMAnalysis( ){

}

/** @brief Initialization method for ZDCAnalysis
 *
 *  Takes a vector of detectors, picks out the EM
 *  and channels, then assigns them to member pointers
 *  Rows correspond to Z segments, Columns to X segments
 */
void EMAnalysis::Initialize( std::vector < Detector* > _vDet ){
  std::cout << "======================= Beginning of EMAnalysis =======================" << std::endl;
  m_counter = 0;

    for( auto& det : _vDet ){
        if(det->GetChannelsVector().size() == 0) continue;
        if(det->GetChannelsVector().at(0)->detector == "UEM" ){ //CHECK FOR 2021!
            m_EM = (EM*) det;
            m_alignment2021 = m_EM->GetAlignment2021();
            m_numRows = m_EM->GetnRows();
            m_numCols = m_EM->GetnCols();
            for(int row = 0; row < m_numRows; row++){
                for(int col = 0; col < m_numCols; col++){
                    em[row][col] = m_EM->GetElement(row,col);
                }
            }
        }
    }

    beamX = m_EM->GetBeamPos().x();
    beamY = m_EM->GetBeamPos().y();
}

/** @brief Historgam Setup method for EMAnalysis
 *
 *  Should instantiate any histograms you wish to output here.
 *
 *  @return none
 */
void EMAnalysis::SetupHistograms( ){

   hCharge     = new TH2D("EM Charge", "average charge per tile", 3, 0, 3, 3, 0, 3);
   hPeak       = new TH2D("EM Peak", "average peak height per tile", 3, 1, 4, 3, 1, 4);
   hCenter     = new TH2D("Shower Center", "Calculated center of mass", 200, -50, 50, 200, -50, 50);
   hChgVsPk    = new TH2D("EM Charge vs Peak height","Q vs Peak; Q; Peak", 300, 0, 300000, 300, 0, 1100);
   hPkVsDiffPk = new TH2D("EM Peak height vs Differential peak height","Peak vs Diff Peak", 200, 0, 5000, 200, 0, 300);
   hChargeSum  = new TH1D("EM Integrated Signal","EM Integrated Signal", 200, 0, 160000);
   hPeakSum    = new TH1D("EM Peak Height Sum","EM Peak Height Sum", 200, 0, 3000);
   hDiffPeakSum= new TH1D("EM Differential Peak Height Sum","EM Diff Peak Sum", 200, 0, 55000);

   hChargeArr.resize(m_numRows);
   hPeakArr.resize(m_numRows);
   hDPeakArr.resize(m_numRows);
   for(int row = 0; row < m_numRows; row++){
     hChargeArr[row].resize(m_numCols);
     hPeakArr[row].resize(m_numCols);
     hDPeakArr[row].resize(m_numCols);
       for(int col = 0; col < m_numCols; col++){
            hChargeArr[row][col] = new TH1D(Form("em%d_%d_Charge",row,col),Form("em%d_%d_Charge",row,col), 200 , 0, 15000);
            hChargeArr[row][col]->SetAxisRange(0,1500,"Y");

            hPeakArr[row][col]   = new TH1D(Form("em%d_%d_Peak",row,col),Form("em%d_%d_Peak",row,col), 200, 0, 300);
            hPeakArr[row][col]->SetAxisRange(0,1500,"Y");

            hDPeakArr[row][col]  = new TH1D(Form("em%d_%d_Diff_peak",row,col),Form("em%d_%d_Diff_peak",row,col), 200, 0, 5000);
            hDPeakArr[row][col]->SetAxisRange(0,1500,"Y");
       }
   }
}

/** @brief Branch setup method for ZDCAnalysis
 *
 *  Adds branches with data created by the analysis
 *
 */
void EMAnalysis::SetBranches( TTree* _tree ){
    m_AnalysisTree = _tree;

    m_AnalysisTree->Branch( "em_BeamX", &beamX, "em_BeamX/D");
    m_AnalysisTree->Branch( "em_BeamY", &beamY, "em_BeamY/D");
    for(int row = 0; row < m_numRows; row++){
        for(int col = 0; col < m_numCols; col++){
            m_AnalysisTree->Branch( Form("em%d_%d_Charge", row, col),           "std::vector<double>", &em[row][col]->Charge           );
            m_AnalysisTree->Branch( Form("em%d_%d_nPhotons", row, col),         "std::vector<float>",  &em[row][col]->nPhotons        );
            m_AnalysisTree->Branch( Form("em%d_%d_Peak_max", row, col),         "std::vector<double>", &em[row][col]->Peak_max         );
            m_AnalysisTree->Branch( Form("em%d_%d_Diff_max", row, col),         "std::vector<double>", &em[row][col]->Diff_max         );
            m_AnalysisTree->Branch( Form("em%d_%d_Peak_center", row, col),      "std::vector<int>",    &em[row][col]->Peak_center      );
            m_AnalysisTree->Branch( Form("em%d_%d_Diff_Peak_center", row, col), "std::vector<int>",    &em[row][col]->Diff_Peak_center );
        }
    }

    m_AnalysisTree->Branch("em_Charge_sum",   &ChargeSum,  "ChargeSum/D"   );
    m_AnalysisTree->Branch("em_Peak_sum",     &PeakSum,    "PeakSum/D"     );
    m_AnalysisTree->Branch("em_Diff_Peak_sum",&DiffPeakSum,"DiffPeakSum/D" );

}

/** @brief Analyze Events method for EMAnalysis
 *
 *
 */
void EMAnalysis::AnalyzeEvent( ){
    ChargeSum = 0;
    PeakSum = 0;
    DiffPeakSum = 0;

    //This loop takes the running average of charge and peak height per tile
    for(int row = 0; row < m_numRows; row++){
        for(int col = 0; col < m_numCols; col++){
            if( em[row][col]->is_on ){
              for(int hit = 0; hit < em[row][col]->nHits; hit++){
                  ChargeSum   += em[row][col]->Charge[hit];
                  PeakSum     += em[row][col]->Peak_max[hit];
                  DiffPeakSum += em[row][col]->Diff_max[hit];

                  hChargeArr[row][col]->Fill(em[row][col]->Charge[hit]);
                  hPeakArr[row][col]->Fill(em[row][col]->Peak_max[hit]);
                  hDPeakArr[row][col]->Fill(em[row][col]->Diff_max[hit]);
                }//end hits loop
            }//end if channel is on
        }//end column loop
    }//end row loop

    //This loop finds the center of mass per event (should probably be it's own function)

    double totalCharge = 0;
    double weightedRow = 0;
    double weightedCol = 0;
    for(int i = 0; i < m_numRows; i++){
        for(int j = 0; j < m_numCols; j++){
            if( em[i][j]->is_on && em[i][j]->was_hit ){
              for(int hit = 0; hit < em[i][j]->nHits; hit++){
                  totalCharge += em[i][j]->Charge[hit];
                  weightedRow += em[i][j]->Charge[hit] * zPos[i];
                  weightedCol += em[i][j]->Charge[hit] * xPos[j];
                }
            }
        }
    }

    hChargeSum->Fill(ChargeSum);
    hPeakSum->Fill(PeakSum);
    hDiffPeakSum->Fill(DiffPeakSum);

    // m_EM->PlotWF(m_counter);
    // m_EM->PlotWFandDerivative(m_counter, -3300, -2550, -400, 900);
    // m_EM->PlotWFandPWF(m_counter, -3300, -2550, -10, 700);

    m_counter++;
}

/** @brief Finalize method for EMAnalysis
 *
 *
 */
void EMAnalysis::Finalize( ){

    if(m_viz == NULL) m_viz = new Visualizer( "ATLAS" );

    TCanvas cCharge("charge","charge",800,600);
    TCanvas cPeak("peak","peak",800,600);
    TCanvas cDpeak("dpeak","dpeak",800,600);
    cCharge.Divide(m_numRows, m_numCols);
    cPeak.Divide(m_numRows, m_numCols);
    cDpeak.Divide(m_numRows, m_numCols);
    int pad;

    for(int row = 0; row < m_numRows; row++){
        for(int col = 0; col < m_numCols; col++){
            pad = row*m_numCols + col + 1; //CHECK FOR 2021!

            cCharge.cd(pad);
            hChargeArr[row][col]->Draw();

            cPeak.cd(pad);
            hPeakArr[row][col]->Draw();

            cDpeak.cd(pad);
            hDPeakArr[row][col]->Draw();

            hCharge->SetBinContent(col+1, m_numRows-row, hChargeArr[row][col]->GetMean() );//CHECK FOR 2021!
            hPeak->SetBinContent(  col+1, m_numRows-row, hPeakArr[row][col]->GetMean()  );//CHECK FOR 2021!
        }
    }


    std::string output =  std::getenv("JZCaPA");
    output += "/results/";

    TMarker *marker = new TMarker(m_beamPosX,m_beamPosY,5);
    marker->SetMarkerColor(kRed);
    marker->SetMarkerSize(4);

    //cCharge.Print( (output + "EM_Charge_per_tile.png").c_str() );
    //cPeak.Print( (output + "EM_Peak_per_tile.png").c_str() );
    //cDpeak.Print( (output + "EM_Diff_peak_per_tile.png").c_str() );

    m_viz->DrawPlot(hChgVsPk,"EM Q_{total}","EM Peak_{sum}","EM_TotalCharge.png","");
    m_viz->DrawPlot(hPkVsDiffPk,"EM Peak_{sum}","#frac{#partial V}{#partial t}_{max}","EM_TotalCharge.png","");
    m_viz->DrawPlot(hChargeSum,"EM Q_{total}","Counts","EM_TotalCharge.png","");
    m_viz->DrawPlot(hPeakSum,"EM Peak_{sum}","Counts","EM_PeakSum.png","");
    m_viz->DrawPlot(hDiffPeakSum,"#frac{#partial V}{#partial t}_{max}","Counts","EM_DiffSum.png","");
    m_viz->DrawPlot(hCharge,"Col","Row","EM_Charge.png","COLZ text",marker);
    m_viz->DrawPlot(hPeak,"Col","Row","EM_Peak.png","COLZ text",marker);
    m_viz->DrawPlot(hCenter,"x (mm)","y (mm)","EM_CoM.png","COLZ",marker);

}
