/** @ingroup ana
 *  @file ZDCAnalysis2021.cpp
 *  @brief Implementation of ZDCAnalysis2021.
 *
 *
 *  Function definitions for ZDCAnalysis2021 are provided.
 *  This class is the main class for the ZDC analysis in 2021 test beam.
 *  The analysis is done on all the three HAD ZDCs simultaneously
 *  Please note that the EM modules (prototype and CMS) have a dedicated class and a dedicated analysis
 *
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
*/


#include "ZDCAnalysis2021.h"
#include "ZDC.h"
#include "Containers.h"
#include "Visualizer.h"

#include "TLine.h"
#include "TMarker.h"
#include "TSystem.h"
#include "TH1.h"
#include "TH2D.h"

/** @brief Default Constructor for ZDCAnalysis2021.
 */
ZDCAnalysis2021::ZDCAnalysis2021( ){

}

/** @brief Destructor for ZDCAnalysis2021.
 */
ZDCAnalysis2021::~ZDCAnalysis2021( ){

}

/** @brief Initialization method for ZDCAnalysis2021
 *
 *  Can add other things here that you would
 *  perhaps not put into the constructor.
 *  I.e. a TTree, some tools. Etc.
 *
 */
void ZDCAnalysis2021::Initialize( ){

}

/** @brief Initialization method for ZDCAnalysis2021
 *
 *  Takes a vector of detectors, picks out the ZDCs
 *  and assigns them to member pointers
 *
 */
void ZDCAnalysis2021::Initialize( std::vector < Detector* > _vDet ){
  m_counter = 0;
    for( auto& det : _vDet ){
        if( det->GetChannelsVector().at(0)->detector == "HAD" && det->GetChannelsVector()[0]->mapping_column == 1){
            m_zdc1 = (ZDC*)det;
            zdc1 = det->GetElement(0,1);
        }
        if( det->GetChannelsVector().at(0)->detector == "HAD" && det->GetChannelsVector()[0]->mapping_column == 2){
            m_zdc2 = (ZDC*)det;
            zdc2 = det->GetElement(0,2);
        }
        if( det->GetChannelsVector().at(0)->detector == "HAD" && det->GetChannelsVector()[0]->mapping_column == 3){
            m_zdc3 = (ZDC*)det;
            zdc3 = det->GetElement(0,3);
        }
    }
    vZDC.push_back(zdc1);
    vZDC.push_back(zdc2);
    vZDC.push_back(zdc3);

    hMarksFront = new std::vector< std::vector< TObject* >* >;
    hMarksBack = new std::vector< std::vector< TObject* >* >;
    for( Channel* mod: vZDC){
      hWFvec.push_back( mod->WF_histo );
      hPWFvec.push_back( mod->PWF_histo );
      hDiffvec.push_back( mod->FirstDerivative );
      hMarksBack->push_back( new std::vector< TObject* > );
      hMarksFront->push_back( new std::vector< TObject* > );
    }

    if(m_zdc1 != 0){
      beamX = m_zdc1->GetBeamPos().x();
      beamY = m_zdc1->GetBeamPos().y();
    }else if(m_zdc2 != 0){
      beamX = m_zdc2->GetBeamPos().x();
      beamY = m_zdc2->GetBeamPos().y();
    }else if(m_zdc2 != 0){
      beamX = m_zdc2->GetBeamPos().x();
      beamY = m_zdc2->GetBeamPos().y();
    }

    m_alignment2021 = m_zdc1->GetAlignment2021();

}


/** @brief Historgam Setup method for ZDCAnalysis2021
 *
 *  Should instantiate any histograms you wish to output here.
 *
 *  @return none
 */
void ZDCAnalysis2021::SetupHistograms( ){
    //TH1D
    hChargeRatio1Tot = new TH1D("ChargeZDC1_over_ChargeZDCTot","Q_{ZDC1}/Q_{ZDC1+2+3}",100,0,1);
    //hChargeRatio1Tot->SetCanExtend(TH1::kXaxis);
    hChargeRatio2Tot = new TH1D("ChargeZDC2_over_ChargeZDCTot","Q_{ZDC2}/Q_{ZDC1+2+3}",100,0,1);
    //hChargeRatio2Tot->SetCanExtend(TH1::kXaxis);
    hChargeRatio3Tot = new TH1D("ChargeZDC3_over_ChargeZDCTot","Q_{ZDC3}/Q_{ZDC1+2+3}",100,0,1);
    //hChargeRatio3Tot->SetCanExtend(TH1::kXaxis);

    hChargeSum = new TH1D("Charge_sum","Q_{total} (pC)",300,0,450);

    hPeakRatio1 = new TH1D("PeakZDC1_over_AvgZDCPeak","Peak_{ZDC1}/#mu_{Peak}",100,0.5,1.5);
    //hPeakRatio2to1->SetCanExtend(TH1::kXaxis);
    hPeakRatio2 = new TH1D("PeakZDC2_over_AvgZDCPeak","Peak_{ZDC2}/#mu_{Peak}",100,0.5,1.5);
    //hPeakRatio3to1->SetCanExtend(TH1::kXaxis);
    hPeakRatio3 = new TH1D("PeakZDC3_over_AvgZDCPeak","Peak_{ZDC3}/#mu_{Peak}",100,0.5,1.5);
    //hPeakRatio3to2->SetCanExtend(TH1::kXaxis);

    hCharge1 = new TH1D("ZDC1_Charge","Q_{ZDC1} (pC)",200,0,200);
    hCharge2 = new TH1D("ZDC2_Charge","Q_{ZDC2} (pC)",200,0,200);
    hCharge3 = new TH1D("ZDC3_Charge","Q_{ZDC3} (pC)",200,0,200);

    hPeak1   = new TH1D("ZDC1_Peak","Peak_{ZDC1}",200,0,1000);
    hPeak2   = new TH1D("ZDC2_Peak","Peak_{ZDC2}",200,0,1000);
    hPeak3   = new TH1D("ZDC3_Peak","Peak_{ZDC3}",200,0,1000);

    hDpeak1  = new TH1D("ZDC1_Diff_Peak","#frac{#partial V}{#partial t}_{max ZDC1}",200,0,4500);
    hDpeak2  = new TH1D("ZDC2_Diff_Peak","#frac{#partial V}{#partial t}_{max ZDC2}",200,0,4500);
    hDpeak3  = new TH1D("ZDC3_Diff_Peak","#frac{#partial V}{#partial t}_{max ZDC3}",200,0,4500);

    hArrival1  = new TH1D("ZDC1_Arrival_time","Peak Center_{ZDC1} (ns)",150,240,390);
    hArrival2  = new TH1D("ZDC2_Arrival_time","Peak Center_{ZDC2} (ns)",150,240,390);
    hArrival3  = new TH1D("ZDC3_Arrival_time","Peak Center_{ZDC3} (ns)",150,240,390);

    hToF  = new TH1D("Time_of_Flight","Peak_Center_{ZDC3-ZDC1} (ns)",30,-15,15);

    //TH2D
    hCharge = new TH2D("ZDC_Charge_Correlation", "ZDC Charge Correlation", 100, 0, 450, 100, 0, 450);
    //hCharge->SetCanExtend(TH1::kAllAxes);

    hPeak   = new TH2D("ZDC_Peak_Correlation", "ZDC Peak Correlation", 100, 0, 1000, 100, 0, 1000);
    //hPeak->SetCanExtend(TH1::kAllAxes);


    hChargePeakZDC1 = new TH2D("ZDC1_ChargePeakCorrelation","Q_{ZDC1} (pC) vs Peak_{ZDC1}",50,0,200,50,0,1000);
    //hChargePeakZDC1->SetCanExtend(TH1::kAllAxes);
    hChargePeakZDC2 = new TH2D("ZDC2_ChargePeakCorrelation","Q_{ZDC2} (pC) vs Peak_{ZDC2}",50,0,200,50,0,1000);
    //hChargePeakZDC2->SetCanExtend(TH1::kAllAxes);
    hChargePeakZDC3 = new TH2D("ZDC3_ChargePeakCorrelation","Q_{ZDC3} (pC) vs Peak_{ZDC3}",50,0,200,50,0,1000);
    //hChargePeakZDC3->SetCanExtend(TH1::kAllAxes);

}

/** @brief Branch setup method for ZDCAnalysis2021
 *
 *  Adds branches with data created by the analysis
 *
 */
void ZDCAnalysis2021::SetBranches( TTree* _tree ){
    m_AnalysisTree = _tree;
    m_AnalysisTree->Branch( "zdc_BeamX", &beamX, "zdc_BeamX/D");
    m_AnalysisTree->Branch( "zdc_BeamY", &beamY, "zdc_BeamY/D");
    for(int mod = 0; mod < vZDC.size(); mod++){
        m_AnalysisTree->Branch( Form("zdc%d_Charge",mod+1),           "std::vector<double>", &vZDC[mod]->Charge             );
        m_AnalysisTree->Branch( Form("zdc%d_Peak_max",mod+1),         "std::vector<double>", &vZDC[mod]->Peak_max           );
        m_AnalysisTree->Branch( Form("zdc%d_Diff_max",mod+1),         "std::vector<double>", &vZDC[mod]->Diff_max           );
        m_AnalysisTree->Branch( Form("zdc%d_Peak_center",mod+1),      "std::vector<int>",    &vZDC[mod]->Peak_center        );
        m_AnalysisTree->Branch( Form("zdc%d_Peak_time",mod+1),        "std::vector<double>", &vZDC[mod]->Peak_time          );
        m_AnalysisTree->Branch( Form("zdc%d_Diff_Peak_center",mod+1), "std::vector<int>",    &vZDC[mod]->Diff_Peak_center   );
        m_AnalysisTree->Branch( Form("zdc%d_Diff_Peak_time",mod+1),   "std::vector<double>", &vZDC[mod]->Diff_Peak_time     );
        m_AnalysisTree->Branch( Form("zdc%d_Ped_mean",mod+1),         &vZDC[mod]->PedMean,   Form("zdc%d_Ped_mean/D",mod+1) );
    }
}

/** @brief Analyze Events method for ZDCAnalysis2021
 *
 *
 */
void ZDCAnalysis2021::AnalyzeEvent( ){

    //Find the hit for each module in the range we care about
    int zdcHit[] = {-1,-1,-1};
    int start = 200, end = 350;
    for(int mod = 0; mod < vZDC.size(); mod++){
        for(int hit = 0; hit < vZDC[mod]->nHits; hit++){
            if( vZDC[mod]->Peak_center[hit] > start && vZDC[mod]->Peak_center[hit] < end){
                zdcHit[mod] = hit;
            }
        }
        //If the hits aren't in the right range, consider it not to be hit
        if( zdcHit[mod] == -1 ) vZDC[mod]->was_hit = false;
    }
    if( zdc1->was_hit         && zdc2->was_hit        && zdc3->was_hit){

        Double_t totalCharge = zdc1->Charge[zdcHit[0]] + zdc2->Charge[zdcHit[1]] + zdc3->Charge[zdcHit[2]];
        hChargeRatio1Tot->Fill(zdc1->Charge[zdcHit[0]]/totalCharge);
        hChargeRatio2Tot->Fill(zdc2->Charge[zdcHit[1]]/totalCharge);
        hChargeRatio3Tot->Fill(zdc3->Charge[zdcHit[2]]/totalCharge);

        Double_t totalPeak = zdc1->Peak_max[zdcHit[0]] + zdc2->Peak_max[zdcHit[1]] + zdc3->Peak_max[zdcHit[2]];
        hPeakRatio1->Fill( zdc1->Peak_max[zdcHit[0]]/(totalPeak/3) );
        hPeakRatio2->Fill( zdc2->Peak_max[zdcHit[1]]/(totalPeak/3) );
        hPeakRatio3->Fill( zdc3->Peak_max[zdcHit[2]]/(totalPeak/3) );

        hCharge->Fill( zdc1->Charge[zdcHit[0]],   zdc2->Charge[zdcHit[1]] );//OUTDATED
        hPeak->Fill(   zdc1->Peak_max[zdcHit[0]], zdc2->Peak_max[zdcHit[1]] );//OUTDATED

        hChargePeakZDC1->Fill(zdc1->Charge[zdcHit[0]],zdc1->Peak_max[zdcHit[0]]);
        hChargePeakZDC2->Fill(zdc2->Charge[zdcHit[1]],zdc2->Peak_max[zdcHit[1]]);
        hChargePeakZDC2->Fill(zdc3->Charge[zdcHit[2]],zdc3->Peak_max[zdcHit[2]]);

        hChargeSum->Fill(totalCharge);

        hToF->Fill(zdc3->Diff_Peak_time[zdcHit[2]] - zdc1->Diff_Peak_time[zdcHit[0]]);
    }

    //If HAD1 was hit
    if( zdc1->was_hit){
        hCharge1->Fill(zdc1->Charge[zdcHit[0]]);
        hPeak1->Fill(zdc1->Peak_max[zdcHit[0]]);
        hDpeak1->Fill(zdc1->Diff_max[zdcHit[0]]);
        hArrival1->Fill(zdc1->Diff_Peak_time[zdcHit[0]]);
    }
    //If HAD2 was hit
    if( zdc2->was_hit ){
        hCharge2->Fill(zdc2->Charge[zdcHit[1]]);
        hPeak2->Fill(zdc2->Peak_max[zdcHit[1]]);
        hDpeak2->Fill(zdc2->Diff_max[zdcHit[1]]);
        hArrival2->Fill(zdc2->Diff_Peak_time[zdcHit[1]]);
    }
    //If HAD3 was hit
    if( zdc3->was_hit ){
        hCharge3->Fill(zdc3->Charge[zdcHit[2]]);
        hPeak3->Fill(zdc3->Peak_max[zdcHit[2]]);
        hDpeak3->Fill(zdc3->Diff_max[zdcHit[2]]);
        hArrival3->Fill(zdc3->Diff_Peak_time[zdcHit[2]]);
    }


    //To draw low level plots of the waveforms primarily for validation of the WFAnalysis
    // PlotWF( m_counter );
    // PlotWFandPWF( m_counter );
    // PlotWFandDerivative( m_counter );

    m_counter++;

}//end AnalyzeEvent()


/** @brief Draw the waveforms for all ZDCs for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void ZDCAnalysis2021::PlotWF(int eventNum){
  if(m_counter == 0){//If this is the first event create an output directory
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  for(int mod = 0; mod < vZDC.size(); mod++){

    //Delete and clear old markers
    for(int i = 0; i < hMarksFront->at(mod)->size(); i++){
      if( !hMarksFront->at(mod)->at(i)->InheritsFrom("TF1") ){
        delete hMarksFront->at(mod)->at(i);
      }
    }
    hMarksFront->at(mod)->clear();


    //Draw the pedestal no matter what
    hMarksFront->at(mod)->push_back( new TLine( 0, vZDC[mod]->PedMean, 1024, vZDC[mod]->PedMean) );
    ((TLine*)hMarksFront->at(mod)->back())->SetLineColor(kGreen);

    //Draw hit window lines, peak marker, and set the range of the fit function to the full hit window
    if(vZDC[mod]->is_on){
      vZDC[mod]->WF_histo->GetYaxis()->SetRange(0,0);
      vZDC[mod]->WF_histo->ResetStats();
      if(vZDC[mod]->was_hit){
        for(int hit = 0; hit < vZDC[mod]->nHits; hit++){
          //Draw vertical red lines around the hit window
          hMarksFront->at(mod)->push_back( new TLine( vZDC[mod]->hit_window.first[hit], -2950, vZDC[mod]->hit_window.first[hit], -2600) );
          ((TLine*)hMarksFront->at(mod)->back())->SetLineColor(kRed);
          hMarksFront->at(mod)->push_back( new TLine( vZDC[mod]->hit_window.second[hit], -2950, vZDC[mod]->hit_window.second[hit], -2600) );
          ((TLine*)hMarksFront->at(mod)->back())->SetLineColor(kRed);
          hMarksFront->at(mod)->push_back( new TMarker( vZDC[mod]->Peak_center[hit], vZDC[mod]->Peak_max[hit] + vZDC[mod]->PedMean, 5) );
          ((TMarker*)hMarksFront->at(mod)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksFront->at(mod)->back())->SetMarkerSize(3);
        }//end hit loop
      }//end if channel was hit
    }//end if channel is on
  }//end module loop
  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(502, m_alignment2021 );
  }
  m_viz->SimplePadsPlot( hWFvec, 3, 1, "Time [bins]", "Amplitude [ADC]", //Background histos, foreground histos, nCol, nRow, Detector label
                         Form("ZDCwf%d.png", m_counter), "ZDC HAD", "", // Output file name, Module name, plot type
                         Form("%s/results/plots/", std::getenv("JZCaPA") ), // Output directry
                         true, hMarksFront ); // Autoscale, Plot markers
}

/** @brief Draw the waveform and processed waveforms for all ZDCs for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void ZDCAnalysis2021::PlotWFandPWF(int eventNum){
  if(eventNum == 0){//If this is the first event create an output directory
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );
  }

  for(int mod = 0; mod < vZDC.size(); mod++){
    //Delete and clear old markers
    for(int i = 0; i < hMarksBack->at(mod)->size(); i++){
      delete hMarksBack->at(mod)->at(i);
    }
    hMarksBack->at(mod)->clear();
    for(int i = 0; i < hMarksFront->at(mod)->size(); i++){
      if( !hMarksFront->at(mod)->at(i)->InheritsFrom("TF1") ){
        delete hMarksFront->at(mod)->at(i);
      }
    }
    hMarksFront->at(mod)->clear();


    //Draw the pedestal no matter what
    hMarksBack->at(mod)->push_back( new TLine( 0, vZDC[mod]->PedMean, 1024, vZDC[mod]->PedMean) );
    ((TLine*)hMarksBack->at(mod)->back())->SetLineColor(kGreen);

    //Draw hit window lines, peak marker, and set the range of the fit function to the full hit window
    if(vZDC[mod]->is_on){
      vZDC[mod]->WF_histo->GetYaxis()->SetRange(0,0);
      vZDC[mod]->WF_histo->ResetStats();
      if(vZDC[mod]->was_hit){
        for(int hit = 0; hit < vZDC[mod]->nHits; hit++){
          //Draw vertical red lines around the hit window
          hMarksBack->at(mod)->push_back( new TLine( vZDC[mod]->hit_window.first[hit], -2950, vZDC[mod]->hit_window.first[hit], -2600) );
          ((TLine*)hMarksBack->at(mod)->back())->SetLineColor(kRed);
          hMarksBack->at(mod)->push_back( new TLine( vZDC[mod]->hit_window.second[hit], -2950, vZDC[mod]->hit_window.second[hit], -2600) );
          ((TLine*)hMarksBack->at(mod)->back())->SetLineColor(kRed);
          hMarksBack->at(mod)->push_back( new TMarker( vZDC[mod]->Peak_center[hit], vZDC[mod]->Peak_max[hit] + vZDC[mod]->PedMean, 5) );
          ((TMarker*)hMarksBack->at(mod)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksBack->at(mod)->back())->SetMarkerSize(3);
          // hMarksFront->at(mod)->push_back( vZDC[mod]->FitFunc );
          // ((TF1*)hMarksFront->at(mod)->back())->SetRange(vZDC[mod]->hit_window.first, vZDC[mod]->hit_window.second);
          // ((TF1*)hMarksFront->at(mod)->back())->SetLineColor(kCyan);
        }//end hit loop
      }//end if channel was hit
    }//end if channel is on
  }//end module loop
  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(502, m_alignment2021 );
  }
  m_viz->ManyPadsPlot( hWFvec, hPWFvec, 3, 1, "HAD ", //Background histos, foreground histos, nCol, nRow, Detector label
                       Form("%s/results/plots/ZDCwfPwfPlot_ev%d",std::getenv("JZCaPA"), eventNum ),  // Output file name
                       "Time [bins]", "Amplitude [ADC]", "overlay", //X-axis label, Y-axis label, plot type
                       hMarksBack, hMarksFront, //Background plot markers, Foreground plot markers
                       -3600, -2550, -10, 700); //Background yMin, Background yMax, Foreground yMin, Foreground yMax

}


/** @brief Draw the waveform and waveform derivative for all ZDCs for a given event
 *  WARNING: This is cpu intensive and intended for low level inspection of a few events at a time
 *
 *  @param - Event number for output file naming
 */
void ZDCAnalysis2021::PlotWFandDerivative(int eventNum){
  if(m_counter == 0){//If this is the first event create an output directory
    //
    gSystem->Exec( Form("mkdir -p %s/results/plots",std::getenv("JZCaPA") ) );

  }

  for(int mod = 0; mod < vZDC.size(); mod++){

    //Delete and clear old markers
    for(int i = 0; i < hMarksBack->at(mod)->size(); i++){
      delete hMarksBack->at(mod)->at(i);
    }
    hMarksBack->at(mod)->clear();
    for(int i = 0; i < hMarksFront->at(mod)->size(); i++){
      if( !hMarksFront->at(mod)->at(i)->InheritsFrom("TF1") ){ // Don't delete the fit functions
        delete hMarksFront->at(mod)->at(i);
      }
    }
    hMarksFront->at(mod)->clear();


    //Draw the pedestal no matter what
    hMarksBack->at(mod)->push_back( new TLine( 0, vZDC[mod]->PedMean, 1024, vZDC[mod]->PedMean) );
    ((TLine*)hMarksBack->at(mod)->back())->SetLineColor(kGreen);

    //Plot the threshold lines on the derivative no matter what
    hMarksFront->at(mod)->push_back( new TLine( 0, vZDC[mod]->FirstDerivativeRMS*vZDC[mod]->Threshold, 1024, vZDC[mod]->FirstDerivativeRMS*vZDC[mod]->Threshold) );
    ((TLine*)hMarksFront->at(mod)->back())->SetLineColor(kMagenta);
    hMarksFront->at(mod)->push_back( new TLine( 0, -1*vZDC[mod]->FirstDerivativeRMS*vZDC[mod]->Threshold, 1024, -1*vZDC[mod]->FirstDerivativeRMS*vZDC[mod]->Threshold) );
    ((TLine*)hMarksFront->at(mod)->back())->SetLineColor(kMagenta);

    //Draw hit window lines, peak marker, and set the range of the fit function to the full hit window
    if(vZDC[mod]->is_on){
      vZDC[mod]->WF_histo->GetYaxis()->SetRange(0,0);
      vZDC[mod]->WF_histo->ResetStats();

      if(vZDC[mod]->was_hit){
        for(int hit = 0; hit < vZDC[mod]->nHits; hit++){
          //Draw vertical red lines around the hit window
          hMarksBack->at(mod)->push_back( new TLine( vZDC[mod]->hit_window.first[hit], -2950, vZDC[mod]->hit_window.first[hit], -2600) );
          ((TLine*)hMarksBack->at(mod)->back())->SetLineColor(kRed);
          hMarksBack->at(mod)->push_back( new TLine( vZDC[mod]->hit_window.second[hit], -2950, vZDC[mod]->hit_window.second[hit], -2600) );
          ((TLine*)hMarksBack->at(mod)->back())->SetLineColor(kRed);

          //Draw an X on the peak
          hMarksBack->at(mod)->push_back( new TMarker( vZDC[mod]->Peak_center[hit], vZDC[mod]->Peak_max[hit] + vZDC[mod]->PedMean, 5) );
          ((TMarker*)hMarksBack->at(mod)->back())->SetMarkerColor(kMagenta+2);
          ((TMarker*)hMarksBack->at(mod)->back())->SetMarkerSize(3);
        }
      }//end if channel was hit
    }//end if channel is on
  }//end module loop
  if(m_viz == NULL){
    m_viz = new Visualizer( "ATLAS" );
    m_viz->SetTestBeamLabel(502, m_alignment2021 );
  }
  m_viz->ManyPadsPlot( hWFvec, hDiffvec, 3, 1, "HAD ", //Background histos, foreground histos, nCol, nRow, Detector label
                       Form("%s/results/plots/ZDCwfDiff_ev%d", std::getenv("JZCaPA"), m_counter ), // Output file name
                       "Time [bins]", "Amplitude [ADC]", "overlay", //X-axis label, Y-axis label, plot type
                       hMarksBack, hMarksFront, //Background plot markers, Foreground plot markers
                       -3600, -2550, -6000, 16000); //Background yMin, Background yMax, Foreground yMin, Foreground yMax
}//end PlotWFandDerivative

/** @brief Finalize method for ZDCAnalysis2021
 *
 *
 */
void ZDCAnalysis2021::Finalize( ){

    std::string output =  std::getenv("JZCaPA");
    output += "/results/";

    if(m_viz == NULL) m_viz = new Visualizer( "ATLAS" );


    std::vector<TH1*> hist1D_vec;
    std::vector<TH1*> hist2D_vec;

    hist1D_vec.push_back(hCharge1); hist1D_vec.push_back(hCharge2); hist1D_vec.push_back(hCharge3);
    m_viz->SimplePadsPlot( hist1D_vec, 3, 1, "Q_{ZDC} (pC)","Counts", "HAD[1-3]_Charge1D.png", "HAD", ""); hist1D_vec.clear();

    hist1D_vec.push_back(hPeak1); hist1D_vec.push_back(hPeak2); hist1D_vec.push_back(hPeak3);
    m_viz->SimplePadsPlot( hist1D_vec, 3, 1, "Peak_{max ZDC} (mV)" ,"Counts", "HAD[1-3]_Peak1D.png", "HAD", ""); hist1D_vec.clear();

    hist1D_vec.push_back(hDpeak1); hist1D_vec.push_back(hDpeak2); hist1D_vec.push_back(hDpeak3);
    m_viz->SimplePadsPlot( hist1D_vec, 3, 1, "#frac{#partial V}{#partial t}_{max ZDC}" ,"Counts", "HAD[1-3]_DiffPeak1D.png", "HAD", ""); hist1D_vec.clear();

    hist1D_vec.push_back(hArrival1); hist1D_vec.push_back(hArrival2); hist1D_vec.push_back(hArrival3);
    m_viz->SimplePadsPlot( hist1D_vec, 3, 1, "Arrival time_{ZDC} (ns)" ,"Counts", "HAD[1-3]_Arrival1D.png", "HAD", ""); hist1D_vec.clear();

    hist1D_vec.push_back(hChargeRatio1Tot); hist1D_vec.push_back(hChargeRatio2Tot); hist1D_vec.push_back(hChargeRatio3Tot);
    m_viz->SimplePadsPlot( hist1D_vec, 3, 1, "Q_{ZDC}/Q_{ZDC1+2+3}" ,"Counts", "HAD[1-3]_chargeRatioTot1D.png", "HAD", ""); hist1D_vec.clear();

    hist1D_vec.push_back(hPeakRatio1); hist1D_vec.push_back(hPeakRatio2); hist1D_vec.push_back(hPeakRatio3);
    m_viz->SimplePadsPlot( hist1D_vec, 3, 1, "Peak_{ZDC}/#mu_{Peak}" ,"Counts", "HAD[1-3]_peakRatio1D.png", "HAD", ""); hist1D_vec.clear();

    hist2D_vec.push_back(hChargePeakZDC1); hist2D_vec.push_back(hChargePeakZDC2); hist2D_vec.push_back(hChargePeakZDC3);
    m_viz->SimplePadsPlot( hist2D_vec, 3, 1, "Q_{ZDC} (pC)" ,"Peak_{ZDC} (mV)", "HAD[1-3]_ChargePeak2D.png", "HAD", "COLZ"); hist2D_vec.clear();

    m_viz->DrawPlot(hCharge,"Q_{ZDC1} (pC)","Q_{ZDC2} (pC)","ZDC_charge.png","COLZ");//OUTDATED
    m_viz->DrawPlot(hPeak,"Peak_{ZDC1} (mV)","Peak_{ZDC2} (mV)","ZDC_peak.png","COLZ");//OUTDATED
    m_viz->DrawPlot(hToF,"Time of Flight (ns)","Counts","ZDC_ToF.png","");
    m_viz->DrawPlot(hChargeSum,"Q_{total} (pC)","Counts","ZDC_Qtot.png","");


}
