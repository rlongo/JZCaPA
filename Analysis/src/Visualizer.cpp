/** @ingroup ana
 *  @file Visualizer.cpp
 *  @brief Implementation of Visualizer class.
 *
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#include <TH1.h>
#include <TH2.h>
#include <TROOT.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TClass.h>

#include <iostream>

#include "Visualizer.h"
#include "Containers.h"

/** @brief Visualizer Constructor for Visualizer.
 */
Visualizer::Visualizer( ){

}

Visualizer::Visualizer( std::string _style ){
    m_style = _style;
    if(m_style == "ATLAS" || m_style == "atlas") Visualizer::SetAtlasStyle();
}

/**
 * @brief Function inherited from rcdaq. Creates the ATLAS TStyle object
 * @return
 */
TStyle* Visualizer::AtlasStyle(){
    TStyle *atlasStyle = new TStyle("ATLAS","Atlas style");

      // use plain black on white colors
      Int_t icol=0; // WHITE
      atlasStyle->SetFrameBorderMode(icol);
      atlasStyle->SetFrameFillColor(icol);
      atlasStyle->SetCanvasBorderMode(icol);
      atlasStyle->SetCanvasColor(icol);
      atlasStyle->SetPadBorderMode(icol);
      atlasStyle->SetPadColor(icol);
      atlasStyle->SetStatColor(icol);
      //atlasStyle->SetFillColor(icol); // don't use: white fill color for *all* objects

      // set the paper & margin sizes
      atlasStyle->SetPaperSize(20,26);

      // set margin sizes
      atlasStyle->SetPadTopMargin(0.05);
      atlasStyle->SetPadRightMargin(0.05);
      atlasStyle->SetPadBottomMargin(0.16);
      atlasStyle->SetPadLeftMargin(0.16);

      // set title offsets (for axis label)
      atlasStyle->SetTitleXOffset(1.4);
      atlasStyle->SetTitleYOffset(1.4);

      // use large fonts
      //Int_t font=72; // Helvetica italics
      Int_t font=42; // Helvetica
      Double_t tsize=0.05;
      atlasStyle->SetTextFont(font);

      atlasStyle->SetTextSize(tsize);
      atlasStyle->SetLabelFont(font,"x");
      atlasStyle->SetTitleFont(font,"x");
      atlasStyle->SetLabelFont(font,"y");
      atlasStyle->SetTitleFont(font,"y");
      atlasStyle->SetLabelFont(font,"z");
      atlasStyle->SetTitleFont(font,"z");

      atlasStyle->SetLabelSize(tsize,"x");
      atlasStyle->SetTitleSize(tsize,"x");
      atlasStyle->SetLabelSize(tsize,"y");
      atlasStyle->SetTitleSize(tsize,"y");
      atlasStyle->SetLabelSize(tsize,"z");
      atlasStyle->SetTitleSize(tsize,"z");

      // use bold lines and markers
      atlasStyle->SetMarkerStyle(20);
      atlasStyle->SetMarkerSize(1.2);
      atlasStyle->SetHistLineWidth(2.);
      atlasStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

      // get rid of X error bars
      //atlasStyle->SetErrorX(0.001);
      // get rid of error bar caps
      atlasStyle->SetEndErrorSize(0.);

      // do not display any of the standard histogram decorations
      atlasStyle->SetOptTitle(0);
      //atlasStyle->SetOptStat(1111);
      atlasStyle->SetOptStat(0);
      //atlasStyle->SetOptFit(1111);
      atlasStyle->SetOptFit(0);

      // put tick marks on top and RHS of plots
      atlasStyle->SetPadTickX(1);
      atlasStyle->SetPadTickY(1);

      return atlasStyle;

}

/**
 * @brief Function inherited from rcdaq. Set the ATLAS style.
 */
void Visualizer::SetAtlasStyle(){

    static TStyle* atlasStyle = 0;
    std::cout << "\nApplying ATLAS style settings...\n" << std::endl ;
    if ( atlasStyle==0 ) atlasStyle = AtlasStyle();
    gROOT->SetStyle("ATLAS");
    gROOT->ForceStyle();

}


/** @brief OverlayHistos method for Visualizer
 *
 *  Plots two input histograms on the same, specified pad with
 *  separate axis. Draws markers (TMarker, TLine...) if given a vector of them
 *
 *  @param h1 - Base histogram (left y-axis)
 *  @param h2 - Overlayed histogram (right y-axis)
 *  @param pad - Address of a pad to be drawn on
 *  @param _markersFirst - Vector of TObject* which holds TMarkers or TLines to be drawn on the bachground plot.
 *  @param _markersSecond - Vector of TObject* which holds TMarkers or TLines to be drawn on the foreground plots.
 *  @param _ymin1 - Y-axis minimum value of the base histogram
 *  @param _ymax1 - Y-axis maximum value of the base histogram
 *  @param _ymin2 - Y-axis minimum value of the overlaid histogram
 *  @param _ymax2 - Y-axis maximum value of the overlaid histogram
 */
void Visualizer::OverlayHistos( TH1 *h1, TH1 *h2 , TVirtualPad* pad,
                                std::vector< TObject* >* _markersFirst,
                                std::vector< TObject* >* _markersSecond,
                                double _ymin1, double _ymax1,
                                double _ymin2, double _ymax2 ){

    // If there is no pad or no data in the histograms, return
    if( pad == nullptr ) {std::cerr<< "WARNING: No pad to overlay histos onto" << std::endl; return;}
    if( !h1->GetMinimum() && !h1->GetMaximum()) {std::cerr << "WARNING: "<< h1->GetTitle() << " is empty. Can't overlay" << std::endl; return;}

    //Remove Stat box and double the y-axis range to include negative values
    gStyle->SetOptStat( kFALSE );
    h1->Draw();
    if( _ymin1 != 0 || _ymax1 != 0){
      h1->GetYaxis()->SetRangeUser( _ymin1, _ymax1);
    }
    pad->Update();
    if( _markersFirst != NULL ){
      for(int i = 0; i < _markersFirst->size(); i++){
        if(_markersFirst->at(i)->InheritsFrom("TF1")){
          _markersFirst->at(i)->Draw("same");
        }else{
          _markersFirst->at(i)->Draw("same");
        }
      }
    }

    pad->cd();
    TPad *overlay = new TPad("overlay","",0,0,1,1);
    overlay->SetFillStyle(4000);
    overlay->SetFillColor(4000);
    overlay->SetFrameFillStyle(4000);
    overlay->Draw();
    overlay->cd();

    if( _ymin2 != 0 || _ymax2 != 0){
      h2->GetYaxis()->SetRangeUser( _ymin2, _ymax2 );
    }
    Double_t xmin = pad->GetUxmin();
    Double_t ymin = h2->GetMinimum();
    Double_t xmax = pad->GetUxmax();
    Double_t ymax = h2->GetMaximum();
    TH1F *hframe = overlay->DrawFrame(xmin,ymin,xmax,ymax);
    hframe->GetXaxis()->SetLabelOffset(99);
    hframe->GetYaxis()->SetLabelOffset(99);
    h2->SetLineColor( kRed );
    h2->Draw("Y+");

    if( _markersSecond != NULL ){
      for(int i = 0; i < _markersSecond->size(); i++){
        if(_markersSecond->at(i)->InheritsFrom("TF1")){
          _markersSecond->at(i)->Draw("same");
        }else{
          _markersSecond->at(i)->Draw("same");
        }
      }
    }

}


/** @brief OverlayHistos method for Visualizer
 *
 *  Plots two input histograms on the same, specified pad with
 *  separate axis. Draws two horizontal lines at +_line and -_line
 *  on h2's axis if f _line is non-zero.
 *
 *  @param h1 - Base histogram (left y-axis)
 *  @param h2 - Overlayed histogram (right y-axis)
 *  @param pad - Address of a pad to be drawn on
 *  @param _markersFirst - 2d vector of TObject* which holds TMarkers or TLines to be drawn on the background plot. 1st index indicates pad number, 2nd index gives the object to be drawn
 *  @param _markersSecond - 2d vector of TObject* which holds TMarkers or TLines to be drawn on the foreground plots. 1st index indicates pad number, 2nd index gives the object to be drawn
 */
void Visualizer::OverlayHistos  ( TH1 *h1, TH1 *h2 , std::string _out_name, std::string _ext,
                                  std::string _oFolder, std::vector< TObject* >* _markersFirst,
                                  std::vector< TObject* >* _markersSecond ){
    TCanvas* canv = new TCanvas( _out_name.c_str(),_out_name.c_str(), 600, 800);

    OverlayHistos( h1, h2, (TPad*)canv, _markersFirst, _markersSecond );

    canv->Print( ( _oFolder + _out_name + _ext).c_str());
    delete canv;

}

/**
 * @brief Implementation of Visualizer::ManyPadsPlot. This method takes two vector of histograms that needs to be treated in a peculiar way and
 * takes care of plot those together in a canvas with a given number of columns and rows.
 * @param _first_form - vector of the first type of histograms to be plotted
 * @param _second_form - vector of the second type of histograms to be plotted s
 * @param _ncol - number of columns of the canvas
 * @param _nrow - number of rows of the canvas
 * @param _out_name - name of the plot (w/o extension, that's defined by a data member [ .pdf by default ]
 * @param _treatment - treatment of the plots (at the moment only one available, overlay)
 * @param _markersFirst - 2d vector of TObject* which holds TMarkers or TLines to be drawn on the background plots. 1st index indicates pad number, 2nd index indicates the object to be drawn
 * @param _markersSecond - 2d vector of TObject* which holds TMarkers or TLines to be drawn on the foreground plots. 1st index indicates pad number, 2nd index indicates the object to be drawn
 */
void Visualizer::ManyPadsPlot( std::vector< TH1* > _first_form, std::vector< TH1* > _second_form,
                               int _ncol, int _nrow, std::string det, std::string _out_name,
                               std::string _xTitle, std::string _yTitle, TString _treatment,
                               std::vector< std::vector< TObject* >* > *_markersFirst,
                               std::vector< std::vector< TObject* >* > *_markersSecond,
                               double _yMin1, double _yMax1, double _yMin2, double _yMax2){
    _treatment.ToLower();
    if(_treatment != "overlay"){
        std::cerr << "WARNING!!! You're looking for a treatment that has not been implemented yet! Please check it carefully" << std::endl;
        std::cerr << "Exiting w/o doing anything .." << std::endl;
        return;
    }

    if(_first_form.size() != _second_form.size())
        std::cerr << "WARNING!!! The two vectors of histograms "
                     "have different size. May result in a crash..." << std::endl;

    if( _first_form.size() < _ncol*_nrow ||  _second_form.size() < _ncol*_nrow )
        std::cerr << "WARNING!!! You have selected a vector of histrograms that will not fill all your pads "
                     "This may result in a crash..." << std::endl;

    if( _first_form.size() > _ncol*_nrow ||  _second_form.size() > _ncol*_nrow )
        std::cerr << "WARNING!!! You have selected a vector of histrograms that is bigger than the number of requested pads "
                     "This may result in histograms lost w/o plotting..." << std::endl;

    //This for the moment is hardcoded. Maybe can be moved to data member to have more general usage also for single plots.
    int squared_pad_size = 500;
    TCanvas* canv = new TCanvas( _out_name.c_str(),_out_name.c_str(),
                                 squared_pad_size*_ncol, squared_pad_size*_nrow);
    canv->Divide(_ncol,_nrow);

    //Find the full range of all of the histograms
    double ymin1=1.0e6, ymin2=1.0e6, ymax1=-1.0e6, ymax2=-1.0e6;
    for( int hist = 0; hist < _first_form.size(); hist++){
       _first_form.at(hist)->ResetStats();
      if( _first_form.at(hist)->GetMinimum()  < ymin1 ) ymin1 = _first_form.at(hist)->GetMinimum();
      if( _first_form.at(hist)->GetMaximum()  > ymax1 ) ymax1 = _first_form.at(hist)->GetMaximum();
      if( _second_form.at(hist)->GetMinimum() < ymin2 ) ymin2 = _second_form.at(hist)->GetMinimum();
      if( _second_form.at(hist)->GetMaximum() > ymax2 ) ymax2 = _second_form.at(hist)->GetMaximum();

    }
    if( _yMin1 !=0 )ymin1 = _yMin1;
    if( _yMax1 !=0 )ymax1 = _yMax1;
    if( _yMin2 !=0 )ymin2 = _yMin2;
    if( _yMax2 !=0 )ymax2 = _yMax2;

    //Draw
    for( int idraw = 0; idraw < _first_form.size(); idraw++){
      canv->cd(idraw+1);
        if( _treatment == "overlay"){
            OverlayHistos( _first_form.at(idraw), _second_form.at(idraw), gPad, _markersFirst->at(idraw), _markersSecond->at(idraw), ymin1, ymax1, ymin2, ymax2);
        }
        AddPlotLabel( _first_form.at(idraw), gPad, "", Form("%s %d", det.c_str(), idraw+1), _xTitle, _yTitle );
    }
    canv->Print(( _out_name + ".png" ).c_str());
    // canv->Print(( _out_name + m_extension ).c_str());

}


void Visualizer::SimplePadsPlot(  std::vector< TH1* > _vec_hist, int _ncol, int _nrow,
                                  std::string _xTitle, std::string _yTitle,
                                  std::string _saveName, std::string det, std::string ext,
                                  std::string _oFolder, bool _autoScale,
                                  std::vector< std::vector< TObject* >* >* _markers){
    int squared_pad_size = 500;
    TCanvas* canv = new TCanvas( _saveName.c_str(),_saveName.c_str(),
                                 squared_pad_size*_ncol, squared_pad_size*_nrow);
    canv->Divide(_ncol,_nrow);

    //Find the full range of all of the histograms if autoScale is true
    double ymin=1.0e6, ymax=-1.0e6;
    if(_autoScale){
      for( int hist = 0; hist < _vec_hist.size(); hist++){
        _vec_hist.at(hist)->ResetStats();
        if( _vec_hist.at(hist)->GetMinimum()  < ymin ) ymin = _vec_hist.at(hist)->GetMinimum();
        if( _vec_hist.at(hist)->GetMaximum()  > ymax ) ymax = _vec_hist.at(hist)->GetMaximum();
      }
    }

    for( int idraw = 0; idraw < _vec_hist.size(); idraw++){
      canv->cd(idraw+1);
      if(_autoScale){
        _vec_hist.at(idraw)->GetYaxis()->SetRangeUser( ymin, ymax );
      }
        _vec_hist.at(idraw)->GetXaxis()->SetTitle(_xTitle.c_str());
        _vec_hist.at(idraw)->GetXaxis()->SetTitleOffset(1.4);
        _vec_hist.at(idraw)->GetYaxis()->SetTitle(_yTitle.c_str());
        _vec_hist.at(idraw)->GetYaxis()->SetTitleOffset(1.4);
        _vec_hist.at(idraw)->Draw(ext.c_str());
        gPad->Update();

        AddPlotLabel(_vec_hist.at(idraw), gPad, ext, Form("%s %d", det.c_str(), idraw + 1), _xTitle, _yTitle );

        if( _markers != NULL ){
          for(int i = 0; i < _markers->at(idraw)->size(); i++){
            if(_markers->at(idraw)->at(i)->InheritsFrom("TF1")){
              _markers->at(idraw)->at(i)->Draw("same");
            }else{
              _markers->at(idraw)->at(i)->Draw();
            }
          }
        }
    }
    if( _oFolder == "" ){ _oFolder = m_oFolder; }
    canv->Print( ( _oFolder + _saveName).c_str() );
    delete canv;
}

/**
 * @brief Implementation of Visualizer::SinglePadPlot. This method takes two vectors of floats and plots them on a single pad with the requested "treatment".
 * Currently supports scatter plot and overlay
 * @param _v1 - vector of floats. Can represent x values for a scatter, or y values of a histogram
 * @param _v2 - vector of the second type of histograms to be plotted s
 * @param _out_name - name of the plot (w/o extension, that's defined by a data member [ .pdf by default ]
 * @param _treatment - treatment of the plots (scatter, overlay, overlay with lines)
 * @param _markers - 2d vector of TObject* which holds TMarkers or TLines to be drawn on the plots. 1st index indicates pad number, 2nd index indicates the object to be drawn
 */
void Visualizer::SinglePlot( std::vector< double > _v1, std::vector< double > _v2, std::string _out_name, TString _treatment, std::vector< TObject* >* _markers ){

    if(_v1.size() != _v2.size())
    std::cerr << "WARNING!!! The two vectors have different size. "
                 "May result in a crash..." << std::endl;

    int treatment = -1;
    _treatment.ToLower();
    if( _treatment == "overlay" ){ treatment = 1; }
    if( _treatment == "scatter" ){ treatment = 2; }

    int squared_pad_size = 300;
    TCanvas* canv = new TCanvas( _out_name.c_str(),_out_name.c_str(),
                                 squared_pad_size, squared_pad_size*2);

    switch( treatment ){
      case -1:
        std::cerr << "WARNING!!! You're looking for a treatment that has not been implemented yet! Please check it carefully" << std::endl;
        std::cerr << "Exiting w/o doing anything .." << std::endl;
        return;
      case 1:
        {
          TH1D* h1 = new TH1D( _out_name.c_str(), _out_name.c_str(), _v1.size(), _v1.at(0), _v1.at(_v1.size()-1));
          TH1D* h2 = new TH1D( (_out_name + "(1)").c_str(), (_out_name + "(1)").c_str(), _v1.size(), _v2.at(0), _v2.at(_v1.size()-1));

          //Fill each histogram separately in case they are different sizes
          for(int bin = 0; bin < _v1.size(); bin++){
              h1->SetBinContent(bin,_v1.at(bin));
          }
          for(int bin = 0; bin < _v2.size(); bin++){
              h2->SetBinContent(bin,_v2.at(bin));
          }

          OverlayHistos( h1, h2, canv->cd(), _markers);
          break;
        }

      case 2:
        ScatterPlot(_v1, _v2, canv->cd());
        break;

      default:
        std::cerr << "WARNING!!! You're looking for a treatment that has not been implemented yet! Please check it carefully" << std::endl;
        std::cerr << "Exiting w/o doing anything .." << std::endl;
        return;
      }// end switch
    canv->Print(( _out_name + m_extension ).c_str());
}

/**
 * @brief Draws a scatter plot from two vectors
 *
 * @param _vx - Vector of x values
 * @param _vy - Vector of y values
 * @param pad - Address of a pad to be drawn on
 */
void Visualizer::ScatterPlot( std::vector< double > _vx, std::vector< double > _vy, TVirtualPad* pad){
    //Declare TVectors using the input std::vectors
    TVectorD TVx;
    TVx.Use(_vx.size(),&_vx[0]);
    TVectorD TVy;
    TVy.Use(_vy.size(),&_vy[0]);

    TGraph g(TVx, TVy);
    g.DrawClone("ap");
}

/**
 * @brief Adds labels to plot
 *
 * @param h - Histogram to be labeled
 * @param pad - Pad the histogram is to be drawn on
 */
void Visualizer::AddPlotLabel(TH1* h, TVirtualPad* pad, std::string _opt, std::string det, std::string _xTitle, std::string _yTitle){

  auto lx = new TLatex();
  lx->SetTextAlign(31);
  lx->SetTextFont( 62 );
  lx->SetTextSize( 0.04 );
  lx->DrawLatexNDC(0.92,0.9,("ZDC " + m_category + m_year).c_str());
  lx->SetTextFont( 42 );
  lx->SetTextSize( 0.028 );
  lx->DrawLatexNDC(0.92,0.87,m_beam.c_str());
  lx->SetTextFont( 52 );
  lx->DrawLatexNDC(0.92,0.84,"Ongoing Analysis");
  lx->SetTextFont( 42 );
  lx->DrawLatexNDC(0.92,0.81, det.c_str() );
  pad->Update();

}



/**
 * @brief Sets plot labels based on run number and alignment data
 *
 */
void Visualizer::SetTestBeamLabel( int runNo, Alignment* alignment ){
    if(runNo == 1){
      m_year = "";
      m_category = "Simulated";
    }
    if(runNo <= 405 && runNo > 10){
      m_year = "2018";
      m_category = "Test Beam";
    }
    if(alignment->magnet_On && alignment->target_In) m_beam = "Fragments - Magnet On";
    if(!alignment->magnet_On && alignment->target_In) m_beam = "Fragments - Magnet Off";
    if(!alignment->magnet_On && alignment->target_In && alignment->lead_In) m_beam = "Fragments - Magnet Off - Pb absorber";
    if(!alignment->magnet_On && !alignment->target_In && !alignment->lead_In) m_beam = "Pb ions, 150 GeV/A";
}

/**
 * @brief Sets plot labels based on run number and alignment data for 2021 test beam
 *
 */
void Visualizer::SetTestBeamLabel( int runNo, Alignment2021* alignment ){
    if(runNo == 1){
      m_year = "2021";
      m_category = "Simulated";
    }
    else{
      m_year = "2021";
      m_category = "TestBeam";
    }
    m_beam = Form( "%s beam @ %.1f GeV", alignment->beam_type.c_str(), alignment->beam_energy );
}


void Visualizer::DrawPlot(TH1 *h2, std::string _xTitle, std::string _yTitle, std::string _saveName, std::string _opt, TMarker* _marker, std::string _oFolder)
{
    TCanvas* c1 = new TCanvas(_saveName.c_str(),"c1",600,500);
    c1->cd();
    gPad->SetLeftMargin(0.15);
    gPad->SetRightMargin(0.12);
    gPad->SetTopMargin(0.10);
    h2->Draw(_opt.c_str());
    h2->SetFillColorAlpha(kAzure+1,0.3);
    h2->SetLineColor(kAzure+1);
    h2->GetXaxis()->SetTitle(_xTitle.c_str());
    h2->GetXaxis()->SetTitleOffset(1.4);
    h2->GetYaxis()->SetTitle(_yTitle.c_str());
    h2->GetYaxis()->SetTitleOffset(1.4);
    TLatex* lx = new TLatex();
    lx->SetTextFont( 62 );
    lx->SetTextSize( 0.048 );
    if (!h2->InheritsFrom("TH2")){
        lx->DrawLatexNDC(0.49,0.83,("ZDC " + m_category + m_year).c_str());
        lx->SetTextFont( 42 );
        lx->SetTextSize( 0.038 );
        lx->DrawLatexNDC(0.49,0.79,m_beam.c_str());
        lx->SetTextFont( 52 );
        lx->DrawLatexNDC(0.49,0.75,"Ongoing Analysis");
    }
    if (h2->InheritsFrom("TH2")){
        lx->DrawLatexNDC(0.15,0.93,("ZDC " + m_category + m_year).c_str());
        lx->SetTextFont( 42 );
        lx->SetTextSize( 0.038 );
        lx->DrawLatexNDC(0.55,0.95,m_beam.c_str());
        lx->SetTextFont( 52 );
        lx->DrawLatexNDC(0.55,0.91,"Ongoing Analysis");
    }
    if(_marker){
        _marker->Draw();
    }
    if( _oFolder == "" ){ _oFolder = m_oFolder; }
    c1->Print( ( _oFolder + _saveName).c_str() );
    delete c1;

}
