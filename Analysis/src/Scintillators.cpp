/** @ingroup ana
 *  @file Scintillators.cpp
 *  @brief Implementation of Scintillators.
 *
 *  Function definitions for Scintillators are provided.
 *  This is a daughter class of Detector.
 *  Methods specific to Scintillatorss are implemented here.
 *
 *  @author Riccardo Longo
 *  @bug No known bugs.
 */

#include "Scintillators.h"


/** @brief Default Constructor for Scintillators.
 */
Scintillators::Scintillators( ){

}

/** @brief Name Constructor for Scintillators.
 */
Scintillators::Scintillators( std::string _name ){
  SetName(_name);
}

/** @brief Destructor for Scintillators.
 */
Scintillators::~Scintillators( ){

}

/**  @brief Implementation of LoadElements for the Scintillators class
  *  @param _elements Vector of Channels with all detector configs
  *  @param _runNumber Current run number being analyzed
  *
  *  Adds the Scintillator channels to the channels vector
  *
  */
void Scintillators::LoadElements( std::vector < Channel* > _elements, int _runNumber ){

    m_runNumber = _runNumber;
    for(int i = 0; i < (int)_elements.size(); i++){
        if((_elements.at(i)->detector.find("TRIG") != std::string::npos)){
            SetElement(_elements.at(i));
        }
    }
    if(GetChannelsVector().size() > 4)
      std::cout << "WARNING : more than one entry for one Scintillators module. Check the config.xml" << std::endl;

    if(GetChannelsVector().size() > 0)
      std::cout << "Scintillators object created with " << GetChannelsVector().size() << " channel entries " << std::endl;
}

/**  @brief Get specific scintillator paddle by reference ID
  *
  *  @param _scintillatorID Reference string for the scintillator. Can be LV (Large Vertical), LH (Large Horizontal), SV (Small Vertical), or SH (Small Horizontal).
  *
  */
Channel* Scintillators::GetScintillator( std::string _scintillatorID ){

    for(int i = 0; i < 2; i++){
      for(int j = 0; j < 2; j++){
        Channel *c = GetElement(i, j);
        if( FindPaddleType( c ) == _scintillatorID)
          return c;
      }
    }
    std::cout << "ERROR! Scintillator " << _scintillatorID << " not found " << std::endl;
    return NULL;
}
/** @brief From channel returns the Scintillator ID **/
std::string Scintillators::FindPaddleType( Channel *c ){

  if(c->mapping_row == 1 && c->mapping_column == 1)
    return "LV";
  if(c->mapping_row == 1 && c->mapping_column == 2)
    return "LH";
  if(c->mapping_row == 2 && c->mapping_column == 1)
    return "SV";
  if(c->mapping_row == 2 && c->mapping_column == 2)
    return "SH";

    //Default behavior
    return "";
}
/** @brief Prints a map of the Scintillators to the terminal
 *
 * Prints a "map" of the Scintillators.
 * Displays one element with Scintillators number on the top line, DRS4
 * Channel on the second line, and if the element is functioning on the
 * third line.
 *
 */
void Scintillators::PrintMap(){

  for(int i = 1; i <= 2; i++){
    for(int j = 1; j <= 2; j++){
      Channel *c = GetElement(i, j);
      std::cout<<"|   "     << FindPaddleType(c)  <<"  |"<<std::endl;
    	std::cout<<"|   "     << c->name <<"  |"<<std::endl;
    	std::cout<<"|       |"<< std::endl;
    	std::cout<<"|_______|"<< std::endl;
    }
  }


}
