/** @ingroup ana
 *  @file EM.cpp
 *  @brief Implementation of EM.
 *
 *  Function definitions for EM are provided.
 *  This is a daughter class of Detector.
 *  Methods specific to EMs are implemented here.
 *
 *  @author Aric Tate
 *  @bug No known bugs.
 */

 /*
 MAPPING
  J        1 4 7
 --> Beam  2 5 8
  S        3 6 9
 */

#include "EM.h"

#include <string>
#include <stdio.h>
#include <fstream>
#include <iomanip>


/** @brief Default Constructor for EM.
 */
EM::EM( ){

}

/** @brief Name Constructor for EM.
 */
EM::EM( std::string _name ){
  SetName(_name);
}

/** @brief Destructor for EM.
 */
EM::~EM( ){

}

/** @brief Implementation of LoadElements for the EM class
 *  @param _elements Vector of Channels with all detector configs
 *  @param _runNumber Current run number being analyzed
 *
 *  Adds the EM channels to the channels vector
 *
 */
void EM::LoadElements( std::vector < Channel* > _elements, int _runNumber ){

    // Set up the row translation if run number is from 2021 test beam
    for(int i = 0; i < m_nRows; i++){
        rowTranslation[i] = i+1;
    }
    // Set up the column translation if run number is from 2021 test beam
    for(int i = 0; i < m_nColumns; i++){
        colTranslation[i] = i+1;
    }

    ResizeSortedElements();
    std::cout << std::endl << "EM MODULE SECTION-SIGNAL MAPPING" << std::endl << "---------------------------------" << std::endl;
    for(int row = 0; row < m_nRows; row++){
        for(int column = 0; column < m_nColumns; column++){
            for(int i=0; i < (int)_elements.size(); i++){
                if(_elements.at(i)->mapping_row == rowTranslation[row]
                    && _elements.at(i)->mapping_column == colTranslation[column]
                    && ( _elements.at(i)->detector == GetName() ) ){
                    m_SortedElements.at(row).push_back(_elements.at(i));

                    std::cout <<  Form("Config: X_Col[%d], Y_Row[%d] -> Array: X_Col[%d], Y_Row[%d] -> Channel[%s] -> Det[%s]",
                                  colTranslation[column], rowTranslation[row],  column, row,
                                  m_SortedElements[row][column]->name.c_str(),
                                  m_SortedElements[row][column]->detector.c_str()) << std::endl;


                    SetElement(_elements.at(i));
                }
            }
        }
    }

    if(GetChannelsVector().size() > 0)
      std::cout << "EM object created with " << m_nRows << " rows and " << m_nColumns << " columns" << std::endl;

}

/** @brief Get the properties of a detector element
 *  @param row Row of element to be accessed
 *  @param column Column of element to be accessed
 *  @return Pointer to the Channel of the requested element
 *
 * If m_SortedElements is populated, return the element in
 * m_SortedElements[row][column].
 * Otherwise returns a pointer to the Channel stored in m_Element
 * with the requested row and column.
 * If the requested element is not found, return a warning message and a NULL pointer.
 *
 */
Channel* EM::GetElement(int row, int column){
    if( ((int)m_SortedElements.size()) == m_nRows){
      return m_SortedElements[row][column];
    }else{
        for(int i=0; i < (int)GetChannelsVector().size(); i++){
            if(rowTranslation[row] == GetChannelsVector().at(i)->mapping_row && colTranslation[column] == GetChannelsVector().at(i)->mapping_column){
              return GetChannelsVector().at(i);
            }
        }
     }
  std::cerr << " WARNING: Element (" << row << "," << column << ") not found! " << std::endl;
  return nullptr;

}

/** @brief Prints a map of the EM to the terminal
 *
 * Prints the map of a 3x3 EM.
 * Displays a grid of elements with row,column on the top line, DRS4
 * Channel on the second line, and if the element is functioning on the
 * third line.
 *
 */

// void EM::PrintMap(){
//   std::cout << " ___________________________________ " << std::endl;
//   //EM has 3 rows and 3 columns
//   for(int row = 1; row <= m_nRows; row++){
//       Channel* c[5];
//       std::string status, name;
//       for(int cln = 1; cln <= m_nColumns; cln++){
//
//           c[cln] = GetElement(row,cln);
//           if(c[cln]->is_on) status = "ON";
//               else status = "OFF";
//           if (cln == 1) std::cout << "|  " << row << "," << cln << " --> " << c[cln]->name << " , " << status;
//           else std::cout << "  |  " << row << "," << cln << " --> " << c[cln]->name << " , " << status;
//
//         }//End of the loop over columns
//       }//End of the loop over rows
// }


void EM::PrintMap(){
  //EM has Z rows and X columns
  for(int col = 0; col < m_nColumns; col++){
      std::string status;
      for(int row = 0; row < m_nRows; row++){
          if(GetElement(col,row)->is_on) status = "ON";
              else status = "OFF";
          if(row != m_nColumns-1)   std::cout << "| " << col << "," << row << " --> \t " << GetElement(col,row)->name.c_str() << " , " << status;
          else                      std::cout << "| " << col << "," << row << " --> \t " << GetElement(col,row)->name.c_str() << " , " << status << " |" << std::endl;
        }//End of the loop over columns
      }//End of the loop over rows
}

/** @brief Resizes m_SortedElements
 *
 * Uses m_nRows and m_nColumns to resize m_SortedElements
 * 2D vectors must have columns sized individually.
 *
 */
void EM::ResizeSortedElements(){
    m_SortedElements.resize(m_nRows);
    for(int row=0; row < m_nRows; row++){
        m_SortedElements.resize(m_nColumns);
    }
}
