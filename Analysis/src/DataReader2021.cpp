 /** @defgroup ana Analysis
 *   @ingroup ana
 *   @file DataReader2021.cpp
 *   @brief Implementation of DataReader2021.
 *
 *  Function definitions for DataReader2021 are provided.
 *  This class reads a rootfile with raw waveforms
 *  that are processed by rcdaqAnalysis running on prdf files.
 *  Then, in the event loop, analysis classes can be called.
 *
 *  @author Yakov Kulinich, Riccardo Longo
 *  @bug No known bugs.
 */

#include <TFile.h>
#include <TTree.h>

#include <iostream>
#include <fstream>
#include <iomanip>

#include "DataReader2021.h"


/** @brief Default Constructor for DataReader2021.
 */
DataReader2021::DataReader2021() : DataReader2021( 0, 0, "", 0 ){

}

/** @brief Constructor for DataReader2021.
 *
 *  @param nCh Number of channels being read
 *  @param nSamp Number of samples per channel
 */
DataReader2021::DataReader2021( const unsigned int nCh, const unsigned int nSamp )
  : DataReader2021( nCh, nSamp, "", 0 ){

  // here say we will read in list of files, maybe
  // there is better way to do it, just an idea for now.
  m_readListOfFiles = true;
}

/** @brief Constructor for DataReader2021.
 *
 *  @param nCh Number of channels being read
 *  @param nSamp Number of samples per channel
 *  @param fNameIn Input filename.
 */
DataReader2021::DataReader2021( const uint nCh, const uint nSamp,
			const std::string& fNameIn )
  : DataReader2021( nCh, nSamp, fNameIn, 0 ){

}


/** @brief Constructor for DataReader2021.
 *
 *  @param nCh Number of channels being read
 *  @param nSamp Number of samples per channel
 *  @param4 fNameIn Output file name (custom)
 *  @param3 runNum Run number being used.

 */
DataReader2021::DataReader2021( const uint nCh, const uint nSamp,
			const std::string& fNameIn, const uint runNum )
  : m_nCh( nCh ), m_nSamp( nSamp ), m_fNameIn( fNameIn ),
    m_runNumber( runNum ), m_readListOfFiles( false ), m_fIn( NULL ){

}


/** @brief Destructor for DataReader2021.
 */
DataReader2021::~DataReader2021(){


  for( auto& time : m_time ){
      delete time; time = NULL;
  }
  for( auto& det : m_detectors ){
      delete det; det = NULL;
  }
  for( auto& ana : m_ana ){
    delete ana; ana = NULL;
  }
  for( auto& det_ana : m_det_ana ){
    delete det_ana; det_ana = NULL;
  }

  //This line deletes all histograms so we don't have to
  gDirectory->GetList()->Delete();

  delete m_alignment2021; m_alignment2021 = NULL;
  delete m_fOut; m_fOut = NULL;
  delete m_fIn; m_fIn = NULL;

}

/** @brief Adds an analysis to vector of analysis to be executed before the detector analysis (e.g. WaveForm Analysis)
 *
 *  @param ana Pointer to an Analysis.
 *
 *  @return none
 */
void DataReader2021::AddPreAnalysis( Analysis* ana ){
  ana->SetVerbosity( m_verbose );
  m_ana.push_back( ana );
}

/** @brief Adds an analysis to vector of detector analysis (e.g. ZDC, RPD or combined)
 *
 *  @param ana Pointer to an Analysis.
 *
 *  @return none
 */
void DataReader2021::AddDetectorAnalysis( Analysis* det_ana ){
  det_ana->SetVerbosity( m_verbose );
  m_det_ana.push_back( det_ana );
}

/** @brief Adds an detector to vector of detectors (e.g. ZDC, RPD ...)
 *
 *  @param ana Pointer to a Detector.
 *
 *  @return none
 */
void DataReader2021::AddDetector( Detector* det ){
  det->SetVerbosity( m_verbose );
  m_detectors.push_back( det );
}

/** @brief Enables the read from list of files option for DataReader2021
 *
 *  @param listname name of the list of files to be processed (with the full path if it's not in the execution folder)
 *
 *  @return none
 */
void DataReader2021::ReadListOfFiles( std::string listname ){

    m_readListOfFiles = true;
    m_fListOfFiles = listname;
}


/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
void DataReader2021::LoadAlignmentFile(std::string _inFile ){
    if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/Alignment_2021.xml");
    m_XMLparser = new XMLSettingsReader();

    if (!m_XMLparser->parseFile(_inFile)) {
            std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
            return;
    }

    m_alignment2021 = new Alignment2021();
    m_alignment2021->runNumber = m_runNumber;

    std::cout << "Loading .xml Alignment File..." << std::endl;
    std::cout << "Found " << m_XMLparser->getBaseNodeCount("Alignment") << " alignment entries " << std::endl;
    std::cout << "Retrieving the information for run " << m_runNumber << std::endl;

    int run;
    for (unsigned int i = 0; i < m_XMLparser->getBaseNodeCount("Alignment"); i++) {
        m_XMLparser->getChildValue("Alignment",i,"run",run);
        if(run != m_runNumber) continue;
        std::cout << "Found Run Entry in Alignment file for run " << m_runNumber << std::endl;
        m_XMLparser->getChildValue("Alignment",i,"beam_type",m_alignment2021->beam_type);
        m_XMLparser->getChildValue("Alignment",i,"beam_energy",m_alignment2021->beam_energy);
        m_XMLparser->getChildValue("Alignment",i,"x_table",m_alignment2021->x_det_table);
        m_XMLparser->getChildValue("Alignment",i,"y_table",m_alignment2021->y_det_table);
        m_XMLparser->getChildValue("Alignment",i,"x_trigger",m_alignment2021->x_trig_table);
        m_XMLparser->getChildValue("Alignment",i,"y_trigger",m_alignment2021->y_trig_table);
        m_XMLparser->getChildValue("Alignment",i,"Det1",m_alignment2021->Det1);
        m_XMLparser->getChildValue("Alignment",i,"Det2",m_alignment2021->Det2);
        m_XMLparser->getChildValue("Alignment",i,"Det3",m_alignment2021->Det3);
        m_XMLparser->getChildValue("Alignment",i,"Det4",m_alignment2021->Det4);
        m_XMLparser->getChildValue("Alignment",i,"Det5",m_alignment2021->Det5);
        m_XMLparser->getChildValue("Alignment",i,"Det6",m_alignment2021->Det6);
    }

    if(m_alignment2021 == NULL) std::cout << "WARNING: ALIGNMENT NOT FOUND!!!" << std::endl;
    delete m_XMLparser; m_XMLparser = NULL;
    return;

}

/**
 * @brief Reads the .xml configuration file and load characteristics for all the channels, immediately sorted into detectors objects
 * @param _inFile
 */
void DataReader2021::LoadConfigurationFile(std::string _inFile ){
    if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/ConfigFile2021.xml");
    m_XMLparser = new XMLSettingsReader();

    if (!m_XMLparser->parseFile(_inFile)) {
            std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
            return;
    }

    std::cout << "Loading .xml Configuration File..." << std::endl;
    std::cout << "Found " << m_XMLparser->getBaseNodeCount("channel") << " channel entries " << std::endl;

    std::string gainFP;
    if( m_gain_file == "" ){
      gainFP = std::getenv("JZCaPA") + std::string("/Utils/PMTgain.root");
    }else{
      gainFP = m_gain_file;
    }
    TFile gainFile( gainFP.c_str(), "read");

    std::vector < Channel* > channelEntries;
    int first_run, last_run;

    for (unsigned int i = 0; i < m_XMLparser->getBaseNodeCount("channel"); i++) {
        Channel *buffer_ch = new Channel();
        m_XMLparser->getChildValue("channel",i,"start_run",first_run);
        m_XMLparser->getChildValue("channel",i,"end_run",last_run);

        //Discard entries for any channel that does not apply to our run
        if(m_runNumber < first_run || m_runNumber > last_run) continue;

        //If the entry applies, we store it in the vector
        m_XMLparser->getChildValue("channel",i,"detector",buffer_ch->detector);
        m_XMLparser->getChildValue("channel",i,"name",buffer_ch->name);
        m_XMLparser->getChildValue("channel",i,"mapping_row",buffer_ch->mapping_row);
        m_XMLparser->getChildValue("channel",i,"mapping_column",buffer_ch->mapping_column);
        m_XMLparser->getChildValue("channel",i,"pmt_code",buffer_ch->PMTcode);
        m_XMLparser->getChildValue("channel",i,"delay",buffer_ch->delay);
        m_XMLparser->getChildValue("channel",i,"offset",buffer_ch->offset);
        m_XMLparser->getChildValue("channel",i,"HV",buffer_ch->HV);
        m_XMLparser->getChildValue("channel",i,"is_pos_polarity",buffer_ch->pos_polarity);
        m_XMLparser->getChildValue("channel",i,"is_on",buffer_ch->is_on);
        m_XMLparser->getChildValue("channel",i,"Vop",buffer_ch->Vop);
        m_XMLparser->getChildValue("channel",i,"adc_per_mv",buffer_ch->adc_mV);

        //Get the voltage vs gain plot for this PMT. If it exists, set the gain value for the channel
        TGraph *gainPlot = (TGraph*)gainFile.Get(buffer_ch->PMTcode.c_str());
        if(gainPlot){
          buffer_ch->gain = gainPlot->Eval(buffer_ch->HV);
        }else{
          buffer_ch->gain = 0;
          std::cout << Form("No gain plot found for %s row %d col %d", buffer_ch->detector.c_str(), buffer_ch->mapping_row, buffer_ch->mapping_column) << std::endl;
        }

        bool isNew(true);
        for( int k = 0; k < channelEntries.size(); k++){
            if(buffer_ch->name == channelEntries.at(k)->name){
                std::cout << "WARNING!!! Redundancy in your settings file for " << buffer_ch->name << ". Check it carefully. The second entry found will be skipped..." << std::endl;
                isNew = false;
            }
        }
        if(isNew) channelEntries.push_back(buffer_ch);
    }

    std::cout << "Loaded " << channelEntries.size() << " configuration entries " << std::endl;

    std::string names = "";

    //Hand the channels to the detectors so they can pick theirs from the list
    //If a detector doesn't find its channels, remove it from the vector (and analysis)
    //If it does find its channels, add its name to the list to print for the user
    for(int det = 0; det < m_detectors.size(); det++){
      m_detectors.at(det)->LoadElements(channelEntries, m_runNumber);
      if( m_detectors.at(det)->GetChannelsVector().size() == 0 ){
        std::cout << "No entries found for " << m_detectors.at(det)->GetName() << ". Removing detector from analysis." << std::endl;
        m_detectors.erase(m_detectors.begin() + det);
      }else{
        names+= m_detectors.at(det)->GetName() + "; ";
        m_detectors.at(det)->PrintMap();
      }
    }
    std::cout << "Detectors found in config file: " << names << std::endl << std::endl;

    delete m_XMLparser; m_XMLparser = NULL;
    return;
}


/** @brief Loads calibrated timing information for DRS4 modules based on run number
 *  @param _inFile Optional argument for loading a custom file
 *
 *  Uses run number to determine scan number and loads the appropriate DRS4 timing
 *  information from the 2018 Testbeam. After loading, we hand each Channel a
 *  pointer to its timing vector. Must be run after LoadConfigurationFile()
 *
 */
void DataReader2021::LoadTimingFile(std::string _inFile){
    std::string inFile,buffer;
    char data[14];
    float dat;
    std::vector< Channel* > vChannel;
    m_time.resize(8);
    for(int i = 0; i < 8; i++){
        m_time[i] = new std::vector< float >;
    }

    int chNo;
    //update for 2021
    int start[] = {79,  152, 190, 202, 215, 258 };
    int stop[]  = {112, 171, 200, 213, 231, 1000 };
    int scanNum = 0;

    //If the user has selected a file, use it and skip determining the scan number
    if(_inFile != ""){
        std::cout << "Using timing file definied by user " << _inFile << std::endl;
        inFile = _inFile;
        goto open;
    }

    //Determine scan number using ranges defined by start and stop.
    //Scan 6-13 have identical timing data, so we treat them all as scan 6
    for(int i = 0; i < 6; i++){
        if(start[i]<=m_runNumber && stop[i]>=m_runNumber){
            scanNum = i+1;
        }
    }

    inFile = (std::string)std::getenv("JZCaPA") + Form("/Utils/Timing_data/2018scan%d.txt",scanNum);//UPDATE FOR 2021!!!

    open:
    std::ifstream file( inFile.c_str() );
    if( !file.is_open() ){
        std::cerr << "WARNING: Timing data file didn't open " << inFile << std::endl;
        return;
    }

    //Get the headers out
    getline(file,buffer);
    getline(file,buffer);

    float buffero[8];
    while( file >> buffero[0] >> buffero[1] >> buffero[2] >> buffero[3] >> buffero[4] >> buffero[5] >> buffero[6] >> buffero[7])
    for(int drs = 0; drs<8; drs++){
        m_time[drs]->push_back( buffero[drs] );
      }

    file.close();

    //Loop through the detector vector and their Channel vectors assigning the
    //time vector based on hardware channel number (Channel::name)
    for( auto& det : m_detectors ){
        vChannel = det->GetChannelsVector();
        for( Channel* ch : vChannel ){
            buffer = ch->name;
            //Remove "Signal" from name and convert to int
            buffer.erase(0,6);
            chNo = atoi( buffer.c_str() );

            //If the channel already has a time vector, print an error and continue
            if(ch->pTimeVec != 0){
                std::cerr << "WARNING: Overwriting Channel time vector" << std::endl;
            }
            int myCh = (int)chNo/8;
            ch->pTimeVec = m_time[myCh];

            // std::cout <<  Form("Time:Row[%d]Col[%d] -> Channel[%s] -> Det[%s] -> chNo[%d]",
            //               ch->mapping_row, ch->mapping_column,
            //               ch->name.c_str(),
            //               ch->detector.c_str(),
            //               chNo) << std::endl;

        }
    }
    std::cout << "Timing File: loading complete " << std::endl;
}


/** @brief Loads calibrated timing information for DRS4 modules based on run number
 *  @param _inFile Optional argument for loading a custom file
 *
 *  Uses run number to determine scan number and loads the appropriate DRS4 timing
 *  information from the 2018 Testbeam. After loading, we hand each Channel a
 *  pointer to its timing vector. Must be run after LoadConfigurationFile()
 *
 */
void DataReader2021::LoadSurveyFile(std::string _inFile){
  if( _inFile == "" ) _inFile = std::getenv("JZCaPA") + std::string("/Utils/Survey_2021.xml");
  m_XMLparser = new XMLSettingsReader();

  if (!m_XMLparser->parseFile(_inFile)) {
          std::cerr << " Data Reader could not parse file : " << _inFile << std::endl;
          return;
  }

  std::vector < Survey* > surveyEntries;
  int first_run, last_run;
  double x,y,z,tx,ty,ttx,tty,qw,qi,qj,qk;

  for (unsigned int i = 0; i < m_XMLparser->getBaseNodeCount("Survey"); i++) {
      Survey *buffer = new Survey();

      m_XMLparser->getChildValue("Survey",i,"start_run",first_run);
      m_XMLparser->getChildValue("Survey",i,"end_run",last_run);

      //Discard entries for any channel that does not apply to our run
      if(m_runNumber < first_run || m_runNumber > last_run) continue;

      //If the entry applies, we store it in the vector
      m_XMLparser->getChildValue("Survey",i,"detector",buffer->detector);
      std::cout << "Parsing " << buffer->detector << " from run " << first_run << " to " << last_run << std::endl;
      m_XMLparser->getChildValue("Survey",i,"x_pos",x);
      m_XMLparser->getChildValue("Survey",i,"y_pos",y);
      m_XMLparser->getChildValue("Survey",i,"z_pos",z);
      buffer->pos.SetXYZ(x,y,z);
      m_XMLparser->getChildValue("Survey",i,"trigger_x",ttx);
      m_XMLparser->getChildValue("Survey",i,"trigger_y",tty);
      buffer->trig_table.SetXYZ(ttx,tty,0.0);
      m_XMLparser->getChildValue("Survey",i,"table_x",tx);
      m_XMLparser->getChildValue("Survey",i,"table_y",ty);
      buffer->table.SetXYZ(tx,ty,0.0);
      m_XMLparser->getChildValue("Survey",i,"w",qw);
      m_XMLparser->getChildValue("Survey",i,"i",qi);
      m_XMLparser->getChildValue("Survey",i,"j",qj);
      m_XMLparser->getChildValue("Survey",i,"k",qk);
      buffer->q.SetComponents(qw,qi,qj,qk);

      bool isNew(true);
      for( int k = 0; k < surveyEntries.size(); k++){
          if(buffer->detector == surveyEntries.at(k)->detector){
              std::cout << "WARNING!!! Redundancy in your settings file for " << buffer->detector << ". Check it carefully. The second entry found will be skipped..." << std::endl;
              isNew = false;
          }
      }
      if(isNew) surveyEntries.push_back(buffer);

  }

  std::string names = "";
  bool found = false;
  //Hand the surveys to the detectors so they can pick theirs from the list
  //If a detector doesn't find its survey, remove it from the vector (and analysis)
  //If it does find its survey, add its name to the list to print for the user
  for(int det = 0; det < m_detectors.size(); det++){
    found = m_detectors.at(det)->LoadSurvey(surveyEntries);
    if( !found ){
      std::cout << "No survey entries found for " << m_detectors.at(det)->GetName() << ". Removing detector from analysis." << std::endl;
      m_detectors.erase(m_detectors.begin() + det);
    }else{
      names+= m_detectors.at(det)->GetName() + "; ";
    }
  }

  std::cout << "Detectors found in survey file: " << names << std::endl << std::endl;

  delete m_XMLparser; m_XMLparser = NULL;
}



/**
 * @brief DataReader2021::GetDetector allows the user to access the detectors objects after loading them at the beginning of the execution
 * @param _detName can be ZDC1 (upstream module), ZDC2 (downstream module) or RPD.
 * @return
 */
Detector* DataReader2021::GetDetector( std::string _detName ){

    std::cout << std::left << std::setw(30) <<  Form( "Looking for Detector[ %s ]", _detName.c_str() ) << " <== ";

    for( int iDet = 0; iDet < (int)m_detectors.size(); iDet++){
        //std::cout << std::left << std::setw(19) <<  Form( "DETNAME_%d[ %s ]", iDet, m_detectors.at(iDet)->GetName().c_str() );
        if( _detName == m_detectors.at(iDet)->GetName() ){
          std::cout << "FOUND!" << std::endl << std::endl;
          return m_detectors.at(iDet);
        }
    }
    std::cout << "NOT FOUND!" << std::endl;
    std::cout << "WARNING: detector recognition glitch. NULL pointer being returned..." << std::endl;
    return NULL;
}


/** @brief Console output update
 *  @return none
 *
 *  Called by the TTimer in ProcessEvents
 *  Updates the console with CPU and RAM usage,
 *  number of events processed and events/second
 *  if verbosity is not zero
 */
void DataReader2021::UpdateConsole( Long_t _updateRate){

    if( m_verbose == 0 ){ return; }
    if(m_event!=0){


        // Get CPU information
        gSystem->GetCpuInfo(&m_cpuInfo, 100);
        gSystem->GetProcInfo( &m_procInfo );
        // Get Memory information
        gSystem->GetMemInfo(&m_memInfo);
        // Get events/second
        double rate = 1000*(m_event-m_event_old)/_updateRate;
        m_event_old = m_event;

        std::cout << "\r" << std::left <<  Form("Processed %5d events, ", m_event);
        std::cout << Form( "%5.1f ev/s, ", rate);
        std::cout << Form( "CPU use/time: %3d%%/%6.1fs, ", (int)m_cpuInfo.fTotal, (double)m_procInfo.fCpuSys + m_procInfo.fCpuUser);
        std::cout << Form( "RAM:%4.1f/%4.1fGB, ", (double)m_memInfo.fMemUsed/1024, (double)m_memInfo.fMemTotal/1024);
        std::cout << Form( "RAM used by process: %ldMB   ", (m_procInfo.fMemResident + m_procInfo.fMemVirtual)/1024 );
        std::cout << std::flush;
    }
}

/** @brief Run method for DataReader2021
 *  @return none
 *
 *  Call Initialize, ProcessEvents, and Finalize
 *  Made so the driver class only has to call one method.
 *
 */
void DataReader2021::Run(){
  std::cout << "~=~=~=~=~=~ Initialize Start"  << std::endl;
  Initialize();
  std::cout << "~=~=~=~=~=~ Initialize Stop"  << std::endl;
  if( !m_fIn ){
      std::cerr << "Input file didn't open... exiting" << std::endl;
      return;
  }
  std::cout << "~=~=~=~=~=~ ProcessEvents Start"  << std::endl;
  ProcessEvents();
  std::cout << "~=~=~=~=~=~ ProcessEvents Stop"  << std::endl;

  std::cout << "~=~=~=~=~=~ Finalize Start"  << std::endl;
  Finalize();
  std::cout << "~=~=~=~=~=~ Finalize Stop"  << std::endl;
}

/** @brief Initialization method for DataReader2021
 *
 *  Select which file(s) to read. For now just a single
 *  file, later this can be extended to read many and make
 *  chain of files for example. Also create and initialize
 *  the analysis that will be running.
 *
 *  @return none
 */
void DataReader2021::Initialize(){

  if( m_readListOfFiles ){

    // Riccardo - 21/01/2019 - TChain implementation
    // The file list name is supposed to be already set

    m_fileChain = new TChain("tree");
    std::ifstream inFile;
    inFile.open(m_fListOfFiles.c_str());
    std::string reader_buff;
    while(inFile >> reader_buff){
        //Let's push all the files in the list into the TChain
        m_fileChain->Add(reader_buff.c_str());
    }
    /** TODO - Add fileChain reading below
     */
  } else {
    m_fIn = TFile::Open( m_fNameIn.c_str() );
  }

  std::cout << Form("Reading Input File:  %s", m_fNameIn.c_str()) << std::endl;

  // If no output directory is specified by the user
  // one is created for this run in $JZCaPA/results. Default is $JZCaPA/results/
  // If an output tag has been chosen, use it to name the directory. Default is "run"
  m_outputDir = ( m_outputDir != "" ) ? m_outputDir : (std::string)std::getenv("JZCaPA") + "/results";
  m_outputTag = ( m_outputTag == "" ) ? "run" : m_outputTag;
  m_outputDir += Form("/%s%d/", m_outputTag.c_str(), m_runNumber);
  gSystem->Exec( Form("mkdir -p %s", m_outputDir.c_str() ) );

  // If we are reading a list of files, or have no run number
  // make default name output.root, otherwise make it
  // outputN.root, where N is a run number of a file.
  // If an output tag has been chosen, use it to name the file
  std::string fTag = ( m_outputTag == "" ) ? "output" : m_outputTag;
  std::string fNameOut = m_readListOfFiles ?
    (m_outputDir + fTag + ".root").c_str() : Form( (m_outputDir + "%s%d.root").c_str(), fTag.c_str(), m_runNumber );

  m_fOut = new TFile( fNameOut.c_str(), "RECREATE" );
  m_tOut = new TTree("AnalysisTree","AnalysisTree");
  m_tOut->Branch("runNumber", &m_runNumber, "m_runNumber/i");
  m_tOut->Branch("evNo", &m_event, "m_event/I");
  m_tOut->Branch("x_det_table", &m_alignment2021->x_det_table, "x_det_table/D");
  m_tOut->Branch("y_det_table", &m_alignment2021->y_det_table, "y_det_table/D");
  m_tOut->Branch("x_trig_table", &m_alignment2021->x_trig_table, "x_trig_table/D");
  m_tOut->Branch("y_trig_table", &m_alignment2021->y_trig_table, "y_trig_table/D");

  m_tIn = static_cast< TTree* >( m_fIn->Get( "tree" ) );

  //All the raw channels addresses set for read-out
  for( uint detID = 0; detID < (int) m_detectors.size(); detID++ ){
       m_detectors.at(detID)->SetBranches(m_tIn);
       m_detectors.at(detID)->SetNSamples(m_nSamp);
       m_detectors.at(detID)->DeclareHistograms();
  }

  std::cout << "File: " << m_fIn->GetName() << " has " << m_tIn->GetEntries() << " events." << std::endl;

  for( uint detID = 0; detID < (int) m_detectors.size(); detID++ ){
     m_detectors.at(detID)->SetAlignment2021(m_alignment2021);
  }

  for( auto& ana : m_ana ){
    ana->Initialize();
    ana->SetupHistograms();
  }
  for( auto& det_ana : m_det_ana ){
    det_ana->Initialize( m_detectors );
    det_ana->SetupHistograms();
    det_ana->SetBranches( m_tOut );
  }

}



/** @brief Process Events method for DataReader2021
 *
 *  Connect to files / chain of files.
 *  Read tree event by event, and fill basic waveforms.
 *  So far (12.12.18) For each event - Read Raw data for all channels,
 *  put it into 2D vector size NxM where N = nCh, M = nSamp
 *  Also, For each event - Fill N 1D histos that have size M.
 *  Then one can send these to any Analysis function they make.
 *  Here for example (12.12.18), we send to WFAnalysis::AnalyzeEvent( .. )
 *  See below - Have fun!
 *
 *  @return none
 */
void DataReader2021::ProcessEvents(){
  /** TODO : add reading for list of files
    * Please note that many of the implementations are now for a single-file treatment
    */

  // !! EVENT LOOP
  int nEventsToProcess;
  if( m_maxEvents == -1 ){
    nEventsToProcess = m_tIn->GetEntries();
  }else{
    if( m_maxEvents > m_tIn->GetEntries() ){
      nEventsToProcess = m_tIn->GetEntries();
      std::cout << Form("Requested number of events to be processed (%d) is more than this file contains (%lld)", m_maxEvents, m_tIn->GetEntries() ) << std::endl;
      std::cout << "Setting number of events to be processed to " << nEventsToProcess << std::endl << std::endl;
    }else{
      nEventsToProcess = m_maxEvents;
      std::cout << "Events to be processed set to " << nEventsToProcess << " by user" << std::endl << std::endl;
    }
  }
  for( int ev = 0; ev < nEventsToProcess; ev++ ){
    m_event = ev;
    m_tIn->GetEntry( ev );

    // Fill the raw waveforms
    for( uint detID = 0; detID < (int) m_detectors.size(); detID++ ){
       m_detectors.at(detID)->FillHistograms();
     }
    // Now call all analysis and run their AnalyzeEvent.
    // Can either send a vector of histos, or a 2D vector
    // Of all the data, depending on what you want to do.
    for( auto& ana : m_ana ){
      //raw data analysis
      for( auto& det : m_detectors ){
        ana->AnalyzeEvent( det->GetChannelsVector() );
      }
    }
    for( auto& det_ana : m_det_ana ){
      //Detector level analysis
      det_ana->AnalyzeEvent();
    }
  m_tOut->Fill();

  } // End event loop
}

/** @brief Finalize method for DataReader2021
 *
 *  Close any input files
 *  Call analysis finalize methods
 *
 *  @return none
 */
void DataReader2021::Finalize(){

  if( m_fIn ){
    m_fIn->Close();
  }

  // enter the output file since
  // we will be writing to it now.
  m_fOut->cd();

  //Add label, if wanted
  //To understand the kind of measurement, we use alignments
  Visualizer* viz = new Visualizer( "ATLAS" );

  if(m_useLabel){
      viz->SetOutputDirectory( m_outputDir );
      viz->SetTestBeamLabel( m_runNumber, m_alignment2021);
  }

  for( auto& ana : m_ana ){
    ana->Finalize();
  }
  for( auto& det_ana : m_det_ana ){
    det_ana->AssignVisualizer( viz );
    det_ana->Finalize();
  }

  m_tOut->Write();
  m_fOut->Close();
}
