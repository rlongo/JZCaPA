/** @ingroup ana
 *  @file ScintillatorsAnalysis2021.cpp
 *  @brief Implementation of ScintillatorsAnalysis2021.
 *
 *
 *  Function definitions for ScintillatorsAnalysis2021 are provided.
 *  This class is the main class for the trigger scintillator analysis in 2021 test beam.
 *  Four scintillators in total are readout. The two large scintillators (LV and LH) provides trigger with their coincidence.
 *  The two smaller scintillators (SH and SV)
 *  @author Riccardo Longo
 *  @bug No known bugs.
*/


#include "ScintillatorsAnalysis2021.h"
#include "Scintillators.h"
#include "Containers.h"
#include "TH2D.h"
#include "TH1.h"
#include "Visualizer.h"

/** @brief Default Constructor for ScintillatorsAnalysis2021.
 */
ScintillatorsAnalysis2021::ScintillatorsAnalysis2021( ){

}

/** @brief Destructor for ScintillatorsAnalysis2021.
 */
ScintillatorsAnalysis2021::~ScintillatorsAnalysis2021( ){

}

/** @brief Initialization method for ScintillatorsAnalysis2021
 *
 *  Can add other things here that you would
 *  perhaps not put into the constructor.
 *  I.e. a TTree, some tools. Etc.
 *
 */
void ScintillatorsAnalysis2021::Initialize( ){

}

/** @brief Initialization method for ScintillatorsAnalysis2021
 *
 *  Takes a vector of detectors, picks out the Scintillatorss
 *  and assigns them to member pointers
 *
 */
void ScintillatorsAnalysis2021::Initialize( std::vector < Detector* > _vDet ){
    std::cout << "======================= Beginning of ScintillatorsAnalysis2021 =======================" << std::endl;

    for( auto& det : _vDet ){
      if( det->GetChannelsVector().at(0)->detector == "TRIGGER"){
          m_scintillator = (Scintillators*)det;
          m_sciLV = det->GetElement(1,1);
          m_sciLH = det->GetElement(1,2);
          m_sciSV = det->GetElement(2,1);
          m_sciSH = det->GetElement(2,2);
        }
    }

    m_alignment2021 = m_scintillator->GetAlignment2021();
    Survey* survey = m_scintillator->GetSurvey();

    beamX = m_alignment2021->x_trig_table - survey->trig_table.x() + survey->pos.x();
    beamY = m_alignment2021->y_trig_table - survey->trig_table.y() + survey->pos.y();
}


/** @brief Historgam Setup method for ScintillatorsAnalysis2021
 *
 *  Should instantiate any histograms you wish to output here.
 *
 *  @return none
 */
void ScintillatorsAnalysis2021::SetupHistograms( ){

  hPeak = new TH2D("trig_peak","Trigger Peak",100,0,1000,100,0,1000);

}

/** @brief Branch setup method for ScintillatorsAnalysis2021
 *
 *  Adds branches with data created by the analysis
 *
 */
void ScintillatorsAnalysis2021::SetBranches( TTree* _tree ){
    m_AnalysisTree = _tree;


    m_AnalysisTree->Branch( "trig_BeamX", &beamX, "trig_BeamX/D");
    m_AnalysisTree->Branch( "trig_BeamY", &beamY, "trig_BeamY/D");
    //The tree output in this class is set to be equal to the one of the ZDC channels.
    //Large Vertical scintillator
    m_AnalysisTree->Branch("LV_Charge",           "std::vector<double>",  &m_sciLV->Charge           );
    m_AnalysisTree->Branch("LV_Peak_max",         "std::vector<double>",  &m_sciLV->Peak_max         );
    m_AnalysisTree->Branch("LV_Diff_max",         "std::vector<double>",  &m_sciLV->Diff_max         );
    m_AnalysisTree->Branch("LV_Peak_time",        "std::vector<double>",  &m_sciLV->Peak_time        );
    m_AnalysisTree->Branch("LV_Diff_Peak_time",   "std::vector<double>",  &m_sciLV->Diff_Peak_time   );
    m_AnalysisTree->Branch("LV_Ped_mean",         &m_sciLV->PedMean,      "LV_Ped_mean/D"            );
    //Large Horizontal Scintillator
    m_AnalysisTree->Branch("LH_Charge",           "std::vector<double>",  &m_sciLH->Charge           );
    m_AnalysisTree->Branch("LH_Peak_max",         "std::vector<double>",  &m_sciLH->Peak_max         );
    m_AnalysisTree->Branch("LH_Diff_max",         "std::vector<double>",  &m_sciLH->Diff_max         );
    m_AnalysisTree->Branch("LH_Peak_time",        "std::vector<double>",  &m_sciLH->Peak_time        );
    m_AnalysisTree->Branch("LH_Diff_Peak_time",   "std::vector<double>",  &m_sciLH->Diff_Peak_time   );
    m_AnalysisTree->Branch("LH_Ped_mean",         &m_sciLH->PedMean,       "LH_Ped_mean/D"            );
    //Small Vertical Scintillator
    m_AnalysisTree->Branch("SV_Charge",           "std::vector<double>",  &m_sciSV->Charge           );
    m_AnalysisTree->Branch("SV_Peak_max",         "std::vector<double>",  &m_sciSV->Peak_max         );
    m_AnalysisTree->Branch("SV_Diff_max",         "std::vector<double>",  &m_sciSV->Diff_max         );
    m_AnalysisTree->Branch("SV_Peak_time",        "std::vector<double>",  &m_sciSV->Peak_time        );
    m_AnalysisTree->Branch("SV_Diff_Peak_time",   "std::vector<double>",  &m_sciSV->Diff_Peak_time   );
    m_AnalysisTree->Branch("SV_Ped_mean",         &m_sciSV->PedMean,      "SV_Ped_mean/D"            );
    //Small Horizontal Scintillator
    m_AnalysisTree->Branch("SH_Charge",           "std::vector<double>",  &m_sciSH->Charge           );
    m_AnalysisTree->Branch("SH_Peak_max",         "std::vector<double>",  &m_sciSH->Peak_max         );
    m_AnalysisTree->Branch("SH_Diff_max",         "std::vector<double>",  &m_sciSH->Diff_max         );
    m_AnalysisTree->Branch("SH_Peak_time",        "std::vector<double>",  &m_sciSH->Peak_time        );
    m_AnalysisTree->Branch("SH_Diff_Peak_center", "std::vector<int>",     &m_sciSH->Diff_Peak_center );
    m_AnalysisTree->Branch("SH_Ped_mean",         &m_sciSH->PedMean,      "SH_Ped_mean/D"            );
}

/** @brief Analyze Events method for ScintillatorsAnalysis2021
 *
 *
 */
void ScintillatorsAnalysis2021::AnalyzeEvent( ){
  int lvHit=-1, lhHit=-1, svHit=-1, shHit=-1;
  double lvPeak=-1, lhPeak=-1, svPeak=-1, shPeak=-1;

  //Get large vertical scintillator peak value
  for(int hit = 0; hit < m_sciLV->Peak_max.size(); hit++){
      if(m_sciLV->Peak_center.at(hit) > 200 && m_sciLV->Peak_center.at(hit) < 300){
        lvHit = hit;
        lvPeak = m_sciLV->Peak_max.at(hit);
        break;
      }
  }
  //If there was no hit in our range, set the peak value to zero
  if( lvHit == -1 ) lvPeak = 0;

  //Get large vertical scintillator peak value
  for(int hit = 0; hit < m_sciLH->Peak_max.size(); hit++){
      if(m_sciLH->Peak_center.at(hit) > 200 && m_sciLH->Peak_center.at(hit) < 300){
        lhHit = hit;
        lhPeak = m_sciLH->Peak_max.at(hit);
        break;
      }
  }
  //If there was no hit in our range, set the peak value to zero
  if( lhHit == -1 ) lhPeak = 0;

  //Get large vertical scintillator peak value
  for(int hit = 0; hit < m_sciSV->Peak_max.size(); hit++){
      if(m_sciSV->Peak_center.at(hit) > 200 && m_sciSV->Peak_center.at(hit) < 300){
        svHit = hit;
        svPeak = m_sciSV->Peak_max.at(hit);
        break;
      }
  }
  //If there was no hit in our range, set the peak value to zero
  if( svHit == -1 ) svPeak = 0;

  //Get large vertical scintillator peak value
  for(int hit = 0; hit < m_sciSH->Peak_max.size(); hit++){
      if(m_sciSH->Peak_center.at(hit) > 200 && m_sciSH->Peak_center.at(hit) < 300){
        shHit = hit;
        shPeak = m_sciSH->Peak_max.at(hit);
        break;
      }
  }
  //If there was no hit in our range, set the peak value to zero
  if( shHit == -1 ) shPeak = 0;

  hPeak->Fill(lvPeak+lhPeak,svPeak+shPeak);

}

/** @brief Finalize method for ScintillatorsAnalysis2021
 *
 *
 */
void ScintillatorsAnalysis2021::Finalize( ){

    std::string output =  std::getenv("JZCaPA");
    output += "/results/";

    if(m_viz == NULL) m_viz = new Visualizer( "ATLAS" );

    m_viz->DrawPlot(hPeak,"Large Scintillator Peak Sum [ADC]","Small Scintillator Peak Sum [ADC]","Trig_Peak.png","COLZ");

}
