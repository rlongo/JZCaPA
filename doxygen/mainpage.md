\htmlonly

<div class="contents"><span style="font-size:18px;">Click the image below for an overview of the JZCaPA software and the current waveform processing method</span></div>

<div><iframe src="https://docs.google.com/presentation/d/e/2PACX-1vShw3cQfnU_va_3_zYnxlV0iygPLolWbJFm99svNGYDbjlg2opPNQ-Jx2gz5pui6ZKtzU3D6pWKhvYN/embed?start=false&loop=false&delayms=3000" frameborder="0" width="683" height="541" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p><span style="font-size:18px;">&nbsp; Click the image below for an overview of the 2018 Testbeam conditions and run parameters</span></p>
<iframe height="480" src="https://drive.google.com/file/d/1U7gno4SSnoXD64DNWaCeC4-GoWoYUKxd/preview" width="640"></iframe><!-- start footer part -->




\endhtmlonly
